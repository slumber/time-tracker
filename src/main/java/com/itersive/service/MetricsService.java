package com.itersive.service;

import com.itersive.model.gitlab.Timelog;
import io.micrometer.core.instrument.MeterRegistry;
import io.smallrye.mutiny.Uni;
import io.smallrye.mutiny.infrastructure.Infrastructure;
import lombok.extern.slf4j.Slf4j;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.List;

@Slf4j
@ApplicationScoped
public class MetricsService {
    @Inject
    MeterRegistry meterRegistry;

    public void process(List<Timelog> timelogs) {
        Uni.createFrom()
                .item(timelogs)
                .emitOn(Infrastructure.getDefaultWorkerPool())
                .subscribe()
                .with(this::createMetrics, Throwable::printStackTrace);
    }

    private void createMetrics(List<Timelog> list) {
        list.forEach(this::createMetric);
    }

    private void createMetric(Timelog item) {
        String issueId = item.getIssue() == null ? "" : item.getIssue().getId();
        String mrId = item.getMergeRequest() == null ? "" : item.getMergeRequest().getId();
        LocalDateTime dateTime = LocalDateTime.ofInstant(item.getSpentAt(), ZoneId.systemDefault());
        meterRegistry.counter("com.itersive.time-tracker.timelogs",
                "user", item.getUser().getUsername(),
                "dateTime", dateTime.toString(),
                "year", String.valueOf(dateTime.getYear()),
                "month", String.valueOf(dateTime.getMonthValue()),
                "day", String.valueOf(dateTime.getDayOfMonth()),
                "issue", issueId,
                "mr", mrId)
                .increment(item.getTimeSpent().doubleValue());
        Uni.createFrom().voidItem();
    }
}
