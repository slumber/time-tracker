package com.itersive.service;

import com.itersive.model.gitlab.VisibleGroup;
import com.itersive.model.timetracker.User;
import com.itersive.repository.UserRepository;
import io.quarkus.oidc.IdToken;
import io.quarkus.oidc.OidcSession;
import io.quarkus.oidc.UserInfo;
import io.quarkus.security.Authenticated;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.microprofile.jwt.JsonWebToken;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.net.URI;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

@Path("/auth")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Authenticated
@Slf4j
public class AuthService {
    @Inject
    @IdToken
    JsonWebToken idToken;
    @Inject
    UserRepository userRepository;
    @Inject
    UserInfo userInfo;
    @ConfigProperty(name = "app.frontend.url")
    String redirectUrl;
    @ConfigProperty(name = "app.api.groups")
    Optional<List<String>> groupsToCheck;
    @Inject
    VisibleGroupsService visibleGroupsService;
    @Inject
    OidcSession oidcSession;

    public static String generateBearerToken(String token) {
        return "Bearer " + token;
    }

    @GET
    @Transactional
    public Response authUser() {
        User user = userRepository.findByEmail(this.idToken.getClaim("email"));
        Set<VisibleGroup> visibleGroups = visibleGroupsService.getUserVisibleGroups();

        List<String> groups = visibleGroups.stream()
                .map(g -> g.getFullPath())
                .collect(Collectors.toList());

        if (groups.isEmpty()) {
            groups = userInfo.getArray("groups")
                    .getValuesAs(jsonValue -> jsonValue.toString().replace("\"", ""))
                    .stream()
                    .filter(this::testGroupName)
                    .collect(Collectors.toList());
        }

        if (user == null) {
            user = User.builder()
                    .email(userInfo.getString("email"))
                    .name(userInfo.getString("name"))
                    .nickname(userInfo.getString("nickname"))
                    .profile(userInfo.getString("profile"))
                    .picture(userInfo.getString("picture"))
                    .groups(groups)
                    .defaultGroup(groups.get(0))
                    .hourlyWage(0.0)
                    .build();
            log.debug("New user created - {} with id {}", user.getEmail(), user.getId());
        } else {
            user.setEmail(userInfo.getString("email"));
            user.setName(userInfo.getString("name"));
            user.setNickname(userInfo.getString("nickname"));
            user.setProfile(userInfo.getString("profile"));
            user.setPicture(userInfo.getString("picture"));
            user.setGroups(groups);
        }
        userRepository.persist(user);
        return Response.status(302).location(URI.create(redirectUrl)).entity(user).build();
    }

    @GET
    @Path("/user")
    @Transactional
    public User getCurrentUser() {
        return userRepository.findByEmail(this.idToken.getClaim("email"));
    }

    @POST
    @Path("/logout")
    public void logout() {
        oidcSession.logout()
                .await()
                .indefinitely();
    }

    private boolean testGroupName(String groupName) {
        if (groupsToCheck.isEmpty() || groupsToCheck.get().isEmpty()) {
            return true;
        }
        for (String groupToCheck : groupsToCheck.get()) {
            if (groupName.toLowerCase().contains(groupToCheck.toLowerCase())) return true;
        }
        return false;
    }
}
