package com.itersive.service;

import com.itersive.api.GitLabGroupRestClient;
import com.itersive.model.gitlab.VisibleGroup;
import com.itersive.producer.BearerTokenProducer;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.microprofile.config.inject.ConfigProperty;
import org.eclipse.microprofile.rest.client.inject.RestClient;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.ws.rs.core.GenericType;
import javax.ws.rs.core.Response;
import java.util.HashSet;
import java.util.Set;

@ApplicationScoped
@Slf4j
public class VisibleGroupsService {
    private static String X_TOTAL_PAGES_HEADER = "X-Total-Pages";
    @ConfigProperty(name = "gitlab.visible-groups.page-size")
    int ITEMS_PER_PAGE;
    @Inject
    BearerTokenProducer bearerTokenProducer;

    @RestClient
    GitLabGroupRestClient client;

    public Set<VisibleGroup> getUserVisibleGroups() {
        Set<VisibleGroup> visibleGroups = new HashSet<>();
        try {
            String token = bearerTokenProducer.create();
            Response response = client.getVisibleGroups(token, ITEMS_PER_PAGE);
            visibleGroups = response.readEntity(new GenericType<>() {});
            int counter = 2;
            int totalPages = Integer.valueOf(response.getHeaderString(X_TOTAL_PAGES_HEADER));
            while (counter <= totalPages) {
                visibleGroups.addAll(client.getVisibleGroups(token, ITEMS_PER_PAGE, counter));
                counter++;
            }
        } catch (Exception exc) {
            log.debug("Exception occured during fetching groups from Gitlab: " + exc.getMessage());
        }
        return visibleGroups;
    }
}
