package com.itersive.service;

import com.itersive.api.GitLabGroupApi;
import com.itersive.dto.GroupPermissions;
import com.itersive.exception.NotOwnedGroupOrNotExistException;
import com.itersive.exception.UserNotFoundException;
import com.itersive.model.gitlab.GitLabDateTime;
import com.itersive.model.gitlab.Group;
import com.itersive.model.gitlab.GroupMember;
import com.itersive.model.gitlab.Timelog;
import com.itersive.model.mapper.GroupMembersMapper;
import com.itersive.model.result.TimeSum;
import com.itersive.model.timetracker.GroupMembers;
import com.itersive.model.timetracker.User;
import com.itersive.repository.UserRepository;
import io.quarkus.oidc.AccessTokenCredential;
import io.quarkus.security.Authenticated;
import io.smallrye.context.api.CurrentThreadContext;
import io.smallrye.graphql.client.GraphQLClientException;
import lombok.extern.slf4j.Slf4j;
import org.eclipse.microprofile.context.ManagedExecutor;

import javax.inject.Inject;
import javax.transaction.Transactional;
import javax.validation.Valid;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import java.math.BigDecimal;
import java.net.URLDecoder;
import java.nio.charset.StandardCharsets;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.function.Predicate;
import java.util.function.Supplier;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.Optional.ofNullable;

@Path("/user")
@Produces(MediaType.APPLICATION_JSON)
@Consumes(MediaType.APPLICATION_JSON)
@Authenticated
@Slf4j
public class UserService {
    @Inject
    ManagedExecutor executor;
    @Inject
    GitLabGroupApi client;
    @Inject
    MetricsService metricsService;
    @Inject
    UserRepository userRepository;
    @Inject
    AuthService authService;
    @Inject
    AccessTokenCredential accessTokenCredential;
    @Inject
    GroupMembersMapper groupMembersMapper;

    @GET
    public User getUser() {
        return authService.getCurrentUser();
    }

    @PUT
    @Transactional
    public User updateUser(User user) {
        User found = userRepository.findById(user.getId());
        found.setEmail(user.getEmail());
        found.setName(user.getName());
        found.setNickname(user.getNickname());
        found.setPicture(user.getPicture());
        found.setProfile(user.getProfile());
        found.setGroups(user.getGroups());
        found.setDefaultGroup(user.getDefaultGroup());
        found.setHourlyWage(user.getHourlyWage());
        found.setStartDate(user.getStartDate());
        return found;
    }

    @POST
    @Path("groupPermissions")
    @Transactional
    public Set<String> addGroupPermissions(@Valid GroupPermissions groupPermission) {
        String nickname = groupPermission.getNickname();
        Set<String> groupNames = groupPermission.getGroupNames().stream().collect(Collectors.toSet());
        User found = ofNullable(userRepository.findByNickname(nickname))
                .orElseThrow(() -> new UserNotFoundException(nickname));

        Set<String> groupToAdd = groupNames;
        for (String groupName : groupNames) {
            if (found.getGroupsPermissions().contains(groupName)) {
                groupToAdd.remove(groupName);
            }
            validateGroupOwnership(groupName);
        }
        groupToAdd.forEach(groupName -> setGroupPermission(found, groupName));

        return groupToAdd;
    }

    @DELETE
    @Path("groupPermissions")
    @Transactional
    public Response removeGroupPermissions(@Valid GroupPermissions groupPermission) {
        String nickname = groupPermission.getNickname();
        Set<String> groupNames = new HashSet<>(groupPermission.getGroupNames());
        User found = ofNullable(userRepository.findByNickname(nickname))
                .orElseThrow(() -> new UserNotFoundException(nickname));

        for (String groupName : groupNames) {
            validateGroupOwnership(groupName);
        }
        groupNames.forEach(groupName -> removeGroupPermission(found, groupName));

        return Response.noContent().build();
    }

    @GET
    @Path("{username}/timelog")
    public List<Timelog> getTimelogsForUser(@PathParam("username") String username,
                                            @QueryParam("group") String encodedGroup,
                                            @QueryParam("today") boolean today,
                                            @QueryParam("this-month") boolean thisMonth,
                                            @QueryParam("start") String start,
                                            @QueryParam("end") String end) {
        String group = decodeGroup(encodedGroup);
        if (today) {
            return getTodayTimePerUser(username, group);
        }
        if (Objects.nonNull(start) && Objects.nonNull(end)) {
            return getTimelogPerUserBetween(username, group, GitLabDateTime.of(start), GitLabDateTime.of(end));
        }
        return getThisMonthTimePerUser(username, group);
    }

    @GET
    @Path("group/{group}/timelog")
    public List<Timelog> getTimelogsForGroup(@PathParam("group") String encodedGroup,
                                             @QueryParam("today") boolean today,
                                             @QueryParam("this-month") boolean thisMonth,
                                             @QueryParam("start") String start,
                                             @QueryParam("end") String end) {
        List<Timelog> results;
        String group = decodeGroup(encodedGroup);
        if (today) {
            results = getTimelogsByGroup(group, GitLabDateTime.dayStart(), GitLabDateTime.dayEnd());
        } else if (Objects.nonNull(start) && Objects.nonNull(end)) {
            results = getTimelogsByGroup(group, GitLabDateTime.of(start), GitLabDateTime.of(end));
        } else {
            results = getTimelogsByGroup(group, GitLabDateTime.monthStart(), GitLabDateTime.monthEnd());
        }
        metricsService.process(results);
        return results;
    }

    @GET
    @Path("{username}/time-sum")
    public TimeSum getTimeSumForUser(@PathParam("username") String username,
                                     @QueryParam("today") boolean today,
                                     @QueryParam("group") String encodedGroup,
                                     @QueryParam("this-month") boolean thisMonth,
                                     @QueryParam("start") String start,
                                     @QueryParam("end") String end) {
        List<Timelog> timelogs = new ArrayList<>();
        String group = decodeGroup(encodedGroup);
        if (today) {
            timelogs = getTodayTimePerUser(username, group);
        }
        if (thisMonth) {
            timelogs = getThisMonthTimePerUser(username, group);
        }
        if (Objects.nonNull(start) && Objects.nonNull(end)) {
            GitLabDateTime startDate = new GitLabDateTime(LocalDateTime.parse(start));
            GitLabDateTime endDate = new GitLabDateTime(LocalDateTime.parse(end));
            timelogs = getTimelogPerUserBetween(username, group, startDate, endDate);
        }
        BigDecimal seconds = timelogs.stream()
                .map(Timelog::getTimeSpent)
                .reduce(BigDecimal.ZERO, BigDecimal::add);
        return new TimeSum(seconds);
    }


    @GET
    @Path("ownedGroups")
    @CurrentThreadContext
    public List<GroupMembers> getOwnedGroupsForUser() {
        User user = authService.getCurrentUser();
        Set<String> groupNames = new HashSet<>(user.getGroups());
        groupNames.addAll(user.getGroupsPermissions());
        List<GroupMembers> ownedGroupsMembers = getGroupMembers(user, groupNames);

        return ownedGroupsMembers;
    }

    @GET
    @Path("ownedGroupsSync")
    @Deprecated
    public List<GroupMembers> getOwnedGroupsSync() {
        User user = authService.getCurrentUser();
        Set<GroupMembers> ownedAndPermissionGroups = new LinkedHashSet<>();

        List<GroupMembers> ownedGroups = user.getGroups()
                .stream()
                .map(groupName -> {
                    Group result = null;
                    try {
                        result = client.getGroup(groupName, "",
                                                 AuthService.generateBearerToken(accessTokenCredential.getToken()));
                    } catch (GraphQLClientException exc) {
                        log.debug("No permission to perform getGroup on group <{}>, message <{}>", groupName, exc.getMessage(), exc);
                    }
                    return result;
                })
                .filter(Objects::nonNull)
                .filter(checkOwnershipAndPermission(user))
                .map(groupMembersMapper::toGroupMembers)
                .collect(Collectors.toList());

        List<GroupMembers> permissionGroups = user.getGroupsPermissions()
                .stream()
                .map(groupName -> client.getGroup(groupName, "",
                                                  AuthService.generateBearerToken(accessTokenCredential.getToken())))
                .map(groupMembersMapper::toGroupMembers)
                .collect(Collectors.toList());

        ownedAndPermissionGroups.addAll(ownedGroups);
        ownedAndPermissionGroups.addAll(permissionGroups);
        return new ArrayList<>(ownedAndPermissionGroups);
    }

    private List<GroupMembers> getGroupMembers(User user, Collection<String> groupNames) {
        List<CompletableFuture<GroupMembers>> futureGroupMembers = new ArrayList<>();
        groupNames.forEach(group -> futureGroupMembers.add(getGroupMembersAsync(user, group, executor)));
        CompletableFuture[] completableFutures = new CompletableFuture[futureGroupMembers.size()];
        CompletableFuture<Void> combinedGroupMembers = CompletableFuture.allOf(futureGroupMembers.toArray(completableFutures));

        try {
            combinedGroupMembers.get();
        } catch (InterruptedException | ExecutionException exc) {
            log.debug("Fetching user groups details from Gitlab failed with <{}>", exc.getMessage(), exc);
            throw new RuntimeException("Exception occured during fetching user groups details from Gitlab");
        }

        List<GroupMembers> groupMembers = futureGroupMembers.stream()
                .map(CompletableFuture::join)
                .filter(Objects::nonNull)
                .collect(Collectors.toList());

        return groupMembers;
    }

    private Supplier<GroupMembers> getGroupMembers(User user, String groupName) {
        return () -> Stream.of(groupName)
                .map(group -> {
                    Group result = null;
                    try {
                        result = client.getGroup(group, "",
                                                 AuthService.generateBearerToken(accessTokenCredential.getToken()));
                    } catch (GraphQLClientException exc) {
                        log.debug("No permission to perform getGroup on group <{}>, message <{}>", group, exc.getMessage(), exc);
                    }
                    return result;
                })
                .filter(Objects::nonNull)
                .filter(checkOwnershipAndPermission(user))
                .map(groupMembersMapper::toGroupMembers)
                .findFirst()
                .orElse(null);
    }

    private CompletableFuture<GroupMembers> getGroupMembersAsync(User user, String group, ManagedExecutor executor) {
        return CompletableFuture.supplyAsync(getGroupMembers(user, group), executor);
    }

    private List<Timelog> getTodayTimePerUser(String username, String group) {
        return getTimelogPerUserBetween(username, group, GitLabDateTime.dayStart(), GitLabDateTime.dayEnd());
    }

    private List<Timelog> getThisMonthTimePerUser(String username, String group) {
        return getTimelogPerUserBetween(username, group, GitLabDateTime.monthStart(), GitLabDateTime.monthEnd());
    }

    private List<Timelog> getTimelogPerUserBetween(String username, String groupName, GitLabDateTime start, GitLabDateTime end) {
        if (groupName == null || groupName.isBlank()) {
            groupName = authService.getCurrentUser().getDefaultGroup();
        }
        List<Timelog> results = getTimelogsByGroup(groupName, start, end);
        if (results.isEmpty()) {
            return results;
        }
        results = results.stream()
                .filter(timelog -> timelog.getUser().getUsername().equals(username))
                .collect(Collectors.toList());
        metricsService.process(results);
        return results;
    }

    private List<Timelog> getTimelogsByGroup(String groupName, GitLabDateTime start, GitLabDateTime end) {
        if (groupName == null || groupName.isBlank()) {
            throw new IllegalArgumentException("Group name not supplied");
        }
        Group group = client.getGroup(groupName, start, end, AuthService.generateBearerToken(accessTokenCredential.getToken()));
        if (group == null) {
            return new ArrayList<>();
        }
        List<Timelog> results = group.getTimelogs().getNodes();
        while (group.getTimelogs().getPageInfo().getHasNextPage()) {
            group = client.getGroup(groupName, start, end, group.getTimelogs().getPageInfo().getEndCursor(),
                                    AuthService.generateBearerToken(accessTokenCredential.getToken()));
            results.addAll(group.getTimelogs().getNodes());
        }
        return results != null ? results : new ArrayList<>();
    }

    private Predicate<Group> checkOwnershipAndPermission(User user) {
        return group -> {
            if (user.getGroupsPermissions().contains(group.getFullPath())) {
                return true;
            }
            GroupMember found = group.getGroupMembers().getNodes()
                    .stream()
                    .filter(groupMember -> Objects.nonNull(groupMember.getUser()))
                    .filter(groupMember -> groupMember.getUser().getUsername().equals(user.getNickname()))
                    .findAny()
                    .orElse(null);
            return found != null ? found.getAccessLevel().getStringValue().equals("OWNER") : false;
        };
    }

    private boolean isLoggedUserGroupOwner(String groupName) {
        User loggedUser = authService.getCurrentUser();
        Group requestGroup = client.getGroup(groupName, AuthService.generateBearerToken(accessTokenCredential.getToken()));

        return Optional.ofNullable(requestGroup).map(group -> checkOwnershipAndPermission(loggedUser).test(group)).orElse(false);
    }

    private void setGroupPermission(User user, String groupName) {
        if (user.getGroupsPermissions().isEmpty()) {
            user.setGroupsPermissions(new HashSet<>());
        }
        user.getGroupsPermissions().add(groupName);
    }

    private void removeGroupPermission(User user, String groupName) {
        if (user.getGroupsPermissions().isEmpty()) {
            return;
        }
        user.getGroupsPermissions().remove(groupName);
    }

    private void validateGroupOwnership(String groupName) {
        if (!isLoggedUserGroupOwner(groupName)) {
            throw new NotOwnedGroupOrNotExistException(groupName);
        }
    }

    private String decodeGroup(String encodedGroup) {
        return encodedGroup != null ? URLDecoder.decode(encodedGroup, StandardCharsets.UTF_8) : null;
    }
}
