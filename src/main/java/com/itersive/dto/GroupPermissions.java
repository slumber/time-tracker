package com.itersive.dto;

import lombok.Getter;
import lombok.Setter;

import javax.validation.constraints.NotNull;
import java.util.List;

@Getter
@Setter
public class GroupPermissions {
    @NotNull(message = "Nickname can not be null")
    private String nickname;
    @NotNull(message = "Group list must contain at least one name")
    private List<String> groupNames;
}
