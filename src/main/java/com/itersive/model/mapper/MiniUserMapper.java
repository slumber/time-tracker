package com.itersive.model.mapper;

import com.itersive.model.gitlab.GroupMember;
import com.itersive.model.timetracker.MinimalUser;
import com.itersive.model.timetracker.User;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "cdi")
public interface MiniUserMapper {
    MinimalUser toMinimalUser(User user);

    @Mapping(source = "user.name", target = "name")
    @Mapping(source = "user.username", target = "nickname")
    @Mapping(source = "user.webUrl", target = "profile")
    @Mapping(source = "user.avatarUrl", target = "picture")
    MinimalUser toMinimalUser(GroupMember user);
}
