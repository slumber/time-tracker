package com.itersive.model.mapper;

import com.itersive.model.gitlab.Group;
import com.itersive.model.timetracker.GroupMembers;
import org.mapstruct.Mapper;
import org.mapstruct.Mapping;

@Mapper(componentModel = "cdi", uses = MiniUserMapper.class)
public interface GroupMembersMapper {
    @Mapping(source = "groupMembers.nodes", target = "users")
    GroupMembers toGroupMembers(Group user);
}
