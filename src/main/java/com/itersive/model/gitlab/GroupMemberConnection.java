package com.itersive.model.gitlab;

import lombok.Data;

import java.util.List;

@Data
public class GroupMemberConnection {
    private List<GroupMemberEdge> edges;
    private List<GroupMember> nodes;
    private PageInfo pageInfo;
}
