package com.itersive.model.gitlab;

import lombok.Data;

import java.util.List;

@Data
public class TimelogConnection {
    private List<TimelogEdge> edges;
    private List<Timelog> nodes;
    private PageInfo pageInfo;
}
