package com.itersive.model.gitlab;

import lombok.Data;

@Data
public class Note {
    private String id;
    private String body;
    private User author;
}
