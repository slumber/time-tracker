package com.itersive.model.gitlab;

import com.fasterxml.jackson.annotation.JsonProperty;
import lombok.Data;

@Data
public class VisibleGroup {
    @JsonProperty("full_path")
    private String fullPath;
}
