package com.itersive.model.gitlab;

import lombok.AllArgsConstructor;
import lombok.Data;
import org.eclipse.microprofile.graphql.Name;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Name("Time")
@Data
@AllArgsConstructor
public class GitLabDateTime {
    private final LocalDateTime dateTime;

    public static GitLabDateTime of(String string) {
        return new GitLabDateTime(LocalDateTime.parse(string));
    }

    public static GitLabDateTime dayStart(){
            return new GitLabDateTime(LocalDate.now().atTime(0, 0, 0));
    }

    public static GitLabDateTime dayEnd(){
            return new GitLabDateTime(LocalDate.now().atTime(23, 59, 59));
    }

    public static GitLabDateTime monthStart(){
            return new GitLabDateTime(LocalDate.now().withDayOfMonth(1).atTime(0, 0, 0));
    }

    public static GitLabDateTime monthEnd(){
            return new GitLabDateTime(LocalDate.now().withDayOfMonth(LocalDate.now().lengthOfMonth()).atTime(23, 59, 59));
    }
}
