package com.itersive.model.gitlab;

import lombok.Data;

@Data
public class PageInfo {
    private String endCursor;
    private Boolean hasNextPage;
    private Boolean hasPreviousPage;
    private String startCursor;
}
