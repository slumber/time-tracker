package com.itersive.model.gitlab;

import lombok.Data;

import java.math.BigDecimal;
import java.time.Instant;

@Data
public class Timelog {
    private Issue issue;
    private MergeRequest mergeRequest;
    private Note note;
    private Instant spentAt;
    private BigDecimal timeSpent;
    private User user;
}
