package com.itersive.model.gitlab;

import lombok.Data;

@Data
public class TimelogEdge {
    private String cursor;
    private Timelog node;
}
