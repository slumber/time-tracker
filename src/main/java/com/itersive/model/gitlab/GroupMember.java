package com.itersive.model.gitlab;

import lombok.Data;

@Data
public class GroupMember {
    private AccessLevel accessLevel;
    private User user;
}
