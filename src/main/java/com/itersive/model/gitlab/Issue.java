package com.itersive.model.gitlab;

import lombok.Data;

import java.time.LocalDate;

@Data
public class Issue {
    private String title;
    private String id;
    private String webUrl;
    private LocalDate dueDate;
    private long projectId;
}
