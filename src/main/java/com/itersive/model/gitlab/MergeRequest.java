package com.itersive.model.gitlab;

import lombok.Data;

@Data
public class MergeRequest {
    private String title;
    private String id;
    private String webUrl;
    private long projectId;
}
