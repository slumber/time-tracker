package com.itersive.model.gitlab;

import lombok.Data;

@Data
public class AccessLevel {
    private int integerValue;
    private String stringValue;
}
