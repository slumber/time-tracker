package com.itersive.model.gitlab;

import lombok.Data;

@Data
public class User {
    private String id;
    private String username;
    private String name;
    private String webUrl;
    private String avatarUrl;
}
