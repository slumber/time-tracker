package com.itersive.model.gitlab;

import lombok.Data;

@Data
public class GroupMemberEdge {
    private String cursor;
    private GroupMember node;
}
