package com.itersive.model.gitlab;

import lombok.Data;

@Data
public class Group {
    private String id;
    private String name;
    private String fullPath;
    private String webUrl;
    private TimelogConnection timelogs;
    private GroupMemberConnection groupMembers;
}
