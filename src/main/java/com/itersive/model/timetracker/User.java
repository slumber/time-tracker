package com.itersive.model.timetracker;

import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.Getter;
import lombok.NoArgsConstructor;

import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.time.LocalDateTime;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Entity(name = "users")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Builder
public class User {
    @Id
    @GeneratedValue
    private Long id;
    private String email;
    private String name;
    private String nickname;
    private String profile;
    private String picture;
    @ElementCollection(fetch = FetchType.EAGER)
    private List<String> groups;
    private String defaultGroup;
    private Double hourlyWage;
    private LocalDateTime startDate;
    @ElementCollection(fetch = FetchType.EAGER)
    @Getter(AccessLevel.NONE)
    private Set<String> groupsPermissions;

    public Set<String> getGroupsPermissions() {
        return groupsPermissions != null ? groupsPermissions : new HashSet<>();
    }
}
