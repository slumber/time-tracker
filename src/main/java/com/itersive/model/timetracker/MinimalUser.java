package com.itersive.model.timetracker;

import lombok.Data;

@Data
public class MinimalUser {
    private String name;
    private String nickname;
    private String profile;
    private String picture;
}
