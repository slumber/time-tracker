package com.itersive.model.timetracker;

import lombok.Data;

import java.util.List;

@Data
public class GroupMembers {
    private String id;
    private String name;
    private String fullPath;
    private String webUrl;
    private List<MinimalUser> users;
}
