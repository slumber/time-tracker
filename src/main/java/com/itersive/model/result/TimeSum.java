package com.itersive.model.result;

import lombok.Data;

import java.math.BigDecimal;
import java.math.RoundingMode;

@Data
public class TimeSum {
    private BigDecimal seconds;
    private BigDecimal minutes;
    private BigDecimal hours;

    public TimeSum(BigDecimal seconds) {
        this.seconds = seconds;
        this.minutes = seconds.divide(new BigDecimal(60), 2, RoundingMode.HALF_UP);
        this.hours = seconds.divide(new BigDecimal(3600), 2, RoundingMode.HALF_UP);
    }
}
