package com.itersive.producer;

import io.quarkus.oidc.AccessTokenCredential;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;

@ApplicationScoped
public class BearerTokenProducer {

    @Inject
    AccessTokenCredential accessTokenCredential;

    public String create() {
        return "Bearer " + accessTokenCredential.getToken();
    }
}
