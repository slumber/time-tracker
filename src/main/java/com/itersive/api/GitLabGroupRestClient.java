package com.itersive.api;

import com.itersive.model.gitlab.VisibleGroup;
import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import javax.ws.rs.GET;
import javax.ws.rs.HeaderParam;
import javax.ws.rs.Path;
import javax.ws.rs.QueryParam;
import javax.ws.rs.core.Response;
import java.util.Set;

@RegisterRestClient(configKey = "app.gitlab.api")
public interface GitLabGroupRestClient {
    @GET
    @Path("/groups")
    Response getVisibleGroups(@HeaderParam("Authorization") String token, @QueryParam("per_page") int perPage);

    @GET
    @Path("/groups")
    Set<VisibleGroup> getVisibleGroups(@HeaderParam("Authorization") String token, @QueryParam("per_page") int perPage,
                                       @QueryParam("page") int pageNumber);
}
