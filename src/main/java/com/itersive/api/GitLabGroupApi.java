package com.itersive.api;

import com.itersive.model.gitlab.GitLabDateTime;
import com.itersive.model.gitlab.Group;
import io.smallrye.graphql.client.typesafe.api.GraphQLClientApi;
import io.smallrye.graphql.client.typesafe.api.Header;
import io.smallrye.graphql.client.typesafe.api.NestedParameter;
import org.eclipse.microprofile.graphql.Id;
import org.eclipse.microprofile.graphql.NonNull;

import java.time.LocalTime;

@GraphQLClientApi(configKey = "app.gitlab.api")
public interface GitLabGroupApi {
    Group getGroup(@NonNull @Id String fullPath,
                   @Header(name = "Authorization") String token);

    Group getGroup(@NonNull @Id String fullPath,
                   @NestedParameter("timelogs") LocalTime startDate,
                   @Header(name = "Authorization") String token);

    Group getGroup(@NonNull @Id String fullPath,
                   @NestedParameter("timelogs") GitLabDateTime startDate,
                   @NestedParameter("timelogs") GitLabDateTime endDate,
                   @Header(name = "Authorization") String token);

    Group getGroup(@NonNull @Id String fullPath,
                   @NestedParameter("timelogs") GitLabDateTime startDate,
                   @NestedParameter("timelogs") GitLabDateTime endDate,
                   @NestedParameter("timelogs") String after,
                   @Header(name = "Authorization") String token);

    Group getGroup(@NonNull @Id String fullPath,
                   @NestedParameter("groupMembers") String search,
                   @Header(name = "Authorization") String token);

    Group getGroup(@NonNull @Id String fullPath,
                   @NestedParameter("groupMembers") String search,
                   @NestedParameter("groupMembers") String after,
                   @Header(name = "Authorization") String token);
}
