package com.itersive.repository;

import com.itersive.model.timetracker.User;
import io.quarkus.hibernate.orm.panache.PanacheRepository;

import javax.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class UserRepository implements PanacheRepository<User> {
    public User findByEmail(String email) {
        return find("email", email).firstResult();
    }

    public User findByNickname(String nickname) {
        return find("nickname", nickname).firstResult();
    }
}
