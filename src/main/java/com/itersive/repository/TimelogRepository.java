package com.itersive.repository;

import com.itersive.model.gitlab.Timelog;

import javax.enterprise.context.ApplicationScoped;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

@ApplicationScoped
public class TimelogRepository {
    private final Map<String, List<Timelog>> entries = new HashMap<>();

    public void add(String username, Timelog timelog) {
        if (entries.containsKey(username)) {
            entries.get(username).add(timelog);
        } else {
            List<Timelog> timelogs = new LinkedList<>();
            timelogs.add(timelog);
            entries.put(username, timelogs);
        }
    }

    public void add(String username, List<Timelog> timelog) {
        if (entries.containsKey(username)) {
            entries.get(username).addAll(timelog);
        } else {
            List<Timelog> timelogs = new LinkedList<>(timelog);
            entries.put(username, timelogs);
        }
    }

    public List<Timelog> get(String username) {
        return entries.getOrDefault(username, new LinkedList<>());
    }
}
