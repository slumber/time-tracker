package com.itersive.exception;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

public class UserNotFoundException extends RuntimeException {
    public UserNotFoundException(String nickname) {
        String msg = String.format("User with username <%s> not found.", nickname);
        Response response = Response.status(Response.Status.NOT_FOUND)
                .entity(msg)
                .build();
        throw new WebApplicationException(response);
    }
}
