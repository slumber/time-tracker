package com.itersive.exception;

import javax.ws.rs.WebApplicationException;
import javax.ws.rs.core.Response;

public class NotOwnedGroupOrNotExistException extends RuntimeException{
    public NotOwnedGroupOrNotExistException(String groupName) {
        String msg = String.format("Group <%s> does not exits or you are not owner of it.", groupName);
        Response response = Response.status(Response.Status.NOT_FOUND)
                .entity(msg)
                .build();
        throw new WebApplicationException(response);
    }
}
