import React from 'react'
import { IntlProvider } from 'react-intl'
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom'
import { StylesProvider } from '@material-ui/core'

import { useAppDispatch, useAppSelector } from './redux/hooks'
import { changeLanguage } from './redux/slices/language'

import { GlobalStyle } from './assets/styles/globalStyles'
import englishMessages from './languages/en.json'
import polishMessages from './languages/pl.json'
import { setAuthed } from './redux/slices/authed'
import { getUser } from './api/user.api'
import { setUser } from './redux/slices/user'
import { getUserTimeTrackingResults } from './api/timeTrackingResults.api'
import { setTimeTrackingResults } from './redux/slices/timeTrackingResults'

import GeneralHeader from './components/GeneralHeader/GeneralHeader'
import Navigation from './components/Navigation/Navigation'
import Login from './components/Login/Login'
import Dashboard from './components/Dashboard/Dashboard'
import Results from './components/Results/Results'
import GroupResults from './components/Results/GroupResults'
import Loader from './components/Loader/Loader'
import ErrorSnackbar from './components/Snackbars/ErrorSnackbar'
import SuccessSnackbar from './components/Snackbars/SuccessSnackbar'
import PrivateRoute from './components/PrivateRoute/PrivateRoute'
import GroupsCalendar from './components/GroupsCalendar/GroupsCalendar'
import OwnedGroups from './components/OwnedGroups/OwnedGroups'
import GroupsPermissions from './components/GroupsPermissions/GroupsPermissions'
import Error404 from './components/Error404/Error404'
import ErrorPage from './components/ErrorPage/ErrorPage'
import ErrorSessionExpired from './components/ErrorSessionExpired/SessionExpired'

const App = () => {
  const [header, setHeader] = React.useState<string>('Timetracker')
  const { language } = useAppSelector(state => state.language)
  const { results } = useAppSelector(state => state.timeTrackingResults)
  const { user } = useAppSelector(state => state.user)
  const { authed } = useAppSelector(state => state.authed)
  const [isUserFetchingDone, setIsUserFetchingDone] = React.useState(false)

  const dispatch = useAppDispatch()

  React.useEffect(() => {
    if (navigator.language === 'pl') {
      dispatch(changeLanguage(polishMessages))
    } else {
      dispatch(changeLanguage(englishMessages))
    }
  }, [dispatch])

  React.useEffect(() => {
    dispatch(setAuthed(!!user))
  }, [user])

  React.useEffect(() => {
    getUser(
      data => dispatch(setUser(data)),
      () => setIsUserFetchingDone(true)
    )
  }, [])

  React.useEffect(() => {
    if (user && results === null && !sessionStorage.getItem('dateStartResults') && !sessionStorage.getItem('dateEndResults')) {
      getUserTimeTrackingResults(user!.nickname, data => dispatch(setTimeTrackingResults(data)))
    }
  }, [user, results])

  return (
    <IntlProvider locale={navigator.language || 'en'} messages={language}>
      <StylesProvider injectFirst>
        <div className='App'>
          <Router>
            {isUserFetchingDone && (
              <>
                {user && <Navigation setHeader={setHeader} />}
                <GeneralHeader header={header} />
                <main className="main">
                <Switch>
                  <Route exact path='/'>
                    {user ? <Redirect to={'/dashboard'} /> : <Login />}
                  </Route>
                  <PrivateRoute authed={authed} path='/dashboard' component={Dashboard} />
                  <PrivateRoute authed={authed} path='/:username/results/:group?' component={Results} />
                  <PrivateRoute authed={authed} path='/results/group/:group?' component={GroupResults} />
                  <PrivateRoute authed={authed} path='/calendar' component={GroupsCalendar} />
                  <PrivateRoute authed={authed} path='/owned-groups' component={OwnedGroups} />
                  <PrivateRoute authed={authed} path='/groups-permissions' component={GroupsPermissions} />
                  <Route component={ErrorPage} path={'/error-page'} />
                  <Route component={ErrorSessionExpired} path={'/session-expired'} />
                  <Route component={Error404} path={'*'} />
                </Switch>
                </main>
              </>
            )}
          </Router>
          <Loader />
          <ErrorSnackbar />
          <SuccessSnackbar />
        </div>
        <GlobalStyle />
      </StylesProvider>
    </IntlProvider>
  )
}

export default App
