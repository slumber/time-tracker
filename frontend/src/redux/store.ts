import { configureStore } from '@reduxjs/toolkit'
import userReducer from './slices/user'
import authedReducer from './slices/authed'
import languageReducer from './slices/language'
import timeTrackingResultsReducer from './slices/timeTrackingResults'
import totalWorkTimeReducer from './slices/totalWorkTime'
import ownedGroupsReducer from './slices/ownedGroups'

const store = configureStore({
  reducer: {
    user: userReducer,
    authed: authedReducer,
    language: languageReducer,
    timeTrackingResults: timeTrackingResultsReducer,
    totalWorkTime: totalWorkTimeReducer,
    ownedGroups: ownedGroupsReducer
  }
})
export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch

export default store
