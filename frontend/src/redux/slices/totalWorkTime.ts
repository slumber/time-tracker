import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { ITotalWorkTime } from '../../types'

interface totalWorkTimeState {
  total: ITotalWorkTime | null
}

const initialState = { total: null } as totalWorkTimeState

const totalWorkTimeSlice = createSlice({
  name: 'totalWorkTime',
  initialState,
  reducers: {
    setTotalWorkTime (state, action: PayloadAction<ITotalWorkTime>) {
      state.total = action.payload
    }
  }
})

export const { setTotalWorkTime } = totalWorkTimeSlice.actions
export default totalWorkTimeSlice.reducer
