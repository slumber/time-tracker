import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { IGroup } from '../../types'

interface OwnedGroupsState {
  ownedGroups: IGroup[]
}

const initialState = { ownedGroups: [] } as OwnedGroupsState

const ownedGroupsSlice = createSlice({
  name: 'ownedGroups',
  initialState,
  reducers: {
    setOwnedGroups (state, action: PayloadAction<IGroup[]>) {
      state.ownedGroups = action.payload
    }
  }
})

export const { setOwnedGroups } = ownedGroupsSlice.actions
export default ownedGroupsSlice.reducer
