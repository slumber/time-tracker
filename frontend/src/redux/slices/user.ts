import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { IUser } from '../../types'

interface UserState {
  user: IUser | null
}

const initialState = { user: null } as UserState

const userSlice = createSlice({
  name: 'user',
  initialState,
  reducers: {
    setUser (state, action: PayloadAction<IUser | null>) {
      state.user = action.payload
    }
  }
})

export const { setUser } = userSlice.actions
export default userSlice.reducer
