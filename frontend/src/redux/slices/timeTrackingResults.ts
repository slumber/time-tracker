import { createSlice, PayloadAction } from '@reduxjs/toolkit'
import { ITimeTrackingResults } from '../../types'

interface timeTrackingResultsState {
  results: ITimeTrackingResults[] | null
}

const initialState = { results: null } as timeTrackingResultsState

const timeTrackingResultsSlice = createSlice({
  name: 'timeTrackingResults',
  initialState,
  reducers: {
    setTimeTrackingResults (state, action: PayloadAction<ITimeTrackingResults[]>) {
      state.results = action.payload
    }
  }
})

export const { setTimeTrackingResults } = timeTrackingResultsSlice.actions
export default timeTrackingResultsSlice.reducer
