import { dispatchError, dispatchSuccess } from '../commonFunctions/handleSnackbars'
import { IGroup, IUser } from '../types'
import { axiosInstance } from './axiosConfig'

export const groupCachedKey = 'group-cached'

export const getUser = async (setUser: (arg: IUser) => void, done: () => void) => {
  try {
    const response = await axiosInstance.get('/auth/user', { headers: { 'X-Requested-With': 'JavaScript' } })
    setUser(response.data)
    done()
  } catch (error) {
    dispatchError('session-error')
    done()
  }
}
export const updateUser = async (user: IUser, setUser: (arg: IUser) => void, ignoreSnackbar?: boolean) => {
  try {
    const response = await axiosInstance.put('/user', user)
    setUser(response.data)
    !ignoreSnackbar && dispatchSuccess('changes-saved')
  } catch (error) {
    dispatchError('update-error')
  }
}
export const getOwnedGroups = async (
  setOwnedGroups: (data: IGroup[]) => void,
  done: (groups: IGroup[]) => void
) => {
  try {
    const response = await axiosInstance.get('/user/ownedGroups')
    const groups = (response.data as IGroup[]).filter((x) => !!x)
    if (groups.length > 0) {
      setOwnedGroups(groups)
      localStorage.setItem(groupCachedKey, JSON.stringify(groups))
      done(groups)
      return
    }
    done([])
  } catch (error) {
    dispatchError('get-owned-groups-error')
  }
}

export const addUserToGroup = async (
  username: string,
  groupName: string
) => {
  const payload = {
    nickname: username,
    groupNames: [groupName]
  }
  try {
    await axiosInstance.post('/user/groupPermissions', payload)
  } catch (error) {
    dispatchError('get-owned-groups-error')
  }
}

export const removeUserFromGroup = async (
  username: string,
  groupName: string
) => {
  const payload = {
    nickname: username,
    groupNames: [groupName]
  }
  try {
    await axiosInstance.delete('/user/groupPermissions', {
      headers: {
        'Content-Type': 'application/json',
        'Access-Control-Allow-Origin': '*'
      },
      data: payload
    })
  } catch (error) {
    dispatchError('get-owned-groups-error')
  }
}
