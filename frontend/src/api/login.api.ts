import { axiosInstance } from './axiosConfig'

import { dispatchError } from '../commonFunctions/handleSnackbars'

export const login = async () => {
  try {
    await axiosInstance.get('/auth', { headers: { 'X-Requested-With': 'JavaScript' } }).catch(reason => {
      if (reason.response.status === 499) {
        window.location.assign(`${(window as any)._env_.API_PATH}/auth`)
      }
    })
  } catch (error) {
    dispatchError('login-error')
  }
}

export const logout = async () => {
  try {
    await axiosInstance.post('/auth/logout', {}).then(response => {
      if (response.status === 200) {
        window.location.assign('/')
      }
    })
  } catch (error) {
    dispatchError('login-error')
  }
}
