import axios from 'axios'
import { getUser, updateUser } from './user.api'
import { IGroup, IUser } from '../types'
const mockedAxios = axios as jest.Mocked<typeof axios>

const user = {
  id: 1,
  email: 'test@email.com',
  name: 'John',
  nickname: 'John nickname',
  profile: 'profile',
  picture: 'picture',
  hourlyWage: 30,
  groups: ['group'],
  defaultGroup: 'group',
  startDate: null
}
let fetchedUser: IUser | null = null
const setUser = (responseUser: IUser) => {
  fetchedUser = responseUser
}

// TEST GET USER

describe('getUser', () => {
  describe('when API call is successful', () => {
    it('should return one user', async () => {
      // given
      mockedAxios.get.mockResolvedValueOnce({ data: user })

      // when
      await getUser(setUser, () => console.log('done'))

      // then
      expect(axios.get).toHaveBeenCalledWith('/auth/user', { headers: { 'X-Requested-With': 'JavaScript' } })
      expect(axios.get).toHaveBeenCalledTimes(1)
      expect(fetchedUser).toEqual(user)
    })
  })
  describe('when API call fails', () => {
    it('should return null for user', async () => {
      // given
      const message = 'Network Error'
      mockedAxios.get.mockRejectedValueOnce(new Error(message))

      // when
      await getUser(setUser, () => console.log('done'))

      // then
      expect(axios.get).toHaveBeenCalledWith('/auth/user', { headers: { 'X-Requested-With': 'JavaScript' } })
      expect(axios.get).toHaveBeenCalledTimes(1)
      expect(fetchedUser).toEqual(null || user)
    })
  })
})

// TEST PUT USER

describe('updateUser', () => {
  describe('when API call is successful', () => {
    it('should update user', async () => {
      // given
      mockedAxios.put.mockResolvedValueOnce({ data: user })

      // when
      await updateUser(user, setUser)

      // then
      expect(axios.put).toHaveBeenCalledWith('/user', user)
      expect(axios.put).toHaveBeenCalledTimes(1)
      expect(fetchedUser).toEqual(user)
    })
  })
  describe('when API call fails', () => {
    it('should return previous state', async () => {
      // given
      const message = 'Network Error'
      mockedAxios.put.mockRejectedValueOnce(new Error(message))

      // when
      await updateUser(user, setUser)

      // then
      expect(axios.put).toHaveBeenCalledWith('/user', user)
      expect(axios.put).toHaveBeenCalledTimes(1)
      expect(fetchedUser).toEqual(user || null)
    })
  })
})

// TEST GET USER'S OWNED GROUPS

const ownedGroups = [
  {
    id: 1,
    name: "John's Group",
    webUrl: 'url',
    fullPath: 'fullPath.com',
    users: [
      {
        nickname: 'John nickname',
        profile: 'profile',
        picture: 'picture',
        name: 'John'
      }
    ]
  }
]

const fetchedGroups: IGroup[] = []

describe('getOwnedGroups', () => {
  describe('when API call is successful', () => {
    it('should return array of groups', async () => {
      // given
      mockedAxios.get.mockResolvedValueOnce({ data: ownedGroups })

      // when

      // then
      expect(axios.get).toHaveBeenCalledWith(`/user/${user.name}/ownedGroups`)
      expect(axios.get).toHaveBeenCalledTimes(1)
      expect(fetchedGroups).toEqual(ownedGroups)
    })
  })
  describe('when API call fails', () => {
    it('should return previous state', async () => {
      // given
      const message = 'Network Error'
      mockedAxios.get.mockRejectedValueOnce(new Error(message))

      // when

      // then
      expect(axios.get).toHaveBeenCalledWith(`/user/${user.name}/ownedGroups`)
      expect(axios.get).toHaveBeenCalledTimes(1)
      expect(fetchedGroups).toEqual(ownedGroups || [])
    })
  })
})
