import { axiosInstance } from './axiosConfig'
import { ITimeTrackingResults, ITotalWorkTime } from '../types'
import { dispatchError } from '../commonFunctions/handleSnackbars'

export const getUserTimeTrackingResults = async (user: string, setResults?: (data: ITimeTrackingResults[]) => void) => {
  try {
    const response = await axiosInstance.get(`/user/${user}/timelog?this-month=true`)
    if (setResults) {
      await setResults(response.data)
    } else {
      return response.data
    }
  } catch (error) {
    dispatchError('user-timelog-get-err')
  }
}
export const getTimelogsPerGroup = async (
  user: string,
  group: string,
  setResults: (data: ITimeTrackingResults[]) => void
) => {
  try {
    const response = await axiosInstance.get(`/user/${user}/timelog?this-month=true&group=${encodeURIComponent(group)}`)
    await setResults(response.data)
  } catch (error) {
    dispatchError('user-timelog-get-err')
  }
}
export const getUserTotalTime = async (user: string, setTime: (data: ITotalWorkTime) => void) => {
  try {
    const response = await axiosInstance.get(`/user/${user}/time-sum?this-month=true`)
    setTime(response.data)
  } catch (error) {
    dispatchError('total-time-get-err')
  }
}
export const getUserTotalTimePerGroup = async (
  user: string,
  setTime: (data: ITotalWorkTime) => void,
  group: string
) => {
  try {
    const response = await axiosInstance.get(`/user/${user}/timelog?this-month=true&group=${encodeURIComponent(group)}`)
    setTime(response.data)
  } catch (error) {
    dispatchError('total-time-get-err')
  }
}
export const getTotalGroupTime = async (group: string, setResults?: (data: ITimeTrackingResults[]) => void) => {
  try {
    const response = await axiosInstance.get(`/user/group/${encodeURIComponent(group)}/timelog?this-month=true`)
    if (setResults) {
      await setResults(response.data)
    } else {
      return response.data
    }
  } catch (error) {
    dispatchError('user-timelog-get-err')
  }
}

export const getUserTotalTimeWithTimeRange = async (
  user: string,
  setTime: (data: ITotalWorkTime) => void,
  start: string,
  end: string
) => {
  try {
    const response = await axiosInstance.get(`/user/${user}/time-sum?start=${start}&end=${end}`)
    setTime(response.data)
  } catch (error) {
    dispatchError('total-time-get-err')
  }
}
export const getUserTotalTimeWithTimeRangeAndGroup = async (
  user: string,
  setTime: (data: number) => void,
  start: string,
  end: string,
  group: string
) => {
  try {
    const response = await axiosInstance.get(
      `/user/${user}/time-sum?start=${start}&end=${end}&group=${encodeURIComponent(group)}`
    )
    setTime(response.data.seconds)
  } catch (error) {
    dispatchError('total-time-get-err')
  }
}

export interface IGetUserTimelog {
  user: string
  setResults: (data: ITimeTrackingResults[]) => void
  start?: string
  end?: string
  closePopup?: () => void
}

export const getUserTimelog: (args: IGetUserTimelog) => void = async ({ user, start, end, setResults, closePopup }) => {
  try {
    const response = await axiosInstance.get(
      start && end ? `/user/${user}/timelog?start=${start}&end=${end}` : `/user/${user}/timelog?this-month=true`
    )
    setResults(response.data)
    closePopup && closePopup()
  } catch (error) {
    dispatchError('timelog-time-range-err')
  }
}

export const getUserDefaultGroupTimelog: (username: string) => Promise<any[] | null> = async (username) => {
  try {
    const response: { data: any[] } = await axiosInstance.get(`/user/${username}/timelog?this-month=true`)
    return response.data
  } catch (error) {
    console.log(error)
    dispatchError('timelog-general-err')
    return null
  }
}

export const getResultsWithTimeRangeAndGroup = async (
  user: string,
  start: string,
  end: string,
  setResults: (data: ITimeTrackingResults[]) => void,
  group: string,
  closePopup?: () => void
) => {
  try {
    const response = await axiosInstance.get(
      `/user/${user}/timelog?start=${start}&end=${end}&group=${encodeURIComponent(group)}`
    )
    setResults(response.data)
    closePopup && closePopup()
  } catch (error) {
    dispatchError('timelog-time-range-err')
  }
}
