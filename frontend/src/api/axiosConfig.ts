import axios from 'axios'
import { hideLoader, showLoader } from '../commonFunctions/handleLoader'

export const axiosInstance = axios.create({
  headers: {
    'Content-Type': 'application/json',
    'Access-Control-Allow-Origin': '*'
  },
  withCredentials: true,
  baseURL: (window as any)._env_?.API_PATH
})
axiosInstance.interceptors.request.use(function (config) {
  showLoader()
  return config
})
axiosInstance.interceptors.response.use(
  function (response) {
    hideLoader()
    return response
  },
  function (error) {
    hideLoader()
    return Promise.reject(error)
  }
)
