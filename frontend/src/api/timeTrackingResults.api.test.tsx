import axios from 'axios'
import {
  getUserTimeTrackingResults,
  getTimelogsPerGroup,
  getUserTotalTime,
  getUserTotalTimePerGroup,
  getUserTotalTimeWithTimeRange,
  getUserTotalTimeWithTimeRangeAndGroup,
  getResultsWithTimeRangeAndGroup,
  getUserTimelog
} from './timeTrackingResults.api'
import { ITimeTrackingResults, ITotalWorkTime } from '../types'
const mockedAxios = axios as jest.Mocked<typeof axios>

const user = 'John'
const group = 'My group'
const results = {
  issue: { title: 'Issue title', id: 1, webUrl: 'url.com' },
  spentAt: '21-07-2021',
  timeSpent: 600,
  mergeRequest: { title: 'MR title', id: 1, webUrl: 'url.com' },
  note: null,
  user
}
let fetchedResults: ITimeTrackingResults[] | null = null
const setResults = (responseResults: ITimeTrackingResults[]) => {
  fetchedResults = responseResults
}

const time = {
  seconds: 3000,
  minutes: 50,
  hours: 0.83
}
const timeInSeconds = 3000

let fetchedTime: ITotalWorkTime | null = null
const setTime = (responseTime: ITotalWorkTime) => {
  fetchedTime = responseTime
}

let fetchedTimeInSeconds: number = 0
const setTimeInSeconds = (responseTime: number) => {
  fetchedTimeInSeconds = responseTime
}

const start = '2021-08-03T00:00:00.000'
const end = '2021-08-03T23:59:59.000'

// TEST GET timelogs

describe('getUserTimeTrackingResults', () => {
  describe('when API call is successful', () => {
    it('should return timelogs', async () => {
      // given
      mockedAxios.get.mockResolvedValueOnce({ data: results })

      // when
      await getUserTimeTrackingResults(user, setResults)

      // then
      expect(axios.get).toHaveBeenCalledWith(`/user/${user}/timelog?this-month=true`)
      expect(axios.get).toHaveBeenCalledTimes(1)
      expect(fetchedResults).toEqual(results)
    })
  })
  describe('when API call fails', () => {
    it('should return previous state', async () => {
      // given
      const message = 'Network Error'
      mockedAxios.get.mockRejectedValueOnce(new Error(message))

      // when
      await getUserTimeTrackingResults(user, setResults)

      // then
      expect(axios.get).toHaveBeenCalledWith(`/user/${user}/timelog?this-month=true`)
      expect(axios.get).toHaveBeenCalledTimes(1)
      expect(fetchedResults).toEqual(null || results)
    })
  })
})

// TEST GET timelogs per group

describe('getTimelogsPerGroup', () => {
  describe('when API call is successful', () => {
    it('should return timelogs for selected group', async () => {
      // given
      mockedAxios.get.mockResolvedValueOnce({ data: results })

      // when
      await getTimelogsPerGroup(user, group, setResults)

      // then
      expect(axios.get).toHaveBeenCalledWith(`/user/${user}/timelog?this-month=true&group=${group}`)
      expect(axios.get).toHaveBeenCalledTimes(1)
      expect(fetchedResults).toEqual(results)
    })
  })
  describe('when API call fails', () => {
    it('should return previous state for timelogs per group', async () => {
      // given
      const message = 'Network Error'
      mockedAxios.get.mockRejectedValueOnce(new Error(message))

      // when
      await getTimelogsPerGroup(user, group, setResults)

      // then
      expect(axios.get).toHaveBeenCalledWith(`/user/${user}/timelog?this-month=true&group=${group}`)
      expect(axios.get).toHaveBeenCalledTimes(1)
      expect(fetchedResults).toEqual(null || results)
    })
  })
})

// TEST GET timelogs with time range

describe('getResultsWithTimeRange', () => {
  describe('when API call is successful', () => {
    it('should return timelogs for selected time range', async () => {
      // given
      mockedAxios.get.mockResolvedValueOnce({ data: results })

      // when
      await getUserTimelog({ user, start, end, setResults })

      // then
      expect(axios.get).toHaveBeenCalledWith(`/user/${user}/timelog?start=${start}&end=${end}`)
      expect(axios.get).toHaveBeenCalledTimes(1)
      expect(fetchedResults).toEqual(results)
    })
  })
  describe('when API call fails', () => {
    it('should return previous state in selected time range', async () => {
      // given
      const message = 'Network Error'
      mockedAxios.get.mockRejectedValueOnce(new Error(message))

      // when
      await getUserTimelog({ user, start, end, setResults })

      // then
      expect(axios.get).toHaveBeenCalledWith(`/user/${user}/timelog?start=${start}&end=${end}`)
      expect(axios.get).toHaveBeenCalledTimes(1)
      expect(fetchedResults).toEqual(null || results)
    })
  })
})

// TEST GET timelogs with time range per group

describe('getResultsWithTimeRangeAndGroup', () => {
  describe('when API call is successful', () => {
    it('should return timelogs for group in selected time range', async () => {
      // given
      mockedAxios.get.mockResolvedValueOnce({ data: results })

      // when
      await getResultsWithTimeRangeAndGroup(user, start, end, setResults, group)

      // then
      expect(axios.get).toHaveBeenCalledWith(`/user/${user}/timelog?start=${start}&end=${end}&group=${group}`)
      expect(axios.get).toHaveBeenCalledTimes(1)
      expect(fetchedResults).toEqual(results)
    })
  })
  describe('when API call fails', () => {
    it('should return previous state for group in selected time range', async () => {
      // given
      const message = 'Network Error'
      mockedAxios.get.mockRejectedValueOnce(new Error(message))

      // when
      await getResultsWithTimeRangeAndGroup(user, start, end, setResults, group)

      // then
      expect(axios.get).toHaveBeenCalledWith(`/user/${user}/timelog?start=${start}&end=${end}&group=${group}`)
      expect(axios.get).toHaveBeenCalledTimes(1)
      expect(fetchedResults).toEqual(null || results)
    })
  })
})

// TEST GET total time

describe('getUserTotalTime', () => {
  describe('when API call is successful', () => {
    it('should return total time for user', async () => {
      // given
      mockedAxios.get.mockResolvedValueOnce({ data: time })

      // when
      await getUserTotalTime(user, setTime)

      // then
      expect(axios.get).toHaveBeenCalledWith(`/user/${user}/time-sum?this-month=true`)
      expect(axios.get).toHaveBeenCalledTimes(1)
      expect(fetchedTime).toEqual(time)
    })
  })
  describe('when API call fails', () => {
    it('should return previous state for total time', async () => {
      // given
      const message = 'Network Error'
      mockedAxios.get.mockRejectedValueOnce(new Error(message))

      // when
      await getUserTotalTime(user, setTime)

      // then
      expect(axios.get).toHaveBeenCalledWith(`/user/${user}/time-sum?this-month=true`)
      expect(axios.get).toHaveBeenCalledTimes(1)
      expect(fetchedTime).toEqual(null || time)
    })
  })
})

// TEST GET total time per group

describe('getUserTotalTimePerGroup', () => {
  describe('when API call is successful', () => {
    it('should return total time for user in selected group', async () => {
      // given
      mockedAxios.get.mockResolvedValueOnce({ data: time })

      // when
      await getUserTotalTimePerGroup(user, setTime, group)

      // then
      expect(axios.get).toHaveBeenCalledWith(`/user/${user}/time-sum?this-month=true&group=${group}`)
      expect(axios.get).toHaveBeenCalledTimes(1)
      expect(fetchedTime).toEqual(time)
    })
  })
  describe('when API call fails', () => {
    it('should return previous state for total time for user in selected group', async () => {
      // given
      const message = 'Network Error'
      mockedAxios.get.mockRejectedValueOnce(new Error(message))

      // when
      await getUserTotalTimePerGroup(user, setTime, group)

      // then
      expect(axios.get).toHaveBeenCalledWith(`/user/${user}/time-sum?this-month=true&group=${group}`)
      expect(axios.get).toHaveBeenCalledTimes(1)
      expect(fetchedTime).toEqual(null || time)
    })
  })
})

// TEST GET total time with time range

describe('getUserTotalTimeWithTimeRange', () => {
  describe('when API call is successful', () => {
    it('should return total time for user in selected time range', async () => {
      // given
      mockedAxios.get.mockResolvedValueOnce({ data: time })

      // when
      await getUserTotalTimeWithTimeRange(user, setTime, start, end)

      // then
      expect(axios.get).toHaveBeenCalledWith(`/user/${user}/time-sum?start=${start}&end=${end}`)
      expect(axios.get).toHaveBeenCalledTimes(1)
      expect(fetchedTime).toEqual(time)
    })
  })
  describe('when API call fails', () => {
    it('should return previous state for total time for user in selected time range', async () => {
      // given
      const message = 'Network Error'
      mockedAxios.get.mockRejectedValueOnce(new Error(message))

      // when
      await getUserTotalTimeWithTimeRange(user, setTime, start, end)

      // then
      expect(axios.get).toHaveBeenCalledWith(`/user/${user}/time-sum?start=${start}&end=${end}`)
      expect(axios.get).toHaveBeenCalledTimes(1)
      expect(fetchedTime).toEqual(null || time)
    })
  })
})

// TEST GET total time with time range and group

describe('getUserTotalTimeWithTimeRangeAndGroup', () => {
  describe('when API call is successful', () => {
    it('should return total time for user in selected time range and group', async () => {
      // given
      mockedAxios.get.mockResolvedValueOnce({ data: time })

      // when
      await getUserTotalTimeWithTimeRangeAndGroup(user, setTimeInSeconds, start, end, group)

      // then
      expect(axios.get).toHaveBeenCalledWith(`/user/${user}/time-sum?start=${start}&end=${end}&group=${group}`)
      expect(axios.get).toHaveBeenCalledTimes(1)
      expect(fetchedTimeInSeconds).toEqual(timeInSeconds)
    })
  })
  describe('when API call fails', () => {
    it('should return previous state for total time for user in selected time range and group', async () => {
      // given
      const message = 'Network Error'
      mockedAxios.get.mockRejectedValueOnce(new Error(message))

      // when
      await getUserTotalTimeWithTimeRangeAndGroup(user, setTimeInSeconds, start, end, group)

      // then
      expect(axios.get).toHaveBeenCalledWith(`/user/${user}/time-sum?start=${start}&end=${end}&group=${group}`)
      expect(axios.get).toHaveBeenCalledTimes(1)
      expect(fetchedTimeInSeconds).toEqual(0 || timeInSeconds)
    })
  })
})
