export const notificationTime = 4000
export const APP_VERSION = (window as any)._env_.APP_VERSION
export const navbarWidth = '80px'
export const LOGO = 'Time Tracker'
