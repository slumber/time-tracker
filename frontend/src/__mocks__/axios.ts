const mockAxios: jest.Mocked<typeof import('axios').default> = jest.createMockFromModule('axios')
mockAxios.create = jest.fn(() => mockAxios)

export default mockAxios
