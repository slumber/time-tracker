export interface IOwnedGroupsUsers {
  name: string
  nickname: string
  profile: string
  picture: string
}
export interface IUser {
  id: number
  email: string
  name: string
  nickname: string
  profile: string
  picture: string
  hourlyWage: number
  groups: string[]
  defaultGroup: string
  startDate: string | null
}
export interface IIssue {
  title: string
  id: string
  webUrl: string
  dueDate: Date
}
export interface IMergeRequest {
  title: string
  id: string
  webUrl: string
}
export interface ITimeTrackingResultUser {
  username: string
  name: string
  webUrl: string
  id: string
}

export interface ITimeTrackingResults {
  issue: IIssue | null
  spentAt: string
  timeSpent: number
  mergeRequest: IMergeRequest | null
  note: string | null
  user: ITimeTrackingResultUser | null
  title?: string
}
export interface ITotalWorkTime {
  seconds: number | null
  minutes: number | null
  hours: number | null
}

export interface IHistory {
  date: string
  originalDateFormat?: string
  time: string
  id: string
}

export interface ITimeRange {
  start: string
  end: string
}

export enum SORT_TYPE {
  TYPE,
  DATE,
  TIME,
  NAME,
  FILTERS_CHECKBOX,
  MEMBERS,
  USER
}

export interface IGroup{
 id: string,
 name: string,
 fullPath: string,
 webUrl: string,
 users: IOwnedGroupsUsers[],
 subgroups: any,
}

export interface IGroupWithSubgroups extends IGroup {
  subgroups: IGroup[]
}
