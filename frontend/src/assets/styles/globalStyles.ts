import { createGlobalStyle } from 'styled-components'
import { SIZES } from './mediaQueries'

export const GlobalStyle = createGlobalStyle`
  *{
    box-sizing: border-box;
    margin: 0;
    padding: 0;
    font-family: 'Poppins', sans-serif;
    font-weight: 400;
    text-decoration: none;
  }
  html {
    font-size: 14px;
    @media only screen and ${SIZES.tablet} {
      font-size: 16px;
    }
  }
  body{
    min-height: 100vh;
    width: 100%;
    background-color: #F5F5F5;
  }
  .App, #root {
    min-height: 100vh;
    width: 100%;
    overflow: hidden;
  }
  .App {
    display: flex;
    flex-direction: column;
  }
  .main {
    flex-grow: 1;
  }
  h1 {
    font-size: 36px;
    font-weight: 600;
    color: #000;
  }
  h2 {
    font-size: 20px;
    font-weight: 500;
  }
  img {
    width: 100%;
  }
  a {
    color: inherit;
    border: none;
  }
  ul {
    list-style: none;
  }
  .button {
    cursor: pointer;
    color: #fff;
  }
  .logo {
    color: white;
    font-family: 'Poppins', sans-serif;
    font-size: 20px;
    text-transform: uppercase;
  }
  .MuiAlert-message, .MuiAlert-icon path, .MuiAlert-action path, .MuiButton-label {
    color: #fff;
  }
  .MuiCircularProgress-colorPrimary {
    color: #000;
}
`
