import styled from 'styled-components'

export const Container = styled.div`
  width: 100%;
  padding: 25px 24px 12px 104px;
  section {
    padding: 0;
    align-items: flex-start;
    height: 100%;
  }
  .MuiTableContainer-root {
    border-radius: 20px;
    width: calc(100% - 24px);
    margin: 0 auto;
    background-color: #fff;
    &:last-child {
      margin-left: 24px;
    }
    .MuiTableHead-root {
      background-color: #22325f;
    }
    .MuiTableCell-head,
    .MuiTableSortLabel-root {
      color: #000;
      font-weight: 600;
      font-family: 'Poppins', sans-serif;
    }
    .MuiTableCell-body {
      font-weight: 400;
      font-size: 16px;
      font-family: 'Poppins', sans-serif;
    }
    .MuiTableCell-root {
      border-bottom: 1px solid #F5F5F5;
    }
    .issuesHead {
      background-color: #fff;
    }
    .historyHead {
      background-color: transparent;
      .MuiTableCell-head {
        color: black;
      }
    }
  }
  .MuiIconButton-root:hover {
    border-radius: 10px;
  }
  .thickBorder {
    border-bottom: 1px solid black;
    .MuiTableCell-root {
      border-bottom: 1px solid black;
    }
  }
  .link {
    color: #000;
    text-decoration: underline;
  }
  .MuiTablePagination-root {
    width: 100%;
  }
`
export const TableWrapper = styled.section`
  width: 100%;
  height: 100%;
  padding: 64px 0 0;
  display: flex;
  justify-content: space-between;
  align-items: flex-start;
  flex-wrap: wrap;
  .historyExpandBtn {
    font-size: 14px;
  }
  .historyRow:last-child {
    .historyCell {
      border: none;
    }
  }
  .historyRow {
    .MuiTableCell-root {
      font-size: 12px;
    }
  }
  .historyHead {
    .MuiTableCell-root {
      font-size: 13px;
    }
  }
  .MuiTableCell-root {
    transition: 0.4s;
  }
  .memberMargin1 {
    width: 45%;
    padding-left: 64px;
  }
  .memberMargin2 {
    width: 45%;
    padding-left: 128px;
  }
  .memberMargin3 {
    width: 45%;
    padding-left: 192px;
  }
  .margin1 {
    width: 45%;
  }
  .margin2 {
    width: 45%;
    padding-left: 64px;
  }
  .margin3 {
    width: 45%;
    padding-left: 128px;
  }
  .margin4 {
    width: 45%;
    padding-left: 192px;
  }
  .secondCell {
    width: 35%;
  }
  .thirdCell {
    width: 20%;
  }
  .type-cell,
  .time-cell,
  .date-cell {
    width: 15%;
  }
  .link-cell {
    width: 10%;
  }
  .name-cell {
    width: 25%;
  }
  .project-cell {
    width: 20%;
  }
  .collapse {
    background-color: rgba(200, 200, 200, 0.1);
    width: 100%;
  }
  .userCell {
    padding-left: 100px;
  }
  .click {
    cursor: pointer;
  }
`

export const NoResultsMessage = styled.div`
  display: flex;
  flex-direction: column;
  margin: 0 auto;
  align-items: center;
  margin-top: 100px;
  .no-results-img {
    width: 200px;
  }
  .no-results {
    margin-top: 10px;
    text-align: center;
    font-size: 20px;
    width: 290px;
    color: #000;
  }
`
