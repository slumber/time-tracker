import styled from 'styled-components'

export const ResultsHeader = styled.div`
  margin-bottom: 20px;
  margin-left: 16px;
  display: flex;
  justify-content: space-between;
  h2 {
    color: #fff;
    text-align: center;
  }
  h1 {
    margin-bottom: 8px;
  }
  h2 {
    font-size: 14px;
  }
  .filters-wrapper {
    display: flex;
  }
  .filter {
    position: relative;
  }
  .active {
    border: 1px solid #5932EA;
  }
`

export const FilterBtn = styled.button`
  display: flex;
  align-items: center;
  margin-right: 12px;
  box-sizing: borde-box;
  background-color: #fff;
  border: none;
  font-size: 14px;
  padding: 12px;
  border-radius: 20px;
  border: 1px solid #F5F5F5;
  cursor: pointer;
  transition: 0.3s;
  :hover {
    background-color: #EFF2FB;
  }
  .icon {
    width: 16px;
    height: 16px;
    margin-left: 8px;
  }
  .icon-check {
    width: 16px;
    height: 16px;
    margin-right: 8px;
  }
  .calendar-icon {
    width: 16px;
    height: 16px;
    margin-right: 8px;
  }
`
