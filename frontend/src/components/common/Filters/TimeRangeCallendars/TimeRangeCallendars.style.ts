import styled from 'styled-components'

export const DatePickerWrapper = styled.div`
  background-color: #fff;
  border-radius: 12px;
  padding: 24px;
  display: flex;
  flex-direction: column;
  justify-content: center;
  align-items: flex-end;
  position: absolute;
  z-index: 2;
  top: 53px;
  box-shadow: 0px 8px 24px rgba(0, 0, 0, 0.12);
  .MUIbutton {
    margin: 0 16px 8px 0;
    width: 85px;
    border-radius: 38px;
    background-color: #EFF2FB;
  }
  .MuiButton-label {
    color: #000;
  }
  .search .MuiButton-label {
    color: #fff;
  }
  .search {
    background-color: #7047EB;
    color: #000;
  }
  .buttonsWrapper {
    margin-top: 16px;
  }
  .rdrStaticRangeSelected {
    color: #000 !important;
  }
  .rdrInRange,
  .rdrEndEdge,
  .rdrStartEdge {
    background-color: #7047EB !important;
  }
`
