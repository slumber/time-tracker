import * as React from 'react'
import { useLocation, useParams } from 'react-router-dom'
import { useIntl } from 'react-intl'
import { format } from 'date-fns'
import { Button } from '@material-ui/core'

import {
  getUserTimelog,
  getUserTotalTimeWithTimeRange,
  getUserTotalTimeWithTimeRangeAndGroup,
  getResultsWithTimeRangeAndGroup
} from '../../../../api/timeTrackingResults.api'
import { useAppDispatch } from '../../../../redux/hooks'
import { setTimeTrackingResults } from '../../../../redux/slices/timeTrackingResults'
import { setTotalWorkTime } from '../../../../redux/slices/totalWorkTime'
import { useTimeForUserPerGroupContext } from '../../../Results/context/timeForUserPerGroup'
import { IGroup, ITimeRange } from '../../../../types'

import DatePicker from '../../../Results/ResultsPerDay/DatePicker/DatePicker'
import { DatePickerWrapper } from './TimeRangeCallendars.style'
import { dateFormatter } from '../../../commonFunctions/dateFormatter'

export interface TimeRangeCallendarsProps {
  close: () => void
  setDateStart?: React.Dispatch<React.SetStateAction<string>>
  setDateEnd?: React.Dispatch<React.SetStateAction<string>>
  getTimeRange?: (args: ITimeRange) => void
  forResults?: boolean
}

const TimeRangeCallendars: React.FC<TimeRangeCallendarsProps> = ({
  close,
  setDateStart,
  setDateEnd,
  getTimeRange,
  forResults
}) => {
  const dispatch = useAppDispatch()
  const { username } = useParams<{ username: string }>()
  const location = useLocation<{ group: IGroup }>()
  const groupState = location?.state?.group
  const intl = useIntl()
  const timeContext = useTimeForUserPerGroupContext()

  const now = React.useRef(new Date())
  const [range, setRange] = React.useState({ start: now.current, end: now.current })

  const search = async () => {
    const start = `${format(range.start, 'yyyy-MM-dd')}T${range.start.toLocaleTimeString('en-GB')}.000`
    const end = `${format(range.end, 'yyyy-MM-dd')}T${range.end.toLocaleTimeString('en-GB')}.000`
    if (groupState) {
      await getResultsWithTimeRangeAndGroup(
        username,
        start,
        end,
        (response) => dispatch(setTimeTrackingResults(response)),
        groupState.fullPath,
        close
      )
      await getUserTotalTimeWithTimeRangeAndGroup(
        username,
        (response) => timeContext.setTotalTime(response),
        start,
        end,
        groupState.fullPath
      )
    } else if (getTimeRange) {
      getTimeRange({ start, end })
      close()
    } else {
      await getUserTimelog({
        user: username,
        start,
        end,
        setResults: (response) => dispatch(setTimeTrackingResults(response)),
        closePopup: close
      })
      getUserTotalTimeWithTimeRange(username, (response) => dispatch(setTotalWorkTime(response)), start, end)
    }
  }

  React.useEffect(() => {
    if (setDateStart && setDateEnd && range) {
      setDateStart(dateFormatter(`${format(range.start, 'yyyy-MM-dd')}T${range.start.toLocaleTimeString('en-GB')}.000`))
      setDateEnd(dateFormatter(`${format(range.end, 'yyyy-MM-dd')}T${range.end.toLocaleTimeString('en-GB')}.000`))
    }
  })

  return (
    <DatePickerWrapper className='datePickerWrapper'>
      <DatePicker setRange={setRange} forResults={forResults} />
      <div className='buttonsWrapper'>
        <Button className='MUIbutton' onClick={close}>
          {intl.formatMessage({ id: 'cancel' })}
        </Button>
        <Button className='search MUIbutton' onClick={search}>
          {intl.formatMessage({ id: 'save' })}
        </Button>
      </div>
    </DatePickerWrapper>
  )
}

export default TimeRangeCallendars
