import React from 'react'
import { ResultsHeader } from './Filters.style'

const Filters: React.FC = ({ children }) => {
  return (
    <ResultsHeader>
      <div className='filters-wrapper'>{children}</div>
    </ResultsHeader>
  )
}

export default Filters
