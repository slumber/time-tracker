import React from 'react'
import { useIntl } from 'react-intl'
import { format } from 'date-fns'
import calendarIcon from '../../../assets/images/calendar.png'
import { ITimeRange } from '../../../types'

import { dateFormatter } from '../../commonFunctions/dateFormatter'
import TimeRangeCallendars from './TimeRangeCallendars/TimeRangeCallendars'
import { FilterBtn } from './Filters.style'

interface CalendarButtonProps {
  getTimeRange?: (args: ITimeRange) => void
  forResults: boolean
}

const Calendar: React.FC<CalendarButtonProps> = ({ getTimeRange, forResults }) => {
  const intl = useIntl()
  const [dateStart, setDateStart] = React.useState('')
  const [dateEnd, setDateEnd] = React.useState('')
  const [selectDay, setSelectDay] = React.useState(false)

  React.useEffect(() => {
    if ((sessionStorage.getItem('dateStartGroups') && sessionStorage.getItem('dateEndGroups') && getTimeRange)) {
      const startDate = new Date(sessionStorage.getItem('dateStartGroups') || '')
      const endDate = new Date(sessionStorage.getItem('dateEndGroups') || '')
      const start = `${format(startDate, 'yyyy-MM-dd')}T${startDate.toLocaleTimeString('en-GB')}.000`
      const end = `${format(endDate, 'yyyy-MM-dd')}T${endDate.toLocaleTimeString('en-GB')}.000`
      setDateStart(dateFormatter(start))
      setDateEnd(dateFormatter(end))
      getTimeRange({ start, end })
    } else if (sessionStorage.getItem('dateStartResults') && sessionStorage.getItem('dateEndResults') && !getTimeRange) {
      const startDate = new Date(sessionStorage.getItem('dateStartResults') || '')
      const endDate = new Date(sessionStorage.getItem('dateEndResults') || '')
      const start = `${format(startDate, 'yyyy-MM-dd')}T${startDate.toLocaleTimeString('en-GB')}.000`
      const end = `${format(endDate, 'yyyy-MM-dd')}T${endDate.toLocaleTimeString('en-GB')}.000`
      setDateStart(dateFormatter(start))
      setDateEnd(dateFormatter(end))
    }
  }, [])

  return (
    <div className='filter'>
      {selectDay && (
        <TimeRangeCallendars
          close={() => setSelectDay(false)}
          setDateStart={setDateStart}
          setDateEnd={setDateEnd}
          getTimeRange={getTimeRange}
          forResults={forResults}
        />
      )}
      <FilterBtn className='selectDayBtn' onClick={() => setSelectDay(!selectDay)}>
        <img className='calendar-icon' src={calendarIcon} alt='calendar' />{' '}
        {!dateStart
          ? (
          <span>{intl.formatMessage({ id: 'select-day' })}</span>
            )
          : (
          <span>
            {dateStart} - {dateEnd}
          </span>
            )}
      </FilterBtn>
    </div>
  )
}

export default Calendar
