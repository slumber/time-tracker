import styled from 'styled-components'

export const ButtonCSV = styled.button`
  background-color: #fff;
  padding: 12px;
  border: none;
  cursor: pointer;
  border-radius: 70px;
  height: 47px;
  .icon {
    width: 24px;
    height: 24px;
    margin-right: 12px;
  }
  .link-CSV {
    display: flex;
    align-items: center;
    font-weight: 600;
  }
`
