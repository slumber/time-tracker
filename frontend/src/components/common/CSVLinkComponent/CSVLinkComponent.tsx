import * as React from 'react'
import { useIntl } from 'react-intl'
import { CSVLink } from 'react-csv'
import { useAppSelector } from '../../../redux/hooks'
import { dateFormatter } from '../../commonFunctions/dateFormatter'

import { ButtonCSV } from './CSVLinkComponent.style'
import downloadIcon from '../../../assets/images/download.svg'

interface ICSVLinkComponentProps {
  data?: any[] | null,
  filename?: string,
  resultsMonth?: boolean
}

const CSVLinkComponent: React.FC<ICSVLinkComponentProps> = ({ data, filename, resultsMonth }) => {
  const intl = useIntl()
  const { results } = useAppSelector((state) => state.timeTrackingResults)
  const [resultsToCSV, setResultsToCSV] = React.useState<any[]>([])
  const headers = resultsMonth === false
    ? [
        { label: 'Name', key: 'user.name' },
        { label: 'Title', key: 'title' },
        { label: 'Time spent', key: 'timeSpent' },
        { label: 'Project name', key: 'webUrl' },
        { label: 'Date', key: 'spentAt' }
      ]
    : [
        { label: 'Name', key: 'user.name' },
        { label: 'Title', key: 'title' },
        { label: 'Time spent', key: 'timeSpent' },
        { label: 'Date', key: 'spentAt' }
      ]

  React.useEffect(() => {
    const noNullResults: any[] = []
    const iterable = data || results
    iterable?.forEach((result) => {
      const time = result.timeSpent / 60 / 60
      const webUrl = resultsMonth === false ? (result.issue ? result.issue.webUrl : result.mergeRequest.webUrl) : ''
      const csvObj = resultsMonth === false
        ? {
            user: result.user,
            title: result.issue ? result.issue.title : result.mergeRequest!.title,
            timeSpent: time.toFixed(2).toString().replace('.', ','),
            group: result.group,
            webUrl: webUrl.split('/')[3] + '/' + webUrl.split('/')[4],
            spentAt: dateFormatter(result.spentAt)
          }
        : {
            user: result.user,
            title: !result.issue && !result.mergeRequest ? result.title : (result.issue ? result.issue.title : result.mergeRequest!.title),
            timeSpent: time.toFixed(2).toString().replace('.', ','),
            spentAt: dateFormatter(result.spentAt)
          }
      noNullResults.push(csvObj)
    })
    setResultsToCSV(noNullResults)
  }, [results, data])

  return (
    <ButtonCSV type='submit' className='csvBtn' onClick={(e) => e.stopPropagation()}>
      <CSVLink
        className='link-CSV'
        data={resultsToCSV}
        headers={headers}
        filename={filename || `${resultsToCSV[0]?.user?.name}.csv`}>
        <img className='icon' src={downloadIcon} alt='download' />
        {intl.formatMessage({ id: 'download-report' })}
      </CSVLink>
    </ButtonCSV>
  )
}

export default CSVLinkComponent
