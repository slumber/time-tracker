import styled from 'styled-components'

export const Container = styled.div`
  width: 25%;
  min-width: 350px;
  height: 100%;
  padding: 16px 24px;
  background-color: #fff;
  border-radius: 10px;
  margin-right: 12px;
  h2 {
    color: #484848;
    width: 100%;
    font-size: 17px;
    font-weight: 500;
    display: flex;
    justify-content: flex-start;
    align-items: center;
    margin-bottom: 16px;
  }
  .MuiSvgIcon-root {
    fill: orangered;
    margin-right: 4px;
  }
  a {
    color: #000085;
  }
  .date {
    font-weight: 500;
    .arrow {
      margin: 0 16px;
    }
  }
  .deadline {
    margin-top: 8px;
  }
`
