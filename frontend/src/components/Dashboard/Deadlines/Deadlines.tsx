import * as React from 'react'
import WhatshotIcon from '@material-ui/icons/Whatshot'
import { Container } from './Deadlines.style'
import { useAppSelector } from '../../../redux/hooks'
import { useIntl } from 'react-intl'
import { IIssue } from '../../../types'

const Deadlines = () => {
  const intl = useIntl()
  const { results } = useAppSelector(state => state.timeTrackingResults)
  const [issuesDeadlines, setIssuesDeadlines] = React.useState<IIssue[]>([])

  React.useEffect(() => {
    const issuesWithDeadlines: IIssue[] = []
    if (results) {
      results.forEach(result => {
        if (result.issue?.dueDate) {
          const exist = issuesWithDeadlines.some(issue => issue.id === result.issue!.id)
          !exist && issuesWithDeadlines.push(result.issue)
        }
      })
      setIssuesDeadlines(issuesWithDeadlines)
    }
  }, [results])

  return (
    <Container>
      <h2>
        <WhatshotIcon /> Deadline
      </h2>
      {issuesDeadlines.length > 0
        ? (
            issuesDeadlines.map((issue, index) => (
          <div key={index} className='deadline'>
            <span className='date'>
              {issue.dueDate}
              <span className='arrow'>{'->'}</span>
            </span>
            <a href={issue.webUrl}>{issue.title}</a>
          </div>
            ))
          )
        : (
        <span className='noResults'>{intl.formatMessage({ id: 'no-deadlines' })}</span>
          )}
    </Container>
  )
}

export default Deadlines
