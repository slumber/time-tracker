import * as React from 'react'
import { useIntl } from 'react-intl'
import TrendingUpIcon from '@material-ui/icons/TrendingUp'

import { useStatsContext } from '../context/statsContext'
import { useAppSelector } from '../../../redux/hooks'
import { timeConverter } from '../../commonFunctions/timeConverter'

import TooltipComponent from '../../Tooltip/Tooltip'
import { StyledBox, StyledBoxMianSection } from './Forecast.style'

const Forecast = () => {
  const intl = useIntl()
  const statsContext = useStatsContext()
  const { user } = useAppSelector(state => state.user)
  const [estimatedWorkingTime, setEstimatedWorkingTime] = React.useState('0')
  const [salary, setSalary] = React.useState(0)

  React.useEffect(() => {
    if (user!.hourlyWage && statsContext.workingDays > 0) {
      let calculatedHours = (statsContext.averageTime.dayTimeInSeconds * statsContext.workingDays) / (60 * 60)
      calculatedHours = +calculatedHours.toFixed(2)
      const calculatedSalary = user!.hourlyWage * calculatedHours
      setSalary(+calculatedSalary.toFixed(2))
    }
  }, [user, statsContext.averageTime.dayTimeInSeconds, statsContext.workingDays])

  React.useEffect(() => {
    setEstimatedWorkingTime(timeConverter(statsContext.averageTime.dayTimeInSeconds * statsContext.workingDays)!)
  }, [statsContext.workingDays, statsContext.averageTime.dayTimeInSeconds])

  return (
    <StyledBox>
      <h2 className='fullWidth'>
        {intl.formatMessage({ id: 'forecast' })}
        <TooltipComponent title={intl.formatMessage({ id: 'forecast-description' })} placement={'right'} />
      </h2>
      <StyledBoxMianSection>
        <TrendingUpIcon />
        <span className='time'>{estimatedWorkingTime}</span>
        <span className='greyText'>{intl.formatMessage({ id: 'average-month-time-forecast' })}</span>
      </StyledBoxMianSection>
      <div className='column'>
        <span className='leftSide bottomData'>{statsContext.averageTime.dayTime}</span>
        <span className='bottomData-title'>{intl.formatMessage({ id: 'daily' })}</span>
      </div>
      <div className='column'>
        <span className='rightSide bottomData'>
          {statsContext.workingDays} {intl.formatMessage({ id: 'days' })}
        </span>
        <span className='bottomData-title'>{intl.formatMessage({ id: 'month' })}</span>
      </div>
      <div className='column'>
        <span className='salary bottomData'>{salary} zł</span>
        <span className='bottomData-title salaryText'>
          {intl.formatMessage({ id: 'salary' })}{' '}
          {user!.hourlyWage === null && (
            <TooltipComponent placement='bottom' title={intl.formatMessage({ id: 'salary-settings-tooltip' })} />
          )}
        </span>
      </div>
    </StyledBox>
  )
}

export default Forecast
