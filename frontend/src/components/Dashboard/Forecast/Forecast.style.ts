import styled from 'styled-components'
import { Box, BoxMainSection } from '../Dashboard.style'

export const StyledBox = styled(Box)`
  width: 35%;
  height: 100%;
  .fullWidth {
    display: flex;
    align-items: center;
    justify-content: center;
  }
`
export const StyledBoxMianSection = styled(BoxMainSection)`
  grid-row: 2;
`
