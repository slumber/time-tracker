import * as React from 'react'
import FullCalendar from '@fullcalendar/react'
import dayGridPlugin from '@fullcalendar/daygrid'
import '@fullcalendar/daygrid/main.css'

import { CalendarMonthWrapper } from './CalendarMonth.style'
import { Box } from '../Dashboard.style'

interface ICalendarProps {
  calendarDate: any
}

const CalendarMonth: React.FC<ICalendarProps> = ({ calendarDate }) => {
  interface CalendarFix extends React.Component {}

  const Calendar = (FullCalendar as any) as {
    new(): CalendarFix;
  }

  const props: any = {
    events: calendarDate,
    plugins: [dayGridPlugin],
    initialView: 'dayGridWeek',
    headerToolbar: { start: '', center: 'prev,title,today,next', end: '' },
    fixedWeekCount: false,
    height: 200,
    locale: 'en-gb',
    eventClassNames: function (arg: any) {
      const el = +arg.event._def.title
      switch (true) {
        case (el >= 0 && el < 2):
          return ['circle-empty']
        case (el >= 2 && el < 4):
          return ['circle-quarter']
        case (el >= 4 && el < 6):
          return ['circle-half']
        case (el >= 6 && el < 8):
          return ['circle-quarters']
        case (el >= 8):
          return ['circle-full']
      }
    }
  }

  return (
    <Box>
      <CalendarMonthWrapper>
        <Calendar {...props}
        />
      </CalendarMonthWrapper>
    </Box>
  )
}
export default CalendarMonth
