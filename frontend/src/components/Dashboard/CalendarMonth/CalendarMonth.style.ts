import styled from 'styled-components'

export const CalendarMonthWrapper = styled.div`
  width: 100%;
  .fc-header-toolbar {
    height: 50px;
    div {
      display: flex;
    }
  }
  .fc-toolbar-title {
    font-size: 20px;
    margin: 0 10px;
  }
  .fc-button {
    height: 30px;
    width: 30px;
    font-size: 14px;
    display: flex;
    justify-content: center;
    align-items: center;
    background-color: #fff;
    color: #666E7D;
    border: 1px solid #DBE5F0;
    :hover {
      background-color: #EFF2FB;
      color: #666E7D;
      border-color: #5932EA;
    }
    :disabled:hover {
      background-color: #2C3E50;
      color: #fff;
      border: none;
    }
  }
  .fc-button-primary:focus {
    box-shadow: none;
  }
  .fc .fc-button-primary:not(:disabled):active {
    background-color: #EFF2FB;
    color: #666E7D;
    border-color: #5932EA;
  }
  .fc-today-button {
    width: 55px;
    margin-right: 10px;
    border-radius: 40px;
  }
  .fc-col-header-cell-cushion {
    font-size: 12px;
  }
  .fc-view-harness,
  .fc-dayGrid {
    height: 80px;
  }
  .fc-scrollgrid-sync-inner {
    text-align: right;
    margin-bottom: 12px;
    border: none;
  }
  .fc-scrollgrid {
    border: none;
    position: relative;
    ::before {
      position: absolute;
      content: '';
      width: 3px;
      height: 52px;
      top: 0;
      left: -1px;
      background-color: #fff;
    }
  }
  td,
  th {
    border: none;
  }
  .fc .fc-daygrid-day.fc-day-today {
    background-color: #fff;
  }
  .fc-daygrid-event-harness {
    display: felx;
    justify-content: center;
  }
  .fc-daygrid-day-frame {
    display: flex;
    justify-content: center;
  }
  .fc-daygrid-event {
    height: 48px;
    width: 48px;
    border-radius: 50px;
    display: felx;
    justify-content: center;
    align-items: center;
    font-size: 14px;
    background-color: #fff;
    border: none;
  }
  .fc-event-title {
    font-weight: 600;
    color: #000;
  }
  .fc-col-header-cell-cushion {
    text-align: center;
    font-weight: 500;
  }
  .circle-quarter,
  .circle-half,
  .circle-quarters,
  .circle-full,
  .circle-empty {
    position: relative;
    ::before {
      border-radius: 40px;
      content: '';
      position: absolute;
      top: 0;
      left: 0;
      width: 100%;
      height: 100%;
    }
  }
  .circle-quarter:before {
    border-right: 3px solid #F50505;
    border-top: 2px solid transparent;
    border-left: 2px solid transparent;
    border-bottom: 2px solid transparent;
    transform: rotate(-45deg);
  }
  .circle-half::before {
    border: 3px solid #FA8A0B;
    border-top: 2px solid transparent;
    border-left: 2px solid transparent;
    transform: rotate(-45deg);
  }
  .circle-quarters::before {
    border: 3px solid #F5F510;
    border-top: 2px solid transparent;
    transform: rotate(-45deg);
  }
  .circle-full::before {
    border: 3px solid #3FBF1C;
  }
  .circle-empty::before {
    border: 3px solid #D5D8D4;
  }
  `
