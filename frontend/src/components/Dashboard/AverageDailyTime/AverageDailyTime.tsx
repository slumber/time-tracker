import * as React from 'react'
import { useIntl } from 'react-intl'

import { ITimeTrackingResults } from '../../../types'
import { useStatsContext } from '../context/statsContext'
import { timeConverter } from '../../commonFunctions/timeConverter'

import { Box, BoxMainSection } from '../Dashboard.style'

interface IChartObj {
  name: string
  current: number
  currentInSeconds: number
  previous: number
}

export interface AverageDailyTimeProps {
  currentMonth: ITimeTrackingResults[] | null
  prevMonth: ITimeTrackingResults[]
  chartData: IChartObj[]
  icon: string
}

const AverageDailyTime: React.FC<AverageDailyTimeProps> = ({ currentMonth, prevMonth, chartData, icon }) => {
  const intl = useIntl()
  const statsContext = useStatsContext()

  const getTotalTime = () => {
    let totalTime: number = 0
    chartData.forEach(date => {
      totalTime += date.currentInSeconds
    })
    return totalTime
  }
  const getTheLongestWorkingDay = () => {
    let longestTime: string | number = chartData[0].previous
    chartData.forEach(date => {
      if (date.currentInSeconds > longestTime) {
        longestTime = date.currentInSeconds
      }
    })
    return longestTime
  }
  const getTheShortestWorkingDay = (longestTime: number) => {
    let shortestTime: string | number = longestTime
    chartData.forEach(date => {
      const seconds = date.current * 60 * 60
      if (seconds < shortestTime && seconds > 0) {
        shortestTime = seconds
      }
    })
    return shortestTime
  }
  const getCurrentMonthWorkingDays = () => {
    const currentMonthWorkingDays: number[] = []
    currentMonth?.forEach(result => {
      let date: Date | number = new Date(result.spentAt)
      date = date.getDate()
      if (currentMonthWorkingDays.indexOf(date) === -1) {
        currentMonthWorkingDays.push(date)
      }
    })
    return currentMonthWorkingDays
  }
  const getPreviousMonthWorkingDays = () => {
    const prevMonthWorkingDays: number[] = []
    prevMonth.forEach(result => {
      let date: Date | number = new Date(result.spentAt)
      date = date.getDate()
      if (prevMonthWorkingDays.indexOf(date) === -1) {
        prevMonthWorkingDays.push(date)
      }
    })
    return prevMonthWorkingDays
  }
  const getAverageDailyTime = (currentMonthWorkingDays: number[], totalTime: number) => {
    let dayTimeSum
    let dayTimeInSecondsSum: number = 0
    if (currentMonthWorkingDays.length !== 0) {
      dayTimeSum = timeConverter(totalTime / currentMonthWorkingDays.length)!
      dayTimeInSecondsSum = totalTime / currentMonthWorkingDays.length
    } else dayTimeSum = '0'
    return { dayTimeInSecondsSum, dayTimeSum }
  }

  React.useEffect(() => {
    if (chartData.length > 0) {
      const prevMonthWorkingDays = getPreviousMonthWorkingDays()
      const currentMonthWorkingDays = getCurrentMonthWorkingDays()
      const totalTime = getTotalTime()
      const longestTime = getTheLongestWorkingDay()
      const shortestTime = getTheShortestWorkingDay(longestTime)
      const dailyime = getAverageDailyTime(currentMonthWorkingDays, totalTime)

      statsContext.setAverageTime({
        dayTime: `${dailyime.dayTimeSum}`,
        dayTimeInSeconds: dailyime.dayTimeInSecondsSum,
        longestTime: timeConverter(longestTime)!,
        shortestTime: timeConverter(shortestTime)!
      })
      statsContext.setWorkingDays(prevMonthWorkingDays.length)
    }
  }, [chartData, prevMonth, currentMonth])

  return (
    <Box twoColumns>
      <h2>
        {intl.formatMessage({ id: 'average-time' })}{' '}
        <span className='note'> - {intl.formatMessage({ id: 'current-month' })}</span>
      </h2>
      <BoxMainSection>
        <img className='icon' src={icon} />
        <div className="time-wrapper">
          <span className='time'>{statsContext.averageTime.dayTime}</span>
          <span className='greyText'>{intl.formatMessage({ id: 'total-time' })}</span>
        </div>
      </BoxMainSection>
      <div className="bottomWrapper">
        <div className='column'>
          <span className='bottomData'>{statsContext.averageTime.longestTime}</span>
          <span className='bottomData-title'>{intl.formatMessage({ id: 'longest' })}</span>
        </div>
        <div className='column'>
          <span className='bottomData'>{statsContext.averageTime.shortestTime}</span>
          <span className='bottomData-title'>{intl.formatMessage({ id: 'shortest' })}</span>
        </div>
      </div>
    </Box>
  )
}

export default AverageDailyTime
