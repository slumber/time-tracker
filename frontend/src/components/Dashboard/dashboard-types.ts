export interface IChartObj {
  name: string
  current: number
  currentInSeconds: number
  previous: number
}
