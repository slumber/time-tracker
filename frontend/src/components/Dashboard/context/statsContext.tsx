import React from 'react'

interface IAverageTime {
  dayTime: string
  dayTimeInSeconds: number
  longestTime: string
  shortestTime: string
}

interface StatsContextValue {
  workingDays: number
  setWorkingDays: (arg: number) => void
  averageTime: IAverageTime
  setAverageTime: (arg: IAverageTime) => void
}

export const StatsContext = React.createContext<StatsContextValue>(null!)

export const useStatsContext = () => {
  return React.useContext(StatsContext)
}

export interface StatsContextProviderProps {
  children: React.ReactNode
}

const StatsContextProvider: React.FC<StatsContextProviderProps> = ({ children }) => {
  const [workingDays, setWorkingDays] = React.useState(0)
  const [averageTime, setAverageTime] = React.useState<IAverageTime>({
    dayTime: '0',
    dayTimeInSeconds: 0,
    longestTime: '0',
    shortestTime: '0'
  })

  return (
    <StatsContext.Provider value={{ workingDays, setWorkingDays, averageTime, setAverageTime }}>
      {children}
    </StatsContext.Provider>
  )
}

export default StatsContextProvider
