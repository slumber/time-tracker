import styled from 'styled-components'
import { navbarWidth } from '../../variables'

export const Container = styled.div`
  overflow: auto;
  width: 100vw;
  padding: 24px 24px 24px calc(${navbarWidth} + 24px);
  display: flex;
  flex-direction: column;
  justify-content: start;
  align-items: flex-start;
  .row {
    display: flex;
    width: 100%;
    justify-content: start;
    flex-wrap: wrap;
  }
`
interface Props {
  twoColumns?: any
}
export const Box = styled.section<Props>`
  width: 425px;
  background-color: green;
  padding: 16px 24px;
  margin: 0 24px 24px 0;
  box-shadow: 2px 4px 24px rgba(48, 77, 175, 0.1);
  &:last-child {
    margin-right: 0;
  }
  display: flex;
  flex-direction: column;
  align-items: flex-start;
  background-color: #fff;
  border-radius: 10px;
.note {
  font-size: 20px;
  font-weight: 500;
  color: #000;
}
  }
  .bottomWrapper {
    margin-top: 16px;
    width: 80%;
    display: flex;
    justify-content: space-between;
  }
  .column {
    text-align: start;
    border-radius: 10px;
    display: flex;
    flex-direction: column;
    justify-content: center;
    align-items: flex-start;
  }
  .bottomData {
    font-size: 16px;
  }
  .bottomData-title {
    font-size: 16px;
    margin-top: 4px;
    color: #6D7D93;
  }
  .salaryText{
    position: relative;
    .tooltip {
      position: absolute;
      top: 0;
      left: 0;
      transform: translateX(-110%);
    }
  }
`
export const BoxMainSection = styled.div`
  display: flex;
  width: 100%;
  height: 70px;
  margin-top: 24px;
  padding-bottom: 16px;
  justify-content: start;
  align-items: center;
  border-bottom: 1px solid #a8a8a859;
  .MuiSvgIcon-root {
    fill: #78d0f4;
    font-size: 2.3rem;
  }
  .time-wrapper {
    display: flex;
    flex-direction: column;
    justify-content: center;
    margin-left: 15px;
  }
  .time {
    font-size: 24px;
    font-weight: 600;
    min-width: 115px;
  }
  .greyText {
    color: dimgray;
    font-size: 16px;
  }
  .icon {
    width: 48px;
    height: 48px;
  }
`
