import styled from 'styled-components'

export const Container = styled.section`
  background-color: #fff;
  border-radius: 10px;
  padding: 16px 24px 32px;
  width: 100%;
  height: 48%;
  min-height: 360px;
  margin-top: 12px;
  .chartContainer {
    margin: 0 auto;
  }
  .recharts-legend-wrapper {
    bottom: -20px !important;
  }
`
