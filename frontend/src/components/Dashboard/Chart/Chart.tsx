import * as React from 'react'
import { useIntl } from 'react-intl'
import { getDaysInMonth } from 'date-fns'
import { BarChart, Bar, XAxis, YAxis, CartesianGrid, Tooltip, Legend, ResponsiveContainer } from 'recharts'

import { ITimeTrackingResults } from '../../../types'
import { IChartObj } from '../dashboard-types'

import { Container } from './Chart.style'

export interface ChartProps {
  currentMonth: ITimeTrackingResults[] | null
  prevMonth: ITimeTrackingResults[]
  chartData: IChartObj[]
  setChartData: (arg: IChartObj[]) => void
}

const Chart: React.FC<ChartProps> = ({ currentMonth, prevMonth, chartData, setChartData }) => {
  interface IOneDay {
    date: number
    currentMonthTime: number
    previousMonthTime: number
  }
  const intl = useIntl()
  const [dates, setDates] = React.useState<IOneDay[]>([])

  React.useEffect(() => {
    const allDays: IOneDay[] = []
    const daysInMonth = getDaysInMonth(new Date())
    const daysInMonthArr = Array.from({ length: daysInMonth }, (_, i) => i + 1)
    daysInMonthArr.forEach(day => {
      allDays.push({ date: day, currentMonthTime: 0, previousMonthTime: 0 })
    })
    if (prevMonth) {
      prevMonth.forEach(result => {
        const date = new Date(result.spentAt)
        const found = allDays.find(element => element.date === date.getDate())
        if (found) {
          const index = allDays.indexOf(found)
          allDays[index].previousMonthTime += result.timeSpent
        } else {
          allDays.push({ date: date.getDate(), currentMonthTime: 0, previousMonthTime: result.timeSpent })
        }
      })
    }
    if (currentMonth) {
      currentMonth.forEach(result => {
        const date = new Date(result.spentAt)
        const found = allDays.find(element => element.date === date.getDate())
        if (found) {
          const index = allDays.indexOf(found)
          allDays[index].currentMonthTime += result.timeSpent
        } else {
          allDays.push({ date: date.getDate(), currentMonthTime: result.timeSpent, previousMonthTime: 0 })
        }
      })
    }
    allDays.sort((a, b) => a.date - b.date)
    setDates(allDays)
  }, [currentMonth, prevMonth])

  React.useEffect(() => {
    const newChartData: IChartObj[] = []
    dates.forEach(date => {
      newChartData.push({
        name: `${date.date}`,
        current: +(date.currentMonthTime / (60 * 60)).toFixed(2),
        currentInSeconds: date.currentMonthTime,
        previous: +(date.previousMonthTime / (60 * 60)).toFixed(2)
      })
    })
    setChartData(newChartData)
  }, [dates])

  return (
    <Container>
      <ResponsiveContainer className='chartContainer' width='100%' height='100%'>
        <BarChart
          data={chartData}
          margin={{
            top: 5,
            right: 30,
            left: 20,
            bottom: 5
          }}
        >
          <CartesianGrid strokeDasharray='3 3' />
          <XAxis dataKey='name' />
          <YAxis />
          <Tooltip labelFormatter={value => `${intl.formatMessage({ id: 'day' })} ${value}`} />
          <Legend />
          <Bar name={intl.formatMessage({ id: 'current-month' })} dataKey='current' fill='#8884d8' />
          <Bar name={intl.formatMessage({ id: 'previous-month' })} dataKey='previous' fill='#82ca9d' />
        </BarChart>
      </ResponsiveContainer>
    </Container>
  )
}

export default Chart
