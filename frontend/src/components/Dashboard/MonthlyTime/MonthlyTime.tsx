import * as React from 'react'
import { useIntl } from 'react-intl'

import { ITimeTrackingResults } from '../../../types'
import { splitToIssuesAndMR, IIssueResult, IMergeRequestResult } from '../../commonFunctions/splitToIssuesAndMR'
import { timeConverter } from '../../commonFunctions/timeConverter'
import { useAppSelector } from '../../../redux/hooks'

import { Box, BoxMainSection } from '../Dashboard.style'
import TooltipComponent from '../../Tooltip/Tooltip'

export interface MonthlyTimeProps {
  monthlyResults: ITimeTrackingResults[] | null
  heading: string
  icon: string
}

const MonthlyTime: React.FC<MonthlyTimeProps> = ({ monthlyResults, heading, icon }) => {
  const intl = useIntl()
  const { user } = useAppSelector(state => state.user)
  const [resultsWithIssue, setResultsWithIssue] = React.useState<IIssueResult[]>([])
  const [resultsWithMR, setResultsWithMR] = React.useState<IMergeRequestResult[]>([])
  const [totalTime, setTotalTime] = React.useState<string>('0')
  const [MRTime, setMRTime] = React.useState<string>('0')
  const [issueTime, setIssueTime] = React.useState<string>('0')
  const [totalHours, setTotalHours] = React.useState(0)
  const [salary, setSalary] = React.useState(0)

  React.useEffect(() => splitToIssuesAndMR(monthlyResults, setResultsWithIssue, setResultsWithMR), [monthlyResults])
  React.useEffect(() => {
    if (monthlyResults) {
      let totalTimeSum = 0
      let MRTimeSum = 0
      let issueTimeSum = 0
      monthlyResults.forEach(result => {
        totalTimeSum += result.timeSpent
      })
      resultsWithMR.forEach(mr => {
        MRTimeSum += mr.totalTime
      })
      resultsWithIssue.forEach(issue => {
        issueTimeSum += issue.totalTime
      })
      const totalTimeSumString = timeConverter(totalTimeSum)
      const MRTimeSumString = timeConverter(MRTimeSum)
      const issueTimeSumString = timeConverter(issueTimeSum)
      setTotalHours(totalTimeSum / (60 * 60))
      setTotalTime(totalTimeSumString!)
      setMRTime(MRTimeSumString!)
      setIssueTime(issueTimeSumString!)
    }
  }, [resultsWithIssue, resultsWithMR, monthlyResults])

  React.useEffect(() => {
    if (user!.hourlyWage && totalHours > 0) {
      const calculatedSalary = user!.hourlyWage * +totalHours.toFixed(2)
      setSalary(+calculatedSalary.toFixed(2))
    }
  }, [user, totalHours])

  return (
    <Box style={{ minWidth: '270px' }}>
      <h2>{heading}</h2>
      <BoxMainSection>
        <img className='icon' src={icon} />
        <div className='time-wrapper'>
          <span className='time'>{totalTime}</span>
          <span className='greyText'>{intl.formatMessage({ id: 'total-time' })}</span>
        </div>
      </BoxMainSection>
      <div className="bottomWrapper">
        <div className='column'>
          <span className='bottomData'>{issueTime}</span>
          <span className='bottomData-title'>Issues</span>
        </div>
        <div className='column'>
          <span className='bottomData'>{MRTime}</span>
          <span className='bottomData-title'>MRs</span>
        </div>
        <div className='column'>
          <span className='bottomData'>{salary} zł</span>
          <span className='bottomData-title salaryText'>
            {intl.formatMessage({ id: 'salary' })}
            {user!.hourlyWage === null && (
              <TooltipComponent placement='bottom' title={intl.formatMessage({ id: 'salary-settings-tooltip' })} />
              )}
          </span>
        </div>
      </div>
    </Box>
  )
}

export default MonthlyTime
