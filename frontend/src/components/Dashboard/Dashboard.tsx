import * as React from 'react'
import { useIntl } from 'react-intl'
import { getDaysInMonth } from 'date-fns'

import { useAppDispatch, useAppSelector } from '../../redux/hooks'
import { setTimeTrackingResults } from '../../redux/slices/timeTrackingResults'
import { ITimeTrackingResults } from '../../types'
import { IChartObj } from './dashboard-types'
import { getUserTimeTrackingResults } from '../../api/timeTrackingResults.api'
import StatsContextProvider from './context/statsContext'
import { handleStartEndOfMonth } from '../commonFunctions/handleStartEndOfMonth'

import clockPurple from '../../assets/images/clock-purple.png'
import clockBlue from '../../assets/images/clock-blue.png'
import clockOrange from '../../assets/images/clock-orange.png'

import MonthlyTime from './MonthlyTime/MonthlyTime'
import AverageDailyTime from './AverageDailyTime/AverageDailyTime'
import CalendarMonth from './CalendarMonth/CalendarMonth'
import { Container } from './Dashboard.style'

interface IOneDay {
  date: number
  currentMonthTime: number
  previousMonthTime: number
}

const Dashboard = () => {
  const intl = useIntl()
  const dispatch = useAppDispatch()

  const { results } = useAppSelector(state => state.timeTrackingResults)
  const { user } = useAppSelector(state => state.user)
  const [prevResults, setPrevResults] = React.useState<ITimeTrackingResults[]>([])
  const [chartData, setChartData] = React.useState<IChartObj[]>([])
  const [dates, setDates] = React.useState<IOneDay[]>([])
  const [calendarDate, setCalendarDate] = React.useState<any>([])
  React.useEffect(() => {
    const date = new Date()
    const prevMonth = date.setMonth(date.getMonth() - 1)
    handleStartEndOfMonth(prevMonth, user!, setPrevResults)
  }, [user])

  React.useEffect(() => {
    getUserTimeTrackingResults(user!.nickname, data => dispatch(setTimeTrackingResults(data)))
  }, [user, dispatch])

  React.useEffect(() => {
    const allDays: IOneDay[] = []
    const daysInMonth = getDaysInMonth(new Date())
    const daysInMonthArr = Array.from({ length: daysInMonth }, (_, i) => i + 1)
    daysInMonthArr.forEach(day => {
      allDays.push({ date: day, currentMonthTime: 0, previousMonthTime: 0 })
    })
    if (prevResults) {
      prevResults.forEach(result => {
        const date = new Date(result.spentAt)
        const found = allDays.find(element => element.date === date.getDate())
        if (found) {
          const index = allDays.indexOf(found)
          allDays[index].previousMonthTime += result.timeSpent
        } else {
          allDays.push({ date: date.getDate(), currentMonthTime: 0, previousMonthTime: result.timeSpent })
        }
      })
    }
    if (results) {
      results.forEach(result => {
        const date = new Date(result.spentAt)
        const found = allDays.find(element => element.date === date.getDate())
        if (found) {
          const index = allDays.indexOf(found)
          allDays[index].currentMonthTime += result.timeSpent
        } else {
          allDays.push({ date: date.getDate(), currentMonthTime: result.timeSpent, previousMonthTime: 0 })
        }
      })
    }
    allDays.sort((a, b) => a.date - b.date)
    setDates(allDays)
  }, [results, prevResults])

  React.useEffect(() => {
    const newChartData: IChartObj[] = []
    dates.forEach(date => {
      newChartData.push({
        name: `${date.date}`,
        current: +(date.currentMonthTime / (60 * 60)).toFixed(2),
        currentInSeconds: date.currentMonthTime,
        previous: +(date.previousMonthTime / (60 * 60)).toFixed(2)
      })
    })
    setChartData(newChartData)
  }, [dates])

  React.useEffect(() => {
    const newArr: any = []
    const date = new Date()
    const year = date.getFullYear()
    const month = date.getMonth().toString().length === 1 ? `0${date.getMonth() + 1}` : date.getMonth() + 1
    chartData.map((el) => {
      const day = el.name.length === 1 ? `0${el.name}` : el.name
      const objDay = {
        id: day,
        start: `${year}-${month}-${day}`,
        title: el.current
      }
      newArr.push(objDay)
      return newArr
    })
    setCalendarDate(newArr)
  }, [chartData])

  return (
    <StatsContextProvider>
      <Container>
        <div className='row'>
          <MonthlyTime monthlyResults={results} heading={intl.formatMessage({ id: 'this-month' })} icon={clockBlue} />
          <AverageDailyTime currentMonth={results} prevMonth={prevResults} chartData={chartData} icon={clockPurple} />
          <MonthlyTime monthlyResults={prevResults} heading={intl.formatMessage({ id: 'last-month' })} icon={clockOrange} />
          <CalendarMonth calendarDate={calendarDate} />
        </div>
      </Container>
    </StatsContextProvider>
  )
}

export default Dashboard
