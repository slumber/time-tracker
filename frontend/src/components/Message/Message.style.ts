import styled from 'styled-components'

export const StyledMessage = styled.div`
  color: #fff;
  font-size: 27px;
  text-align: center;
  grid-column: 1/-1;
  letter-spacing: 1px;
  position: absolute;
  top: 50%;
  left: 50%;
  transform: translate(-50%, -50%);
`
