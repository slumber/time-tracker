import * as React from 'react'
import { StyledMessage } from './Message.style'

export interface MessageProps {
  text: string
}

const Message: React.FC<MessageProps> = ({ text }) => {
  return <StyledMessage>{text}</StyledMessage>
}

export default Message
