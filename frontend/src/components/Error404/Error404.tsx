import * as React from 'react'
import { useIntl } from 'react-intl'
import { Container } from './Error404.style'
import ErrorImg from './images/Error.svg'

const Error404 = () => {
  const intl = useIntl()

  return (
    <Container>
      <div>
        <img className='errorImg' src={ErrorImg}/>
        <span>{intl.formatMessage({ id: 'not-found' })}</span>
      </div>
    </Container>
  )
}

export default Error404
