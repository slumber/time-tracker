import styled from 'styled-components'

export const Wrapper = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
`
export const WrapperButton = styled.div`
  width: max-content;
  margin-left: 16px;
  padding: 8px 14px 8px 8px;
  border-radius: 40px;
  display: flex;
  justify-content: center;
  align-items: center;
  border: 1px solid #DBE5F0;
  cursor: pointer;
  .group,
  .user-name {
    margin-left: 4px;
    font-size: 16px;
  }
`
export const UserCard = styled.div`
  display: flex;
  align-items: center;
  flex-direction: column;
  background-color: #fff;
  position: fixed;
  height: 100vh;
  width: 487px;
  right: 0;
  top: 0;
  box-shadow: -4px 4px 20px -10px rgba(0, 0, 0, 0.1);
  color: #000;
  padding: 48px;
  z-index: 10;
  .top-wrapper {
    display: flex;
    flex-direction: column;
    align-items: center;
    width: 100%;
  }
  .name-card {
    margin: 15px 0 36px;
    display: flex;
    align-items: center;
    font-size: 16px;
    font-weight: 600;
    width: 100%;
  }
  .group-card,
  .user-name-card {
    font-weight: 600;
    width: 50%;
  }
  .group-card {
    text-align: left;
    margin-left: 8px;
  }
  .user-name-card {
    text-align: right;
    margin-right: 8px;
  }
  .btn-close {
    border: none;
    background-color: #fff;
    position: absolute;
    top: 20px;
    right: 20px;
    cursor: pointer;
  }
`
