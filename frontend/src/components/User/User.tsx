import React, { useState } from 'react'

import { useAppSelector } from '../../redux/hooks'
import closeIcon from '../../assets/images/close.png'

import Wage from '../Settings/Wage/Wage'
import Group from '../Settings/Group/Group'
import Avatar from '../Avatar/Avatar'
import { Wrapper, WrapperButton, UserCard } from './User.style'

const User = () => {
  const { user } = useAppSelector((state) => state.user)
  const [open, setOpen] = useState(false)

  return (
    <Wrapper className='user-wrapper'>
      <h1>
        <WrapperButton onClick={() => setOpen(true)}>
          <Avatar margin='4px 12px' size={'32px'} src={user!.picture} />
          <span className='user-name'>{user!.name}</span>
          <span className='group'>/ {user!.defaultGroup}</span>
        </WrapperButton>
      </h1>
      {open && (
        <UserCard>
          <button className='btn-close' onClick={() => setOpen(false)}>
            <img src={closeIcon} alt='close icon' />
          </button>
          <div className='top-wrapper'>
            <Avatar src={user!.picture} size={'90px'} />
            <div className='name-card'>
              <p className='user-name-card'>{user!.name} </p> -<p className='group-card'>{user!.defaultGroup}</p>
            </div>
          </div>
          <Wage />
          <Group />
        </UserCard>
      )}
    </Wrapper>
  )
}

export default User
