import styled from 'styled-components'
import { navbarWidth } from '../../variables'

export const StyledHeader = styled.header`
  width: 100%;
  background-color: #F5F5F5;
  padding: 16px 24px;
  padding-left: calc(${navbarWidth} + 24px);
  height: 90px;
  display: flex;
  justify-content: space-between;
  align-items: center;
  color: #fff;
  .firstColumn {
    justify-self: start;
    display: flex;
    justify-content: center;
    align-items: center;
  }
  .secondColumn {
    display: flex;
  }
  h1 {
    cursor: default;
  }
`
