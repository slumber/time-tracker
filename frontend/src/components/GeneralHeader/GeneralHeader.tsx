import React from 'react'

import { useAppSelector } from '../../redux/hooks'

import User from '../User/User'
import Timer from '../Timer/Timer'
import { StyledHeader } from './GeneralHeader.style'

interface HeaderProps {
  header: string
}
const GeneralHeader: React.FC<HeaderProps> = ({ header }) => {
  const { user } = useAppSelector((state) => state.user)
  if (!user) {
    return null
  }
  return (
    <StyledHeader className='generalHeader'>
      <div className='firstColumn'>
        <h1>{header}</h1>
      </div>
      <div className='secondColumn'>
        <Timer />
        <User />
      </div>
    </StyledHeader>
  )
}

export default GeneralHeader
