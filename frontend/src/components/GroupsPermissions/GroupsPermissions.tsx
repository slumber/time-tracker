import * as React from 'react'
import { useIntl } from 'react-intl'
import { GroupsPermissionsContainer, GroupBox } from './GroupsPermissions.style'
import { addUserToGroup, groupCachedKey, removeUserFromGroup } from '../../api/user.api'
import { IGroup, IOwnedGroupsUsers } from '../../types'
import { Multiselect } from 'multiselect-react-dropdown'
import { ChangeEvent, useState } from 'react'
import { useAppSelector } from '../../redux/hooks'
import Message from '../Message/Message'

const GroupsPermissions = () => {
  const intl = useIntl()
  const getGroupCached = (): IGroup[] => {
    try {
      const key = localStorage.getItem(groupCachedKey)
      if (key) {
        return JSON.parse(localStorage.getItem(groupCachedKey) || '')
      }
      return []
    } catch (e) {
      return []
    }
  }
  const users = getGroupCached().map(x => x.users).flat()
  const { ownedGroups } = useAppSelector(state => state.ownedGroups)
  const [groupLength] = useState<IGroup[]>(ownedGroups.length > 0 ? ownedGroups : getGroupCached())
  const groupNames = getGroupCached().map(group => ({ fullPath: group.fullPath, name: group.name }))
  const [selectedGroup, setSelectedGroup] = useState<{ fullPath: string; name: string } | undefined>()

  const getUniqueUsers = () => {
    const uniqueUsers: IOwnedGroupsUsers[] = []
    users.forEach(user => {
      if (!uniqueUsers.map(x => x.name).includes(user.name)) {
        uniqueUsers.push(user)
      }
    })
    return uniqueUsers
  }
  const handleChangeGroup = (event: ChangeEvent<HTMLSelectElement>) => {
    const group = groupNames.find(x => x.name === event.target.value)
    if (group) {
      setSelectedGroup(group)
    }
  }
  const addToUserGroup = async (username: string, groupName: string) => {
    await addUserToGroup(username, groupName)
  }

  const deleteUserFromGroup = (username: string, groupName: string) => {
    removeUserFromGroup(username, groupName)
  }

  return (
    <>
      <GroupsPermissionsContainer>
          {(groupLength && groupLength.length > 0
            ? (
        <GroupBox>
          <span className='groupText'>Here you can add and remove permission to a specific user which can generate a statement of hours for any user.</span>
          <h4>Selected Group: {selectedGroup?.name}</h4>
          <select className='box' onChange={handleChangeGroup} value={selectedGroup?.name}>
            <option className='option' value="">Select Group</option>
            {groupNames.map((group, i) => <option className='option' value={group.name} key={i}>{group.fullPath}</option>)}
          </select>
          {selectedGroup && <div className='groupSelect'>
            <h4>Select Users:</h4>
            <Multiselect
              className='select'
              onSelect={(selectedList, selectedItem) => {
                const user = getUniqueUsers().find(x => x.name === selectedItem.Option)
                if (user && selectedGroup) {
                  addToUserGroup(user.nickname, selectedGroup.fullPath)
                }
              }}
              onRemove={(selectedList, removedItem) => {
                const user = getUniqueUsers().find(x => x.name === removedItem.Option)
                if (user && selectedGroup) {
                  deleteUserFromGroup(user.nickname, selectedGroup.fullPath)
                }
              }}
              options={getUniqueUsers().map((user, id) => ({ Option: user.name, id }))}
              displayValue="Option" showCheckbox/>
          </div>}
        </GroupBox>
              )
            : (
            <Message text={intl.formatMessage({ id: 'refresh-groups' })}/>
              ))}
      </GroupsPermissionsContainer>
    </>
  )
}

export default GroupsPermissions
