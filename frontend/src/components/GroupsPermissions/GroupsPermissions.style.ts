import styled from 'styled-components'
import { navbarWidth } from '../../variables'

export const GroupsPermissionsContainer = styled.div`
  width: 100vw;
  padding: 0.75rem 0.75rem 0 calc(${navbarWidth} + 0.75rem);
  display: flex;
  flex-wrap: wrap;
  align-items: flex-start;
  justify-content: flex-start;
`
export const GroupBox = styled.div`
  position: relative;
  height: auto;
  min-width: 25rem;
  background-color: #fff;
  margin: 3rem;
  padding: 1.875rem;
  border-radius: 0.625rem;
  box-shadow: 0 0.25rem 0.25rem rgba(0, 0, 0, 0.25);
  .box {
    width: 80%;
    border: 1px solid #ccc;
    border-radius: 0.25rem;
    padding: 0.313rem;
    min-height: 2.063rem;
    margin-bottom: 1.875rem;
  }
  .searchWrapper {
    max-width: 18.75rem;
  }
  .groupSelect {
    width: 80%
  }
  .groupText {
    display: flex;
    margin-bottom: 2rem;
    font-size: 16px;
    font-style: italic;
    max-width: 21.875rem;
  }
  h4 {
    margin-right: 1.25rem;
  }
`
