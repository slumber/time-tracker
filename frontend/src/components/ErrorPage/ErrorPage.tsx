import * as React from 'react'
import { useIntl } from 'react-intl'
import { Container } from './ErrorPage.style'

import ErrorImg from './images/Error.svg'

const ErrorPage = () => {
  const intl = useIntl()

  return (
    <Container>
      <div>
        <img className='errorImg' src={ErrorImg}/>
        <span>
          {intl.formatMessage({ id: 'err-message' })}
          {intl.formatMessage({ id: 'message' })}
        </span>
      </div>
    </Container>
  )
}

export default ErrorPage
