import styled from 'styled-components'

export const Container = styled.div`
  width: 100vw;
  height: 100vh;
  background-color: #fff;
  div {
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
  }
  span {
    font-family: 'Poppins', sans-serif;
    font-style: normal;
    font-weight: 600;
    font-size: 36px;
    line-height: 54px;
    max-width: 80%;
  }
  .errorImg {
    width: auto;
    height: auto;
  }
`
