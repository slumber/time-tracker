import * as React from 'react'
import { useIntl } from 'react-intl'
import {
  Table,
  TableCell,
  TableContainer,
  TableSortLabel,
  TableHead,
  TableRow,
  Paper,
  TableBody,
  TablePagination
} from '@material-ui/core'
import ArrowDropDown from '@material-ui/icons/ArrowDropDown'

import { getOwnedGroups } from '../../api/user.api'
import { setOwnedGroups } from '../../redux/slices/ownedGroups'
import { useAppDispatch, useAppSelector } from '../../redux/hooks'
import { IGroupWithSubgroups, IGroup, IOwnedGroupsUsers, ITimeRange, SORT_TYPE } from '../../types'
import arrowRight from '../../assets/images/arrow-right.svg'
import arrowUp from '../../assets/images/arrow-up.svg'

import NoGroupsInfo from './NoGroups/NoGroups'
import { Container, TableWrapper } from '../common/Table/Table.style'
import SubgroupWrapper from './ResultRow/SubgroupWrapper'
import Member from './ResultRow/Member'
import Filters from '../common/Filters/Filters'
import Calendar from '../common/Filters/Calendar'
import UserFilter from './Filters/UserFilter'
import ResetBtn from './Filters/ResetBtn'
import { FlexWrapper } from './ResultRow/ResultRow.style'

const OwnedGroups = () => {
  const intl = useIntl()
  const dispatch = useAppDispatch()
  const { user } = useAppSelector((state) => state.user)
  const { ownedGroups } = useAppSelector((state) => state.ownedGroups)

  const [page, setPage] = React.useState(0)
  const [rowsPerPage, setRowsPerPage] = React.useState(10)
  const [filterArrow, setFilterArrow] = React.useState('')
  const [groups, setGroups] = React.useState<IGroupWithSubgroups[]>([])
  const [groupsToShow, setGroupsToShow] = React.useState<IGroupWithSubgroups[]>([])
  const [isLoading, setIsLoading] = React.useState(false)
  const [allUsers, setAllUsers] = React.useState<IOwnedGroupsUsers[] | null>(null)
  const [openAllMembers, setOpenAllMembers] = React.useState(false)
  const [timeRange, setTimeRange] = React.useState<null | ITimeRange>(null)
  const [inputName, setInputName] = React.useState<string>('')
  const [listName, setListName] = React.useState<string[]>([])

  const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>) => {
    setRowsPerPage(parseInt(event.target.value, 10))
    setPage(0)
  }
  React.useEffect(() => {
    groups && setGroupsToShow(groups)
  }, [groups])

  React.useEffect(() => {
    if (listName.length) {
      handleSorting(SORT_TYPE.USER)
    } else {
      setGroupsToShow(groups)
    }
  }, [listName])

  const transformData = (data: IGroup[]) => {
    const groupsOnly = data.filter((group) => !group.fullPath.includes('/'))
    const subgroupsLevel1 = data.filter((group) => group.fullPath.split('/').length === 2)
    const subgroupsLevel2 = data.filter((group) => group.fullPath.split('/').length === 3)
    const subgroupsLevel3 = data.filter((group) => group.fullPath.split('/').length === 4)
    const newData: IGroupWithSubgroups[] = []
    const newDataLevel1: IGroupWithSubgroups[] = []
    const newDataLevel2: IGroupWithSubgroups[] = []
    subgroupsLevel2.forEach((currentValue) => {
      const subgroupsOfGroup = subgroupsLevel3.filter((subgroup) => subgroup.webUrl.includes(currentValue.fullPath))
      newData.push({ ...currentValue, subgroups: subgroupsOfGroup })
    })
    subgroupsLevel1.forEach((currentValue) => {
      const subgroupsOfGroup = newData.filter((subgroup) => subgroup.webUrl.includes(currentValue.fullPath))
      newDataLevel1.push({ ...currentValue, subgroups: subgroupsOfGroup })
    })
    groupsOnly.forEach((currentValue) => {
      const subgroupsOfGroup = newDataLevel1.filter((subgroup) => subgroup.webUrl.includes(currentValue.fullPath))
      newDataLevel2.push({ ...currentValue, subgroups: subgroupsOfGroup })
    })
    return newDataLevel2
  }

  React.useEffect(() => {
    const newArr:string[] = []
    groups.map((el) => (
      el.users.map((user) => {
        if ((user?.name.toLowerCase().includes(inputName.toLowerCase()) && inputName.length > 2) && inputName.length) {
          !newArr.includes(user.name) && newArr.push(user.name)
        }
        return newArr
      })
    ))
    setListName(newArr)
  }, [inputName])

  React.useEffect(() => {
    setAllUsers(getUniqueUsers())
    if (!ownedGroups.length) {
      return updateUserGroups()
    }
    setGroups(transformData(ownedGroups).filter((group) => !!group))
  }, [])

  React.useEffect(() => {
    setAllUsers(getUniqueUsers())
  }, [groups])

  const updateUserGroups = () => {
    if (user) {
      setIsLoading(true)
      getOwnedGroups(
        (data) => dispatch(setOwnedGroups(data)),
        (groups) => {
          setIsLoading(false)
          const ownedGroups = transformData(groups).filter((group) => !!group)
          setGroups(ownedGroups)
        }
      )
    }
  }

  const removeDuplicates = (users: IOwnedGroupsUsers[]) => {
    const unique: IOwnedGroupsUsers[] = []
    users.forEach((user) => {
      const isOnTheList = unique.find((uniqueUser) => uniqueUser.nickname === user.nickname)
      if (!isOnTheList) {
        unique.push(user)
      }
    })
    return unique
  }

  const getUniqueUsers = () => {
    const allGroupUsers = groups.map((group) => group.users).flat()
    const allSubgroupUsers = groups.map((group) => group.subgroups.map((sub) => sub.users).flat()).flat()
    const elements = [...allGroupUsers, ...allSubgroupUsers]
    return removeDuplicates(elements)
  }

  const getTimeRange = (args: ITimeRange) => {
    const { start, end } = args
    setTimeRange({ start, end })
  }

  const handleSorting = (ACTION: SORT_TYPE) => {
    let newArr:IGroupWithSubgroups[] = groupsToShow ? [...groupsToShow] : []
    switch (ACTION) {
      case SORT_TYPE.NAME:
        newArr.sort((a, b) => {
          if (b.name.localeCompare(a.name) === 1) {
            return a.name.localeCompare(b.name)
          } else {
            return b.name.localeCompare(a.name)
          }
        })
        break
      case SORT_TYPE.MEMBERS:
        newArr.sort((a, b) => {
          if (b.users.length > a.users.length) {
            return a.users.length > b.users.length ? 1 : -1
          } else {
            return b.users.length > a.users.length ? 1 : -1
          }
        })
        break
      case SORT_TYPE.USER:
        newArr = []
        groups.map((el) => {
          el.users.map((user) => {
            if (listName.includes(user.name) && !newArr.includes(el)) {
              newArr.push(el)
            }
            return newArr
          })
          return newArr
        })
        break
    }
    setGroupsToShow(newArr)
  }

  return !isLoading
    ? (
        groups && groups.length > 0
          ? (
      <Container>
        <Filters>
          <UserFilter inputName={inputName} setInputName={setInputName} listName={listName} />
          <Calendar getTimeRange={getTimeRange} forResults={false} />
          {!!listName.length && <ResetBtn setInputName={setInputName} /> }
        </Filters>
        <TableWrapper>
          <TableContainer component={Paper}>
            <Table aria-label='collapsible table'>
              <TableHead className='issuesHead'>
                <TableRow className='paddingRow'>
                  <TableCell align='left' className='margin2'>
                    <TableSortLabel
                      active={true}
                      direction={filterArrow === 'names' ? 'asc' : 'desc'}
                      IconComponent={ArrowDropDown}
                      onClick={() => {
                        handleSorting(SORT_TYPE.NAME)
                        setFilterArrow('names')
                      }}>
                      {intl.formatMessage({ id: 'name' })}
                    </TableSortLabel>
                  </TableCell>
                  <TableCell align='left' className='secondCell'>
                    <TableSortLabel
                      active={true}
                      direction={filterArrow === 'members' ? 'asc' : 'desc'}
                      IconComponent={ArrowDropDown}
                      onClick={() => {
                        handleSorting(SORT_TYPE.MEMBERS)
                        setFilterArrow('members')
                      }}>
                      {intl.formatMessage({ id: 'number-of-members' })}
                    </TableSortLabel>
                  </TableCell>
                  <TableCell align='left' className='thirdCell'>
                  </TableCell>
                </TableRow>
              </TableHead>
              <TableBody>
                <TableRow
                  className={openAllMembers ? 'thickBorder' : ''}
                  onClick={() => setOpenAllMembers(!openAllMembers)}>
                  <TableCell align='left'>
                    <FlexWrapper>
                      <img
                        alt={openAllMembers ? 'hide' : 'expand'}
                        src={openAllMembers ? arrowUp : arrowRight}
                        className='arrow'
                      />
                      <span onClick={(event) => event.stopPropagation()}>{intl.formatMessage({ id: 'all-members' })}</span>
                    </FlexWrapper>
                  </TableCell>
                  <TableCell align='left' className='timeCell'>
                    {allUsers?.length || 0}
                  </TableCell>
                  <TableCell align='left' className='timeCell'>
                  </TableCell>
                </TableRow>
                {openAllMembers &&
                  allUsers &&
                  allUsers.map((user) => (
                    <Member
                      key={user.nickname}
                      user={user}
                      groupName={null}
                      getGeneralTimelogPerUser
                      start={timeRange?.start || null}
                      end={timeRange?.end || null}
                    />
                  ))}
                {groupsToShow?.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((group) => (
                  <SubgroupWrapper
                    key={group.id}
                    group={group}
                    start={timeRange?.start || null}
                    end={timeRange?.end || null}
                  />
                ))}
              </TableBody>
            </Table>
          </TableContainer>
          <TablePagination
            rowsPerPageOptions={[5, 10, 25]}
            component='div'
            count={groups.length}
            rowsPerPage={rowsPerPage}
            page={page}
            onPageChange={(event: unknown, newPage: number) => setPage(newPage)}
            onRowsPerPageChange={handleChangeRowsPerPage}
          />
        </TableWrapper>
      </Container>
            )
          : (
      <NoGroupsInfo updateUserGroups={updateUserGroups} />
            )
      )
    : null
}

export default OwnedGroups
