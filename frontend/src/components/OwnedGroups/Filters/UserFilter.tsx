import * as React from 'react'

import { FilterWrapper } from './FiltersUser.style'
import searchIcon from '../../../assets/images/search-icon.png'

export interface ResultRowProps {
  inputName: string
  setInputName: React.Dispatch<React.SetStateAction<string>>
  listName: string[]
}

const UserFilter: React.FC<ResultRowProps> = ({ inputName, setInputName, listName }) => {
  const [open, setOpen] = React.useState<boolean>(false)
  const dialogRef = React.useRef<null | HTMLDivElement>(null)

  React.useEffect(() => {
    const handleClickOutside = (event: any) => {
      if (!dialogRef.current || dialogRef.current.contains(event.target)) {
        return
      }
      setOpen(false)
    }
    document.addEventListener('mousedown', handleClickOutside)
    return () => {
      document.removeEventListener('mousedown', handleClickOutside)
    }
  }, [])

  return (
    <>
      <FilterWrapper>
        <div className='input-wrapper'>
          <input className='input' value={inputName} onClick={() => setOpen(true)} onKeyUp={() => setOpen(true)} onChange={(event) => setInputName(event.target.value)} type='text' placeholder='Szukaj uzytkownika...' />
          <img src={searchIcon} />
          {!!listName.length && open && (<div className='list-names' ref={dialogRef}>
            {listName.map((el) => (
              <button className='name-btn' key={el} onClick={() => { setOpen(false); setInputName(el) }}>{el}</button>
            ))}
          </div>)}
        </div>
      </FilterWrapper>
    </>
  )
}

export default UserFilter
