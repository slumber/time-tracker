import styled from 'styled-components'

export const FilterWrapper = styled.div`
  display: flex;
  width: 80px;
  flex-direction: column;
  padding: 0;
  margin: 0;
  z-index: 10;
  margin-right: 10px;
  width: 233px;
  z-index: 1;
  .input-wrapper {
    height: 100%;
    border-radius: 40px;
    border: none;
    position: relative;
  }
  .input {
    width: 100%;
    height: 100%;
    padding: 0 20px 0 40px;
    border-radius: 40px;
    border: none;
    :hover {
      background-color: #EFF2FB;
    }
  }
  img {
    position: absolute;
    left: 20px;
    top: 15px;
    width: 16px;
  }
  .list-names {
    position: absolute;
    top: 50px;
    width: 100%;
    left: 0;
    background-color: #fff;
    border-radius: 20px;
    box-shadow: 0px 8px 24px rgba(0, 0, 0, 0.12);
    overflow: hidden;
  }
  .name-btn {
    border: none;
    width: 100%;
    background-color: #fff;
    font-size: 14px;
    font-weight: 400;
    cursor: pointer;
    transition: 0.2s;
    padding: 12px 25px;
    text-align: left;
    :hover {
      background-color: #EFF2FB;
    }
  }
  input:focus-visible {
    outline: 1px solid #5932EA;
  }
`

export const AllUserBtn = styled.button`
  border: none;
  background-color: transparent;
  font-size: 16px;
  font-weight: 600;
  cursor: pointer
`
