import * as React from 'react'
import { useIntl } from 'react-intl'
import { AllUserBtn } from './FiltersUser.style'

export interface ButtonProps {
  setInputName: React.Dispatch<React.SetStateAction<string>>
}
const ResetBtn: React.FC<ButtonProps> = ({ setInputName }) => {
  const intl = useIntl()

  return <AllUserBtn className='avatar' onClick={() => setInputName('')}>{intl.formatMessage({ id: 'show-all-users' })}</AllUserBtn>
}

export default ResetBtn
