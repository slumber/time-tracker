import * as React from 'react'
import { TableCell, TableRow } from '@material-ui/core'

import { getTimelogForGroup } from './helpers'
import { IGroup } from '../../../types'
import arrowRight from '../../../assets/images/arrow-right.svg'
import arrowUp from '../../../assets/images/arrow-up.svg'

import CSVLinkComponent from '../../common/CSVLinkComponent/CSVLinkComponent'
import Member from './Member'
import { FlexWrapper } from './ResultRow.style'

export interface ResultRowProps {
  subgroup: IGroup
  start: string | null
  end: string | null
}

const Subgroup: React.FC<ResultRowProps> = ({ subgroup, start, end }) => {
  const [open, setOpen] = React.useState(false)
  const [subgroupResults, setSubgroupResults] = React.useState<any[] | null>(null)
  const margin = subgroup.fullPath.split('/').length

  React.useEffect(() => {
    const getResultsForGroup = async () => {
      const results = await getTimelogForGroup({ groupName: subgroup.fullPath, start, end })
      setSubgroupResults(results || [])
    }
    getResultsForGroup()
  }, [start, end])
  return (
    <>
      <TableRow key={subgroup.id} onClick={() => setOpen(!open)}>
        <TableCell component='th' scope='row' className={`margin${margin}`}>
          <FlexWrapper>
            <img alt={open ? 'hide' : 'expand'} src={open ? arrowUp : arrowRight} className='arrow' />
            <span onClick={(event) => event.stopPropagation()}>{subgroup.name}</span>
          </FlexWrapper>
        </TableCell>
        <TableCell component='th' scope='row'>
          {subgroup.users?.length || '...'}
        </TableCell>
        <TableCell align='left'>
          {subgroupResults && <CSVLinkComponent data={subgroupResults} filename={`${subgroup.name}.csv`} resultsMonth={false} />}
        </TableCell>
      </TableRow>
      {open &&
        subgroup.users.length > 0 &&
        subgroup.users.map((user) => (
          <Member key={user.nickname} user={user} groupName={subgroup.name} start={start} end={end} margin={margin} />
        ))}
    </>
  )
}

export default Subgroup
