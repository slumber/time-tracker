import { axiosInstance } from '../../../api/axiosConfig'
import { dispatchError } from '../../../commonFunctions/handleSnackbars'

export const getTimelogForUserInGroup: (args: {
  username: string | null
  groupName: string
  start: string | null
  end: string | null
}) => Promise<any[] | null> = async ({ end, start, username, groupName }) => {
  try {
    const response: { data: any[] } = await axiosInstance.get(
      start && end
        ? `/user/${username}/timelog?this-month=true&group=${encodeURIComponent(groupName)}`
        : `/user/${username}/timelog?this-month=true&group=${encodeURIComponent(groupName)}`
    )
    return response.data
  } catch (error) {
    console.log(error)
    dispatchError('timelog-general-err')
    return null
  }
}

export const getTimelogForGroup: (args: {
  groupName: string
  start: string | null
  end: string | null
}) => Promise<any[] | null> = async ({ start, end, groupName }) => {
  try {
    const response: { data: any[] } = await axiosInstance.get(
      start && end
        ? `/user/group/${encodeURIComponent(groupName)}/timelog?start=${start}&end=${end}`
        : `/user/group/${encodeURIComponent(groupName)}/timelog?this-month=true`
    )
    return response.data
  } catch (error) {
    console.log(error)
    dispatchError('timelog-general-err')
    return null
  }
}
