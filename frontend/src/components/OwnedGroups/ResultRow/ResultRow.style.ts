import styled from 'styled-components'

export const FlexWrapper = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  .arrow {
    width: 24px;
    margin-right: 24px;
    cursor: pointer;
  }
`
