import * as React from 'react'

import { IGroup } from '../../../types'

import ResultRow from '../ResultRow/ResultRow'
import Subgroup from '../ResultRow/Subgroup'

export interface ResultRowProps {
  group: IGroup
  start: string | null
  end: string | null
}
const SubgroupWrapper: React.FC<ResultRowProps> = ({ group, start, end }) => {
  return (
    <>
    {group.subgroups.length > 0
      ? <ResultRow
      key={group.id}
      group={group}
      start={start}
      end={end}
    />
      : <Subgroup key={group.id} subgroup={group} start={start} end={end} />
        }
    </>
  )
}
export default SubgroupWrapper
