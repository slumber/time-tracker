import * as React from 'react'
import { TableCell, TableRow } from '@material-ui/core'
import { useIntl } from 'react-intl'

import { getTimelogForGroup } from './helpers'
import { IGroupWithSubgroups } from '../../../types'
import arrowRight from '../../../assets/images/arrow-right.svg'
import arrowUp from '../../../assets/images/arrow-up.svg'

import CSVLinkComponent from '../../common/CSVLinkComponent/CSVLinkComponent'
import SubgroupWrapper from './SubgroupWrapper'
import Member from '../ResultRow/Member'
import { FlexWrapper } from './ResultRow.style'

export interface ResultRowProps {
  group: IGroupWithSubgroups
  start: string | null
  end: string | null
}

const ResultRow: React.FC<ResultRowProps> = ({ group, start, end }) => {
  const intl = useIntl()
  const [open, setOpen] = React.useState(false)
  const [groupResults, setGroupResults] = React.useState<any[] | null>(null)
  const [openMembers, setOpenMembers] = React.useState(false)
  const margin = group.fullPath.split('/').length

  React.useEffect(() => {
    const getResultsForGroup = async () => {
      const results = await getTimelogForGroup({ groupName: group.fullPath, start, end })
      setGroupResults(results || [])
    }
    getResultsForGroup()
  }, [start, end])

  return (
    <>
      <TableRow className={open ? 'thickBorder' : ''} onClick={() => setOpen(!open)}>
        <TableCell component='th' scope='row' className={`margin${margin}`}>
          <FlexWrapper>
            <img alt={open ? 'hide' : 'expand'} src={open ? arrowUp : arrowRight} className='arrow' />
            <span onClick={(event) => event.stopPropagation()}>{group.name}</span>
          </FlexWrapper>
        </TableCell>
        <TableCell align='left' className='timeCell'>
          {group.users?.length || 0}
        </TableCell>
        <TableCell align='left'>
          {groupResults && <CSVLinkComponent data={groupResults} filename={`${group.name}.csv`} resultsMonth={false} />}
        </TableCell>
      </TableRow>
      {open && <>
      <TableRow className={openMembers ? 'thickBorder' : ''} onClick={() => setOpenMembers(!openMembers)}>
        <TableCell align='left' className={`margin${margin + 1}`}>
          <FlexWrapper>
            <img
              alt={openMembers ? 'hide' : 'expand'}
              src={openMembers ? arrowUp : arrowRight}
              className='arrow'
            />
            <span onClick={(event) => event.stopPropagation()}>{intl.formatMessage({ id: 'members' })}</span>
          </FlexWrapper>
        </TableCell>
        <TableCell align='left' className='timeCell'>
          {group.users?.length || 0}
        </TableCell>
        <TableCell align='left' className='timeCell'>
        </TableCell>
      </TableRow>
      {openMembers && group.users && group.users.map((user: any) => (
        <Member
          key={user.nickname}
          user={user}
          groupName={group.fullPath}
          getGeneralTimelogPerUser
          start={start}
          end={end}
          margin={margin}
        />
      ))}
        {group.subgroups.length > 0 &&
        group.subgroups.map((subgroup) => <SubgroupWrapper key={subgroup.id} group={subgroup} start={start} end={end} />)}
        </>}
    </>
  )
}

export default ResultRow
