import * as React from 'react'
import { TableCell, TableRow } from '@material-ui/core'

import { getTimelogForUserInGroup } from './helpers'
import { IOwnedGroupsUsers } from '../../../types'
import { getUserTimelog } from '../../../api/timeTrackingResults.api'

import CSVLinkComponent from '../../common/CSVLinkComponent/CSVLinkComponent'
import { FlexWrapper } from './ResultRow.style'
import Avatar from '../../Avatar/Avatar'

export interface ResultRowProps {
  user: IOwnedGroupsUsers
  groupName: string | null
  getGeneralTimelogPerUser?: boolean
  start: string | null
  end: string | null
  margin?: number
}

const Member: React.FC<ResultRowProps> = ({ user, groupName, getGeneralTimelogPerUser, start, end, margin }) => {
  const [userResults, setUserResults] = React.useState<any[] | null>(null)

  React.useEffect(() => {
    const getResultsForGroup = async () => {
      if (groupName) {
        const results = await getTimelogForUserInGroup({ groupName, username: user.nickname, start, end })
        setUserResults(results || [])
      }
    }
    const getAllResultsForUser = async () => {
      if (getGeneralTimelogPerUser) {
        await getUserTimelog(
          start && end
            ? { user: user.nickname, setResults: setUserResults, start, end }
            : { user: user.nickname, setResults: setUserResults }
        )
      }
    }
    getResultsForGroup()
    getAllResultsForUser()
  }, [start, end])

  return (
    <TableRow>
      <TableCell component='th' scope='row' className={margin ? `memberMargin${margin}` : 'memberMargin1'}>
        <FlexWrapper>
          <Avatar src={user.picture} size={'24px'} margin='0 10px 0 0' />
          {user.name}
        </FlexWrapper>
      </TableCell>
      <TableCell>
      </TableCell>
      <TableCell align='left'>
        {userResults && <CSVLinkComponent data={userResults} filename={`${user.name}.csv`} resultsMonth={false} />}
      </TableCell>
    </TableRow>
  )
}

export default Member
