import styled from 'styled-components'

export const NoGroupsWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
`
export const Image = styled.img`
  margin-top: 120px;
  width: 200px;
  height: 200px;
`
export const Text = styled.span`
  color: #000;
  font-weight: 400;
  font-size: 20px;
  line-height: 30px;
  margin-top: 15px;
  margin-bottom: 9px;
`
export const Button = styled.button`
  align-items: center;
  padding: 8px 24px;
  gap: 16px;
  border-radius: 70px;
  background-color: #5932ea;
  color: #fff;
  border: none;
  width: max-content;
`
