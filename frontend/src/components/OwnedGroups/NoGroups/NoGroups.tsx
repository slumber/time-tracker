import React from 'react'
import { useIntl } from 'react-intl'
import noGroupsImg from '../../../assets/images/noGroupsInfo.svg'
import { Image, NoGroupsWrapper, Text, Button } from './NoGroups.style'

const NoGroupsInfo: React.FC<{ updateUserGroups: () => void }> = ({ updateUserGroups }) => {
  const intl = useIntl()

  return (
    <NoGroupsWrapper>
      <Image src={noGroupsImg} alt='' />
      <Text>{intl.formatMessage({ id: 'own-no-groups' })}</Text>
      <Button onClick={updateUserGroups}>{intl.formatMessage({ id: 'refresh-page' })}</Button>
    </NoGroupsWrapper>
  )
}

export default NoGroupsInfo
