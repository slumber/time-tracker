import * as React from 'react'
import { useIntl } from 'react-intl'
import { MenuItem } from '@material-ui/core'

import { updateUser } from '../../../api/user.api'
import { useAppDispatch, useAppSelector } from '../../../redux/hooks'
import { setUser } from '../../../redux/slices/user'

import { StyledSelect, GroupWrapper, GroupInputWrapper } from './Group.style'

const Group = () => {
  const intl = useIntl()
  const dispatch = useAppDispatch()
  const { user } = useAppSelector(state => state.user)
  const [group, setGroup] = React.useState(user!.defaultGroup)
  const [saveNewGroup, setSaveNewGroup] = React.useState(false)

  React.useEffect(() => {
    if (user && group !== user.defaultGroup) {
      setSaveNewGroup(true)
    } else setSaveNewGroup(false)
  }, [user, group])

  const changeGroup = () => {
    const newUser = {
      id: user!.id,
      email: user!.email,
      name: user!.name,
      nickname: user!.nickname,
      profile: user!.profile,
      picture: user!.picture,
      hourlyWage: user!.hourlyWage,
      groups: user!.groups,
      defaultGroup: group,
      startDate: user!.startDate
    }
    updateUser(newUser, response => dispatch(setUser(response)))
  }

  return (
    <GroupWrapper>
      <h2 className='groupHeading'>{intl.formatMessage({ id: 'group' })}</h2>
      <p>{intl.formatMessage({ id: 'group-description-extended' })}</p>
      <GroupInputWrapper>
        <StyledSelect
          id='group-select'
          value={group}
          onChange={(event: React.ChangeEvent<{ value: unknown }>) => setGroup(event.target.value as string)}
        >
          {user?.groups.map((userGroup, index) => (
            <MenuItem key={index} value={userGroup}>
              {userGroup}
            </MenuItem>
          ))}
        </StyledSelect>
        <button className='btn-group' onClick={changeGroup} disabled={!saveNewGroup}>
          {intl.formatMessage({ id: 'change-group' })}
        </button>
      </GroupInputWrapper>
    </GroupWrapper>
  )
}

export default Group
