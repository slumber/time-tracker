import styled from 'styled-components'
import { Select } from '@material-ui/core'

export const GroupWrapper = styled.div`
  margin: 40px auto 0;
  .MuiInput-underline::before,
  .MuiInput-underline:hover:not(.Mui-disabled)::before {
    border-bottom: 1px solid rgb(255, 255, 255);
  }
  .MuiInput-underline::after {
    content: none;
  }
  .groupHeading {
    font-size: 16px;
    font-weight: 600;
    margin-bottom: 8px !important;
  }
  p {
    font-size: 14px;
    text-align: left;
    margin-bottom: 16px;
  }
  .btn-group {
    margin-left: 32px;
    background-color: #fff;
    border: none;
    font-size: 16px;
    font-weight: 600;
    cursor: pointer;
  }
`
export const GroupInputWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: start;
  flex-wrap: wrap;
  margin: 48px auto 0;
  .label {
    margin-right: 16px;
  }
  .select-wrapper {
    display: flex;
  }
`
export const StyledSelect = styled(Select)`
  width: 180px;
  color: #000;
  background-color: #F8F9FD;
  padding: 8px 0;
  border-radius: 20px;
  transition: 0.2s;
  :hover {
    background-color: #EFF2FB;
  }
  .MuiSelect-icon {
    color: #6D7D93;
    margin-right: 8px;
  }
  .MuiInputBase-input {
    width: 100%;
    padding-left: 16px;
  }
`
