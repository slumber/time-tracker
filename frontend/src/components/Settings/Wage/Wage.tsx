import * as React from 'react'
import { useIntl } from 'react-intl'

import { updateUser } from '../../../api/user.api'
import { useAppDispatch, useAppSelector } from '../../../redux/hooks'
import { setUser } from '../../../redux/slices/user'

import { StyledWageField, WageWrapper, WageInputWrapper } from './Wage.style'

const Wage = () => {
  const intl = useIntl()
  const dispatch = useAppDispatch()
  const { user } = useAppSelector(state => state.user)
  const [wage, setWage] = React.useState(user!.hourlyWage || Number)
  const [saveNewWage, setSaveNewWage] = React.useState(false)

  React.useEffect(() => {
    if (user && Number(wage) !== user.hourlyWage) {
      setSaveNewWage(true)
    } else setSaveNewWage(false)
  }, [user, wage])

  const changeWage = () => {
    const newUser = {
      id: user!.id,
      email: user!.email,
      name: user!.name,
      nickname: user!.nickname,
      profile: user!.profile,
      picture: user!.picture,
      hourlyWage: Number(wage),
      groups: user!.groups,
      defaultGroup: user!.defaultGroup,
      startDate: user!.startDate
    }
    updateUser(newUser, response => dispatch(setUser(response)))
  }

  return (
    <WageWrapper>
      <h2 className='wageHeading'>{intl.formatMessage({ id: 'wage' })}</h2>
      <p>{intl.formatMessage({ id: 'wage-description-extended' })}</p>
      <WageInputWrapper>
        <div className='input'>
        <StyledWageField
          id='standard-number'
          type='number'
          InputProps={{
            inputProps: {
              min: 0,
              step: 0.01
            }
          }}
          value={wage}
          onChange={(e) => {
            let value = parseFloat(e.target.value)
            if (value < 0) {
              value = Math.abs(value)
            }
            setWage(value)
          }}

          InputLabelProps={{
            shrink: true
          }}
        />{' '}
        <span>zł/h</span>
        </div>
      <button className='btn-wage' onClick={changeWage} disabled={!saveNewWage}>
        {intl.formatMessage({ id: 'edit' })}
      </button>
      </WageInputWrapper>
    </WageWrapper>
  )
}

export default Wage
