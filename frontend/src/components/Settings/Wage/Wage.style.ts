import styled from 'styled-components'
import { TextField } from '@material-ui/core'

export const WageWrapper = styled.div`
  margin: 0 auto;
  border-bottom: 1px solid #DBE5F0;
  padding-bottom: 40px;
  .MuiInput-underline::before,
  .MuiInput-underline:hover:not(.Mui-disabled)::before {
    border-bottom: 1px solid rgb(255, 255, 255);
  }
  .MuiInput-underline::after {
    content: none;
  }
  .wageHeading {
    font-size: 16px;
    font-weight: 600;
    margin-bottom: 8px !important;
  }
  p {
    font-size: 14px;
    text-align: left;
    margin-bottom: 33px;
    font-weight: 400;
  }
  .btn-wage {
    border: none;
    font-size: 16px;
    font-weight: 600;
    background-color: #fff;
    margin-left: 32px;
    cursor: pointer;
  }
  input::-webkit-outer-spin-button,
  input::-webkit-inner-spin-button {
    -webkit-appearance: none;
    margin: 0;
  }

  input[type='number'] {
    -moz-appearance: textfield; /* Firefox */
  }
`
export const WageInputWrapper = styled.div`
  display: flex;
  align-items: center;
  justify-content: left;
  .input {
    display: flex;
    align-items: center;
    justify-content: space-between;
    width: 207px;
    background-color: #F8F9FD;
    border-radius: 38px;
    padding: 8px 16px;
    transition: 0.2s;
    :hover {
      background-color: #EFF2FB;
    }
  }
`
export const StyledWageField = styled(TextField)`
  .MuiInputBase-input {
    width: 100%;
    color: #000;
    text-align: left;
  }
`
