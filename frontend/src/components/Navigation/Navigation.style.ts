import styled from 'styled-components'

export const NavWrapper = styled.div`
  display: flex;
  position: fixed;
  width: 80px;
  background-color: #fff;
  height: 100vh;
  flex-direction: column;
  padding: 24px 0 7px;
  margin: 0;
  justify-content: space-between;
  z-index: 10;
  .timetrackerLogo {
    display: flex;
    justify-content: center;
    align-items: center;
    margin-bottom: 40px;
  }
  img {
    width: auto;
  }
  .label {
    cursor: pointer;
    position: absolute;
    left: 74px;
    background-color: #000;
    color: white;
    border-radius: 4px;
    padding: 4px 8px;
    width: max-content;
    transition: 0.8s;
    display: none;
    ::before {
      content: '';
      width: 8px;
      height: 8px;
      position: absolute;
      left: -4px;
      top: 12px;
      background: #000000;
      transform: rotate(-45deg);
    }
  }
  .navlink {
    padding: 0;
    margin: 0;
  }
  .listitem {
    padding: 0;
    margin: 0 0 17px 0;
    min-width: 80px;
    width: 100%;
    justify-content: center;
  }
  .listitemIcon {
    min-width: auto;
    min-height: 40px;
    align-items: center;
  }
  .listitemIcon:hover {
    background: #F8F9FD;
    border-radius: 8px;
  }
  .listitemIcon:hover svg {
    path {
      stroke: #000;
    }
    
  }
  .MuiButtonBase-root:hover .label {
    display: block;
  }
  .MuiButtonBase-root,
  .MuiSvgIcon-root {
    background-color: #fff;
  }
  .MuiListItemIcon-root {
    width: 48px;
    justify-content: center;
  }
  .active .MuiListItemIcon,
  .active .listitemIcon {
    background-color: #5932EA;
    border-radius: 8px;
  }
  .active svg,
  .active svg:hover {
    path {
      stroke: #fff;
    }
  }
`
