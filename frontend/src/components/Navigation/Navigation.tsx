import React from 'react'
import { NavLink } from 'react-router-dom'
import { useIntl } from 'react-intl'
import { useAppDispatch, useAppSelector } from '../../redux/hooks'
import GroupAddIcon from '@material-ui/icons/GroupAdd'
import { List, ListItem, ListItemIcon } from '@material-ui/core'
import { NavWrapper } from './Navigation.style'
import { logout } from '../../api/login.api'
import TimetrackerLogo from './icons/Logo.svg'
import { ReactComponent as DashboardInactive } from './icons/dashboardInactive.svg'
import { ReactComponent as ResultsInactive } from './icons/resultsInactive.svg'
import { ReactComponent as CalendarInactive } from './icons/calendarInactive.svg'
import { ReactComponent as GroupsInactive } from './icons/groupsInactive.svg'
import { ReactComponent as LogoutInactive } from './icons/logout.svg'
import { setUser } from '../../redux/slices/user'

interface NavigationProps {
  setHeader: React.Dispatch<React.SetStateAction<string>>
}
const Navigation: React.FC<NavigationProps> = ({ setHeader }) => {
  const intl = useIntl()
  const { user } = useAppSelector(state => state.user)
  const [username, setUsername] = React.useState('')
  const dispatch = useAppDispatch()

  const navItems = [
    {
      path: '/dashboard',
      title: intl.formatMessage({ id: 'dashboard' }),
      icon: <DashboardInactive />
    },
    {
      path: `/${username}/results`,
      title: intl.formatMessage({ id: 'results' }),
      icon: <ResultsInactive />
    },
    {
      path: '/calendar',
      title: intl.formatMessage({ id: 'calendar' }),
      icon: <CalendarInactive />
    },
    {
      path: '/owned-groups',
      title: intl.formatMessage({ id: 'groups' }),
      icon: <GroupsInactive />
    },
    {
      path: '/groups-permissions',
      title: intl.formatMessage({ id: 'group-permissions' }),
      icon: <GroupAddIcon />
    }
  ]

  React.useEffect(() => {
    user && setUsername(user.nickname)
  }, [user])

  return (
  <NavWrapper>
    <div className='nav-top'>
      <div className='timetrackerLogo'>
        <img src={TimetrackerLogo}/>
      </div>
        <List>
          {navItems.map((item, index) => (
            <NavLink className='navlink' activeClassName="active" to={item.path} key={index} exact onClick={() => setHeader(item.title === 'Dashboard' ? 'Timetracker' : item.title)}>
              <ListItem className='listitem' button>
                <ListItemIcon className='listitemIcon'>{item.icon}</ListItemIcon>
                  <label className='label'>{item.title}</label>
              </ListItem>
            </NavLink>
          ))}
        </List>
      </div>
      <NavLink onClick={() => {
        logout()
        dispatch(setUser(null))
      }} className='navlink' activeClassName="active" to='/' exact>
        <ListItem className='listitem' button>
          <ListItemIcon className='listitemIcon'><LogoutInactive /></ListItemIcon>
            <label className='label'>{intl.formatMessage({ id: 'logout' })}</label>
        </ListItem>
      </NavLink>
  </NavWrapper>
  )
}
export default Navigation
