import styled from 'styled-components'

interface Props {
  started: boolean
}
export const StyledTimer = styled.div<Props>`
  display: flex;
  position: relative;
  justify-content: space-between;
  align-items: center;
  margin-left: 16px;
  padding: 8px 16px;
  border-radius: 27px;
  background-color: #fff;
  color: #000;
  box-shadow: 2px 4px 24px rgba(48, 77, 175, 0.1);
  .icons-wrapper {
    display: flex;
    justify-content: center;
    align-items: center;
   }
  .timer-icon {
    height: 32px;
    width: 32px;
  }
  .text {
    margin-left: 4px;
    font-weight: 700;
    font-size: 16px;
  }
  .text-stop {
    color: #5932EA;
  }
  .time {
    text-transform: lowercase;
    color: #5932EA;
    margin-left: 4px;
  }
  .text-time {
    min-width: 70px;
    text-align: right;
  }
  .timer-button {
    border: none;
    background-color: #fff;
    height: 32px;
    cursor: pointer;
    margin-right: 8px;
  }
`
export const StyledDialog = styled.div`
  text-align: center;
  position: absolute;
   padding: 16px;
  top: 60px;
  right: 0;
  background-color: white;
  z-index: 2;
  border-radius: 8px;
  box-shadow: 0px 20px 20px rgba(0, 0, 0, 0.1);
  .title {
    margin-bottom: 6px;
    font-size: 16px;
    font-weight: 400;
    text-align: left;
  }
  .copy-wrapper {
    display: flex;
    justify-content: start;
    align-items: center;
  }
  .copy-field {
    white-space: nowrap;
    font-size: 14px;
    padding: 7px 24px;
    background-color: #F8F9FD;
    border-radius: 12px;
  }
  .copy-btn {
    width: 32px;
    height: 32px;
    border: none;
    margin-left: 20px;
    background-color: #fff;
    cursor: pointer;
  }
`
