import * as React from 'react'
import { format } from 'date-fns'
import differenceInSeconds from 'date-fns/differenceInSeconds'

import { updateUser } from '../../api/user.api'
import { useAppSelector, useAppDispatch } from '../../redux/hooks'
import { setUser } from '../../redux/slices/user'
import { timeConverterWithSeconds } from '../commonFunctions/timeConverter'

import pause from '../../assets/images/pause.png'
import stop from '../../assets/images/stop.png'
import timer from '../../assets/images/timer.png'

import CommandDialog from './CommandDialog'
import { StyledTimer } from './Timer.style'

const Timer = () => {
  const dispatch = useAppDispatch()
  const { user } = useAppSelector((state) => state.user)

  const [started, setStarted] = React.useState(false)
  const [showCommand, setShowCommand] = React.useState(false)
  const [time, setTime] = React.useState<null | string>(null)
  const [timeSec, setTimeSec] = React.useState<number>(0)
  const [paused, setPaused] = React.useState(false)
  const [pausedTime, setPausedTime] = React.useState<number>(0)
  React.useEffect(() => {
    setStarted(!!user!.startDate)
    user!.startDate && setTime(timeConverterWithSeconds(differenceInSeconds(new Date(), new Date(user!.startDate!)))!)
  }, [user])
  React.useEffect(() => {
    const timeLocal = localStorage.getItem('time')
    if (timeLocal !== null) {
      setPaused(!!parseInt(timeLocal))
      setPausedTime(+timeLocal)
    }
  }, [localStorage.getItem('time')])
  const handleTimer = () => {
    const shouldStart = !started
    let start = null
    if (shouldStart) {
      start = `${format(new Date(), 'yyyy-MM-dd')}T${new Date().toLocaleTimeString('en-GB')}.000`
    }
    const newUser = {
      id: user!.id,
      email: user!.email,
      name: user!.name,
      nickname: user!.nickname,
      profile: user!.profile,
      picture: user!.picture,
      hourlyWage: user!.hourlyWage,
      groups: user!.groups,
      defaultGroup: user!.defaultGroup,
      startDate: start
    }
    updateUser(newUser, (response) => dispatch(setUser(response)), true)
  }
  React.useEffect(() => {
    let intervalId: ReturnType<typeof setInterval> | null = null
    if (started && time) {
      intervalId = setInterval(() => {
        const newTime = pausedTime
          ? pausedTime + differenceInSeconds(new Date(), new Date(user!.startDate!))
          : differenceInSeconds(new Date(), new Date(user!.startDate!))
        setTime(timeConverterWithSeconds(newTime + 1)!)
        setTimeSec(newTime + 1)
      }, 1000)
    } else {
      intervalId && clearInterval(intervalId)
    }
    return () => {
      intervalId && clearInterval(intervalId)
    }
  }, [started, user])
  return (
    <>
      <StyledTimer started={started}>
        {started
          ? (
          <>
            <div className='icons-wrapper'>
              <button
                className='timer-button'
                onClick={() => {
                  setTimeSec(0)
                  handleTimer()
                  setShowCommand(true)
                  localStorage.setItem('time', '0')
                }}>
                <img className='timer-icon' src={stop} />
              </button>
              <button
                className='timer-button'
                onClick={() => {
                  handleTimer()
                  setPaused(true)
                  localStorage.setItem('time', timeSec.toString())
                }}>
                <img className='timer-icon' src={pause} />
              </button>
            </div>
            <span className='text text-time'>{user!.startDate && <span className='time'>{time}</span>}</span>
          </>
            )
          : (
          <>
            <button
              className='timer-button'
              onClick={() => {
                setPaused(false)
                handleTimer()
              }}>
              <img className='timer-icon' src={timer} />
            </button>
            {paused ? <p className='text'>{time}</p> : <p className='text'>Start timer</p>}
          </>
            )}
        {showCommand && time && <CommandDialog time={time} close={() => setShowCommand(false)} />}
      </StyledTimer>
    </>
  )
}

export default Timer
