import React, { useEffect, useRef } from 'react'
import { useIntl } from 'react-intl'
import { StyledDialog } from './Timer.style'
import { dispatchSuccess } from '../../commonFunctions/handleSnackbars'
import copy from '../../assets/images/copy.png'

export interface CommandProps {
  time: string
  close: () => void
}
const CommandDialog: React.FC<CommandProps> = ({ time, close }) => {
  const intl = useIntl()
  const dialogRef = useRef<null | HTMLDivElement>(null)

  const handleCopyText = () => {
    navigator.clipboard.writeText(`/spend ${time}`)
    dispatchSuccess('copied')
  }

  useEffect(() => {
    const handleClickOutside = (event: any) => {
      if (!dialogRef.current || dialogRef.current.contains(event.target)) {
        return
      }
      close()
    }
    document.addEventListener('mousedown', handleClickOutside)
    return () => {
      document.removeEventListener('mousedown', handleClickOutside)
    }
  }, [])

  return (
    <div ref={dialogRef}>
      <StyledDialog>
        <h2 className='title'>{intl.formatMessage({ id: 'timer-stopped' })}</h2>
        <div className='copy-wrapper'>
          <p className="copy-field">
            /spend {time}{' '}
          </p>
          <button className="copy-btn" onClick={() => handleCopyText()} >
              <img src={copy} alt="copy icon" />
            </button>
        </div>
      </StyledDialog>
    </div>
  )
}
export default CommandDialog
