export const timeConverter = (time: number, setTime?: (timeValue: string) => void) => {
  const hours = Math.round(time / 60) / 60
  const rhours = Math.floor(hours)
  const minutes = (hours - rhours) * 60
  const rminutes = Math.round(minutes)
  let convertedTime = ''
  if (rminutes === 0 && rhours === 0) {
    convertedTime = '0'
  } else if (rminutes === 0) {
    convertedTime = `${rhours}h`
  } else if (rhours === 0) {
    convertedTime = `${rminutes}min`
  } else {
    convertedTime = `${rhours}h ${rminutes}min`
  }
  if (setTime) {
    setTime(convertedTime)
  } else {
    return convertedTime
  }
}

export const timeConverterWithSeconds = (time: number) => {
  const hours = Math.floor((time / (60 * 60)) % 24)
  const minutes = Math.floor((time / 60) % 60)
  const seconds = Math.floor(time % 60)
  let result
  if (hours > 0) {
    result = `${hours}h ${minutes}m ${seconds}s`
  } else if (minutes > 0) {
    result = `${minutes}m ${seconds}s`
  } else {
    result = `${seconds}s`
  }
  return result
}
