export const editLink = (text: string) => {
  return text.replaceAll(/[\s/]/g, '-').toLowerCase()
}
