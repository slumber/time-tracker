import format from 'date-fns/format'
import { endOfMonth, startOfMonth } from 'date-fns'
import { getUserTimelog } from '../../api/timeTrackingResults.api'
import { ITimeTrackingResults, IUser } from '../../types'

export const handleStartEndOfMonth = (
  date: Date | number,
  user: IUser,
  setResults: (results: ITimeTrackingResults[]) => void
) => {
  const start = `${format(startOfMonth(date), 'yyyy-MM-dd')}T${startOfMonth(date).toLocaleTimeString('en-GB')}.000`
  const end = `${format(endOfMonth(date), 'yyyy-MM-dd')}T${endOfMonth(date).toLocaleTimeString('en-GB')}.000`
  getUserTimelog({ user: user!.nickname, start, end, setResults: response => setResults(response) })
}
