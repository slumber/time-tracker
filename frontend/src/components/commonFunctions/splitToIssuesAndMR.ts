import { IIssue, IMergeRequest, ITimeTrackingResults } from '../../types'
import { dateFormatter } from './dateFormatter'

export interface IIssueResult {
  totalTime: number
  spentAt: string
  issue: IIssue
}
export interface IMergeRequestResult {
  totalTime: number
  spentAt: string
  mergeRequest: IMergeRequest
}

export const splitToIssuesAndMR = (
  resultsToShow: ITimeTrackingResults[] | null,
  setResultsWithIssue: (arg: IIssueResult[]) => void,
  setResultsWithMR: (arg: IMergeRequestResult[]) => void
) => {
  const issuesResults: IIssueResult[] = []
  const mergeRequestResults: IMergeRequestResult[] = []
  if (resultsToShow && resultsToShow.length > 0) {
    resultsToShow.forEach(result => {
      const formattedDate = dateFormatter(result.spentAt)
      if (result.issue) {
        const issueExist = issuesResults.some(issuesResult => issuesResult?.issue?.id === result.issue!.id)
        if (!issueExist) {
          const resultToPush = {
            totalTime: result.timeSpent,
            spentAt: formattedDate,
            issue: result.issue
          }
          issuesResults.push(resultToPush)
        } else {
          const index = issuesResults.findIndex(issuesResult => issuesResult.issue!.id === result.issue!.id)
          issuesResults[index].totalTime = issuesResults[index].totalTime + result.timeSpent
        }
      } else if (result.mergeRequest) {
        const mergeRequestExist = mergeRequestResults.some(
          mrResult => mrResult.mergeRequest?.id === result.mergeRequest!.id
        )
        if (!mergeRequestExist) {
          const resultToPush = {
            totalTime: result.timeSpent,
            spentAt: formattedDate,
            mergeRequest: result.mergeRequest
          }
          mergeRequestResults.push(resultToPush)
        } else {
          const index = mergeRequestResults.findIndex(mrResult => mrResult.mergeRequest!.id === result.mergeRequest!.id)
          mergeRequestResults[index].totalTime = mergeRequestResults[index].totalTime + result.timeSpent
        }
      }
      setResultsWithIssue(issuesResults)
      setResultsWithMR(mergeRequestResults)
    })
  } else {
    setResultsWithIssue([])
    setResultsWithMR([])
  }
}
