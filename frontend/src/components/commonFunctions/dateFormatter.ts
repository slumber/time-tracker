import format from 'date-fns/format'

export const dateFormatter = (dateString: string) => {
  const date = new Date(dateString)
  const formattedDate = format(date, 'dd/MM/yyyy')
  return formattedDate
}
