import styled from 'styled-components'

interface Props {
  fullWidth?: any
}
export const UserBox = styled.div<Props>`
  width: ${props => (props.fullWidth ? '100%' : 'calc(100% - 20px)')};
  margin: ${props => (props.fullWidth ? '0 auto -28px' : '0 auto 12px')};
  display: flex;
  align-items: center;
  justify-content: center;
  background-color: #22325f;
  border-radius: 4px;
  padding: 7px;
  color: #fff;
  grid-column: 1/-1;
  background-image: linear-gradient(to right, #074a91, #164484, #1d3e77, #21386b, #22325f);
  .time {
    margin: 0 64px 0 4px;
  }
  .avatar {
    margin-left: 64px;
  }
`
