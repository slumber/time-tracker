import * as React from 'react'
import { Button } from '@material-ui/core'
import { useIntl } from 'react-intl'

import { getUserTotalTimePerGroup } from '../../../api/timeTrackingResults.api'
import { IOwnedGroupsUsers } from '../../../types'
import { timeConverter } from '../../commonFunctions/timeConverter'
import { useTimeForUserPerGroupContext } from '../context/timeForUserPerGroup'

import Avatar from '../../Avatar/Avatar'
import CSVLinkComponent from '../../common/CSVLinkComponent/CSVLinkComponent'
import { UserBox } from './GroupUser.style'
import TimeRangeCallendars from '../../common/Filters/TimeRangeCallendars/TimeRangeCallendars'

export interface GroupUserProps {
  groupUser: IOwnedGroupsUsers
  group: string
  requestPath: string
  fullWidth?: boolean
}

const GroupUser: React.FC<GroupUserProps> = ({ groupUser, group, fullWidth, requestPath }) => {
  const intl = useIntl()
  const timeContext = useTimeForUserPerGroupContext()
  const [selectDay, setSelectDay] = React.useState(false)

  React.useEffect(() => {
    getUserTotalTimePerGroup(groupUser.nickname, data => timeContext.setTotalTime(data.seconds!), requestPath)
  }, [groupUser, group])

  return (
    <UserBox fullWidth={fullWidth}>
      {selectDay
        ? (
        <TimeRangeCallendars close={() => setSelectDay(false)} />
          )
        : (
        <Button className='selectDayBtn' variant='contained' color='primary' onClick={() => setSelectDay(true)}>
          {intl.formatMessage({ id: 'select-day' })}
        </Button>
          )}
      <Avatar src={groupUser.picture} size={'24px'} />
      <span>{groupUser.name} - </span>
      <span className='time'>{timeContext.totalTime ? timeConverter(timeContext.totalTime) : 0}</span>
      <CSVLinkComponent />
    </UserBox>
  )
}

export default GroupUser
