import styled from 'styled-components'
import { Container } from '../../common/Table/Table.style'
import { navbarWidth } from '../../../variables'

interface Props {
  ownerView: any
}

export const StyledContainer = styled(Container)<Props>`
  height: auto;
  padding: 96px 16px 12px calc(16px + ${navbarWidth});
  display: grid;
  grid-template-columns: 1fr 1fr;
  column-gap: 24px;
  row-gap: 40px;
  align-content: start;
  .MuiTableContainer-root {
    width: 100%;
    &:nth-child(even) {
      margin-left: ${props => (props.ownerView ? 'auto' : '0')};
      margin-right: 0;
    }
    &:nth-child(odd) {
      margin-right: ${props => (props.ownerView ? 'auto' : '0')};
      margin-left: 0;
    }
  }
  .fullWidth {
    grid-column: 1/-1;
    margin: 0 auto;
  }
  .MuiIconButton-label {
    font-size: 16px;
  }
  .datePickerWrapper {
    grid-column: 1/-1;
  }
  .currentDay-history {
    background-color: #dee4f2;
  }
  .link {
    color: #000085;
  }
`
