import * as React from 'react'
import { useIntl } from 'react-intl'
import { useLocation, useParams } from 'react-router-dom'

import { useAppDispatch, useAppSelector } from '../../../redux/hooks'
import { IGroup, IOwnedGroupsUsers, ITimeTrackingResults } from '../../../types'
import { dateFormatter } from '../../commonFunctions/dateFormatter'
import { getTimelogsPerGroup, getUserTimeTrackingResults } from '../../../api/timeTrackingResults.api'
import { setTimeTrackingResults } from '../../../redux/slices/timeTrackingResults'
import TimeForUserPerGroupContextProvider from '../context/timeForUserPerGroup'

import Header from '../Header/Header'
import SingleTable from './SingleTable/SingleTable'
import Message from '../../Message/Message'
import { StyledContainer } from './ResultsPerDay.style'
import GroupUser from '../GroupUser/GroupUser'

const ResultsPerDay = () => {
  const intl = useIntl()
  const dispatch = useAppDispatch()
  const { username, group } = useParams<{ username: string; group: string }>()
  const location = useLocation<{ user: IOwnedGroupsUsers; group: IGroup }>()
  const user = location?.state?.user
  const groupState = location?.state?.group

  const { results } = useAppSelector((state) => state.timeTrackingResults)
  const [resultsToShow, setResultsToShow] = React.useState<ITimeTrackingResults[] | null>(null)
  const [dailyResults, setDailyResults] = React.useState<[ITimeTrackingResults[]] | any[]>([])
  const [dates, setDates] = React.useState<string[]>([])

  const setUpDates = () => {
    if (resultsToShow) {
      const allDates: string[] = []
      resultsToShow.forEach((result) => {
        const date = dateFormatter(result.spentAt)
        const dateExist = allDates.some((singleDate) => singleDate === date)
        !dateExist && allDates.push(date)
      })
      setDates(allDates)
    }
  }

  React.useEffect(() => {
    results && setResultsToShow(results)
  }, [results])

  React.useEffect(setUpDates, [resultsToShow])

  React.useEffect(() => {
    const sortedResults: [ITimeTrackingResults[]] | any[] = []
    dates.forEach((singleDate) => {
      const resultsPerDay = resultsToShow?.filter((result) => dateFormatter(result.spentAt) === singleDate)
      resultsPerDay && sortedResults.push(resultsPerDay)
    })
    sortedResults.sort((a, b) => {
      const dateA = new Date(a[0].spentAt)
      const dateB = new Date(b[0].spentAt)
      return dateA.getDate() - dateB.getDate()
    })
    setDailyResults(sortedResults)
  }, [dates])

  React.useEffect(() => {
    if (groupState) {
      getTimelogsPerGroup(username, groupState.fullPath, (data) => dispatch(setTimeTrackingResults(data)))
    } else {
      getUserTimeTrackingResults(username, (data) => dispatch(setTimeTrackingResults(data)))
    }
  }, [groupState, username, dispatch])

  return (
    <TimeForUserPerGroupContextProvider>
      <StyledContainer ownerView={!!group}>
        <Header />
        {user && groupState && (
          <GroupUser groupUser={user} group={group} fullWidth={true} requestPath={groupState.fullPath} />
        )}
        {dailyResults?.map((result, index) => (
          <SingleTable key={index} results={result} oneDay={dates.length === 1} />
        ))}
        {dailyResults.length === 0 && <Message text={intl.formatMessage({ id: 'no-results' })} />}
      </StyledContainer>
    </TimeForUserPerGroupContextProvider>
  )
}

export default ResultsPerDay
