import * as React from 'react'
import { endOfDay, subDays } from 'date-fns'
import { DateRangePicker } from 'react-date-range'
import 'react-date-range/dist/styles.css'
import 'react-date-range/dist/theme/default.css'
import { enGB } from 'date-fns/locale'
import staticRangesGenerator from './rangesConfig'

interface IRange {
  start: Date
  end: Date
}
export interface DatePickerProps {
  setRange: (range: IRange) => void
  forResults?: boolean
}

const DatePicker: React.FC<DatePickerProps> = ({ setRange, forResults }) => {
  const now = React.useRef(new Date())
  const [to, setTo] = React.useState(now.current)
  const [from, setFrom] = React.useState(subDays(now.current, 7))
  const staticRanges = staticRangesGenerator(enGB)

  const handleSelect = React.useCallback(({ selection: { startDate, endDate } }) => {
    if (forResults) {
      sessionStorage.setItem('dateStartResults', startDate)
      sessionStorage.setItem('dateEndResults', endDate)
    } else {
      sessionStorage.setItem('dateStartGroups', startDate)
      sessionStorage.setItem('dateEndGroups', endDate)
    }
    setFrom(startDate)
    setTo(endDate)
  }, [])

  const ranges = React.useMemo(() => {
    return [
      {
        startDate: from,
        endDate: to,
        key: 'selection'
      }
    ]
  }, [from, to])

  React.useEffect(() => {
    setRange({ start: from, end: endOfDay(to) })
  }, [to, from, setRange])

  return (
    <DateRangePicker
      onChange={handleSelect}
      moveRangeOnFirstSelection={false}
      months={2}
      ranges={ranges}
      direction='horizontal'
      locale={enGB}
      maxDate={now.current}
      staticRanges={staticRanges}
    />
  )
}

export default DatePicker
