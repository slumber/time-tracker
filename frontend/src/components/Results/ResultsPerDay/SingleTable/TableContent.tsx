import * as React from 'react'
import { useIntl } from 'react-intl'
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown'
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp'
import { Table, TableBody, TableCell, TableHead, TableRow, Box, Collapse, IconButton } from '@material-ui/core'
import { useAppSelector } from '../../../../redux/hooks'
import { IHistory, ITimeTrackingResults } from '../../../../types'
import { timeConverter } from '../../../commonFunctions/timeConverter'
import { dateFormatter } from '../../../commonFunctions/dateFormatter'

export interface TableContentProps {
  result: ITimeTrackingResults
}

const TableContent: React.FC<TableContentProps> = ({ result }) => {
  const intl = useIntl()
  const { results } = useAppSelector(state => state.timeTrackingResults)

  const [open, setOpen] = React.useState(false)
  const resultId = result.mergeRequest ? result.mergeRequest.id : result.issue!.id
  const [history, setHistory] = React.useState<IHistory[]>([])

  React.useEffect(() => {
    const historyArray: IHistory[] = []
    results?.forEach(singleResult => {
      historyArray.push({
        date: dateFormatter(singleResult.spentAt),
        originalDateFormat: singleResult.spentAt,
        time: timeConverter(singleResult.timeSpent)!,
        id: singleResult.mergeRequest ? singleResult.mergeRequest.id : singleResult.issue!.id
      })
      const filteredHistory = historyArray.filter(history => history.id === resultId)
      filteredHistory.sort((a, b) => {
        const dateA = new Date(a.originalDateFormat!)
        const dateB = new Date(b.originalDateFormat!)
        return dateA.getDate() - dateB.getDate()
      })
      setHistory(filteredHistory)
    })
  }, [results, resultId])

  return (
    <>
      <TableRow className={open ? 'thickBorder' : ''} onClick={() => setOpen(!open)}>
        <TableCell>{result.mergeRequest ? 'MR' : 'Issue'}</TableCell>
        <TableCell component='th' scope='row'>
          <a
            href={result.mergeRequest ? result.mergeRequest.webUrl : result.issue!.webUrl}
            target='_blank'
            rel='noopener noreferrer'
            className='link'
            onClick={event => event.stopPropagation()}
          >
            {result.mergeRequest ? result.mergeRequest.title : result.issue!.title}
          </a>
        </TableCell>
        <TableCell align='center'>{timeConverter(result.timeSpent)}</TableCell>
        <TableCell align='center'>
          <IconButton aria-label='expand row' size='small' onClick={() => setOpen(!open)} className='historyExpandBtn'>
            {intl.formatMessage({ id: 'history' })}
            {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
          </IconButton>
        </TableCell>
      </TableRow>
      <TableRow>
        <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
          <Collapse in={open} timeout='auto' unmountOnExit>
            <Box>
              <Table size='small' aria-label='purchases'>
                <TableHead className='historyHead'>
                  <TableRow>
                    <TableCell>{intl.formatMessage({ id: 'day' })}</TableCell>
                    <TableCell>{intl.formatMessage({ id: 'time' })}</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {history.map((history, index) => (
                    <TableRow
                      key={index}
                      className={
                        history.date === dateFormatter(result.spentAt) ? 'historyRow currentDay-history' : 'historyRow'
                      }
                    >
                      <TableCell component='th' scope='row' className='historyCell'>
                        {history.date}
                      </TableCell>
                      <TableCell className='historyCell'>{history.time}</TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </Box>
          </Collapse>
        </TableCell>
      </TableRow>
    </>
  )
}

export default TableContent
