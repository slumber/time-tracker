import * as React from 'react'
import { useIntl } from 'react-intl'
import { Table, TableCell, TableContainer, TableHead, TableRow, Paper, TableBody } from '@material-ui/core'

import { ITimeTrackingResults } from '../../../../types'
import { dateFormatter } from '../../../commonFunctions/dateFormatter'
import { timeConverter } from '../../../commonFunctions/timeConverter'

import TableContent from './TableContent'

export interface SingleTableProps {
  results: ITimeTrackingResults[]
  oneDay: boolean
}

const SingleTable: React.FC<SingleTableProps> = ({ results, oneDay }) => {
  const intl = useIntl()
  const [dailyTime, setDailyTime] = React.useState('')

  React.useEffect(() => {
    let totalTime = 0
    results.forEach(result => {
      totalTime += result.timeSpent
    })
    const time = timeConverter(totalTime)
    time && setDailyTime(time)
  }, [results])

  return (
    <TableContainer component={Paper} className={oneDay ? 'fullWidth' : ''}>
      <Table aria-label='collapsible table'>
        <TableHead>
          <TableRow>
            <TableCell>{intl.formatMessage({ id: 'type' })}</TableCell>
            <TableCell>{intl.formatMessage({ id: 'name' })}</TableCell>
            <TableCell align='center' className='timeCell'>
              {intl.formatMessage({ id: 'time' })} ({dailyTime})
            </TableCell>
            <TableCell align='center' className='dateCell'>
              {results[0] && dateFormatter(results[0].spentAt)}
            </TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {results.map((result, index) => (
            <TableContent key={index} result={result} />
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  )
}

export default SingleTable
