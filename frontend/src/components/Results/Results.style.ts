import styled from 'styled-components'

export const NoResultsMessage = styled.div`
  display: flex;
  flex-direction: column;
  margin: 0 auto;
  align-items: center;
  margin-top: 100px;
  .no-results-img {
    width: 200px;
  }
  .no-results {
    margin-top: 10px;
    text-align: center;
    font-size: 20px;
    width: 290px;
    color: #000;
  }
`
