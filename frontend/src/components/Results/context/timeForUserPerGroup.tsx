import React from 'react'

interface StatsContextValue {
  totalTime: number | null
  setTotalTime: (arg: number) => void
}

export const TimeForUserPerGroupContext = React.createContext<StatsContextValue>(null!)

export const useTimeForUserPerGroupContext = () => {
  return React.useContext(TimeForUserPerGroupContext)
}

export interface TimeForUserPerGroupProviderProps {
  children: React.ReactNode
}

const TimeForUserPerGroupContextProvider: React.FC<TimeForUserPerGroupProviderProps> = ({ children }) => {
  const [totalTime, setTotalTime] = React.useState<number | null>(0)

  return (
    <TimeForUserPerGroupContext.Provider value={{ totalTime, setTotalTime }}>
      {children}
    </TimeForUserPerGroupContext.Provider>
  )
}

export default TimeForUserPerGroupContextProvider
