import React, { useEffect, useRef } from 'react'
import { useParams } from 'react-router-dom'
import { useIntl } from 'react-intl'
import { FormControlLabel, Checkbox } from '@material-ui/core'

import { getUserTotalTime } from '../../../api/timeTrackingResults.api'
import { setTotalWorkTime } from '../../../redux/slices/totalWorkTime'
import { useAppDispatch, useAppSelector } from '../../../redux/hooks'
import arrowIcon from '../../../assets/images/arrow-down.png'
import checkIcon from '../../../assets/images/check-icon.png'
import { ITimeTrackingResults } from '../../../types'

import CSVLinkComponent from '../../common/CSVLinkComponent/CSVLinkComponent'
import Filters from '../../common/Filters/Filters'
import { FilterBtn } from '../../common/Filters/Filters.style'
import Calendar from '../../common/Filters/Calendar'
import { CheckboxWrapper, FilterNumber, ResetBtn, FiltersWrapper } from './Header.style'

export interface HeaderProps {
  setTypeFilter?: React.Dispatch<React.SetStateAction<string[]>>
  typeFilter?: string[]
  projectsList?: string[]
  setProjectFilter?: React.Dispatch<React.SetStateAction<string[]>>
  projectFilter?: string[]
  connectSimilar?: boolean
  setConnectSimilar?: React.Dispatch<React.SetStateAction<boolean>>
  resultsToShow?: ITimeTrackingResults[] | null
}

const Header: React.FC<HeaderProps> = ({
  setTypeFilter,
  typeFilter,
  projectsList,
  setProjectFilter,
  projectFilter,
  connectSimilar,
  setConnectSimilar,
  resultsToShow
}) => {
  const intl = useIntl()
  const dispatch = useAppDispatch()
  const { username, group } = useParams<{ username: string; group: string }>()
  const { total } = useAppSelector((state) => state.totalWorkTime)
  const typeRef = useRef<null | HTMLDivElement>(null)
  const nameRef = useRef<null | HTMLDivElement>(null)

  const [openTypeFilter, setOpenTypeFilter] = React.useState(false)
  const [openNameFilter, setOpenNameFilter] = React.useState(false)

  const handleFilter = (option: string, checked: boolean, type: boolean) => {
    const filter = type ? typeFilter : projectFilter
    const setFilter = type ? setTypeFilter : setProjectFilter
    if (!setFilter) return
    if (filter) {
      setFilter(checked ? [...filter, option] : filter.filter((el) => el !== option))
    } else {
      setFilter([option])
    }
  }

  useEffect(() => {
    const handleClickOutside = (event: MouseEvent) => {
      if (!typeRef.current || !typeRef.current.contains(event.target as HTMLElement)) {
        setOpenTypeFilter(false)
      }
      if (!nameRef.current || !nameRef.current.contains(event.target as HTMLElement)) {
        setOpenNameFilter(false)
      }
    }
    document.addEventListener('mousedown', handleClickOutside)
    return () => {
      document.removeEventListener('mousedown', handleClickOutside)
    }
  }, [])

  React.useEffect(() => {
    !total && getUserTotalTime(username, (data) => dispatch(setTotalWorkTime(data)))
  }, [total, getUserTotalTime])
  return (
    <FiltersWrapper>
      <Filters>
          {!group && <Calendar forResults={true} />}
          {typeFilter && (
            <div className='filter' ref={typeRef}>
              <FilterBtn className={typeFilter.length ? 'active' : ''} onClick={() => setOpenTypeFilter(!openTypeFilter)}>
                {intl.formatMessage({ id: 'type-of-task' })}
                {!!typeFilter?.length && <FilterNumber>{typeFilter.length}</FilterNumber>}
                <img src={arrowIcon} alt='arrow down' className='icon' />
              </FilterBtn>
              {openTypeFilter && (
                <CheckboxWrapper>
                  <FormControlLabel
                    control={
                      <Checkbox
                        color='default'
                        value={'merge'}
                        onChange={(event) => {
                          handleFilter(event.target.value, event.target.checked, true)
                        }}
                        checked={typeFilter.includes('merge')}
                      />
                    }
                    label='Merge request'
                  />
                  <FormControlLabel
                    control={
                      <Checkbox
                        color='default'
                        value={'issue'}
                        onChange={(event) => handleFilter(event.target.value, event.target.checked, true)}
                        checked={typeFilter.includes('issue')}
                      />
                    }
                    label='Issue'
                  />
                </CheckboxWrapper>
              )}
            </div>
          )}
          {projectFilter && (
            <div className='filter' ref={nameRef}>
              <FilterBtn className={projectFilter.length ? 'active' : ''} onClick={() => setOpenNameFilter(!openNameFilter)}>
                {intl.formatMessage({ id: 'project-name' })}
                {!!projectFilter?.length && <FilterNumber>{projectFilter.length}</FilterNumber>}
                <img src={arrowIcon} alt='arrow down' className='icon' />
              </FilterBtn>
              {openNameFilter && (
                <CheckboxWrapper>
                  {projectsList?.map((name) => (
                    <FormControlLabel
                      key={name}
                      control={
                        <Checkbox
                          color='default'
                          value={name}
                          onChange={(event) => {
                            handleFilter(event.target.value, event.target.checked, false)
                          }}
                          checked={projectFilter.includes(name)}
                        />
                      }
                      label={name}
                    />
                  ))}
                </CheckboxWrapper>
              )}
            </div>
          )}
          <FilterBtn
            className={connectSimilar ? 'active' : ''}
            onClick={() => setConnectSimilar && setConnectSimilar(!connectSimilar)}>
            {!!connectSimilar && <img className='icon-check' src={checkIcon} alt='check icon' />}
            {intl.formatMessage({ id: 'connect-similar' })}
          </FilterBtn>
          {setProjectFilter &&
            setTypeFilter &&
            setConnectSimilar &&
            (!!typeFilter?.length || !!projectFilter?.length || connectSimilar) && (
              <ResetBtn
                onClick={() => {
                  setProjectFilter([])
                  setTypeFilter([])
                  setConnectSimilar(false)
                }}>
                {intl.formatMessage({ id: 'clear' })}
              </ResetBtn>
          )}
      </Filters>
      {!group && <CSVLinkComponent data={resultsToShow} resultsMonth={true} />}
    </FiltersWrapper>
  )
}

export default Header
