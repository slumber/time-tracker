import styled from 'styled-components'
import { FormGroup } from '@material-ui/core'

export const FiltersWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  margin-right: 10px;
`

export const FilterNumber = styled.span`
  background-color: #5932ea;
  height: 16px;
  width: 16px;
  border-radius: 20px;
  margin-left: 4px;
  color: #fff;
  font-size: 12px;
`
export const CheckboxWrapper = styled(FormGroup)`
  position: absolute;
  background-color: #fff;
  top: 53px;
  white-space: nowrap;
  color: #000;
  z-index: 1;
  border-radius: 12px;
  box-shadow: 0px 8px 24px rgba(0, 0, 0, 0.12);
  .Mui-checked {
    color: #5932EA;
  }
  .MuiFormControlLabel-root {
    margin: 0;
    padding: 6px 34px 6px 4px;
    width: 100%;
    left: 0;
    :hover {
      background-color: #EFF2FB;
    }
  }
`

export const ResetBtn = styled.button`
  margin-left: 12px;
  background: none;
  border: none;
  font-weight: 600;
  font-size: 16px;
  cursor: pointer;
`
