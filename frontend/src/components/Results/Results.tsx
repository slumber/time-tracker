import {
  Table,
  TableCell,
  TableContainer,
  TableSortLabel,
  TableHead,
  TableRow,
  Paper,
  TableBody,
  TablePagination
} from '@material-ui/core'
import ArrowDropDown from '@material-ui/icons/ArrowDropDown'
import * as React from 'react'
import { useIntl } from 'react-intl'
import { useLocation, useParams } from 'react-router-dom'
import { format } from 'date-fns'

import { useAppDispatch, useAppSelector } from '../../redux/hooks'
import { setTimeTrackingResults } from '../../redux/slices/timeTrackingResults'
import { IGroup, IOwnedGroupsUsers, ITimeTrackingResults, SORT_TYPE } from '../../types'
import { getTimelogsPerGroup, getUserTimeTrackingResults, getUserTimelog } from '../../api/timeTrackingResults.api'
import TimeForUserPerGroupContextProvider from './context/timeForUserPerGroup'
import noResults from '../../assets/images/no-results.png'

import Header from './Header/Header'
import MergeRequestsTable from './ResultsTable/ResultsTable'
import { Container, TableWrapper } from '../common/Table/Table.style'
import { NoResultsMessage } from './Results.style'
import GroupUser from './GroupUser/GroupUser'

const Results = () => {
  const [page, setPage] = React.useState(0)
  const [filterArrow, setFilterArrow] = React.useState('')
  const [rowsPerPage, setRowsPerPage] = React.useState(10)
  const [typeFilter, setTypeFilter] = React.useState<string[]>([])
  const [projectFilter, setProjectFilter] = React.useState<string[]>([])
  const [projectsList, setProjectsList] = React.useState<string[]>([])
  const [connectSimilar, setConnectSimilar] = React.useState<boolean>(false)

  const intl = useIntl()
  const { username, group } = useParams<{ username: string; group: string }>()
  const dispatch = useAppDispatch()
  const location = useLocation<{ user: IOwnedGroupsUsers; group: IGroup }>()
  const locationStateUser = location?.state?.user
  const groupState = location?.state?.group

  const { user } = useAppSelector(state => state.user)
  const { results } = useAppSelector((state) => state.timeTrackingResults)
  const [resultsToShow, setResultsToShow] = React.useState<ITimeTrackingResults[] | null>(null)

  const basicSetup = () => {
    results && setResultsToShow(results)
  }

  React.useEffect(basicSetup, [results])

  React.useEffect(() => {
    dispatch(setTimeTrackingResults([]))
    if (group) {
      getTimelogsPerGroup(username, group, (data) => dispatch(setTimeTrackingResults(data)))
    } else {
      if (sessionStorage.getItem('dateStartResults') && sessionStorage.getItem('dateEndResults')) {
        const startDate = new Date(sessionStorage.getItem('dateStartResults') || '')
        const endDate = new Date(sessionStorage.getItem('dateEndResults') || '')
        const start = `${format(startDate, 'yyyy-MM-dd')}T${startDate.toLocaleTimeString('en-GB')}.000`
        const end = `${format(endDate, 'yyyy-MM-dd')}T${endDate.toLocaleTimeString('en-GB')}.000`
        getUserTimelog({
          user: username,
          start,
          end,
          setResults: (response) => dispatch(setTimeTrackingResults(response)),
          closePopup: close
        })
      } else {
        getUserTimeTrackingResults(username, (data) => dispatch(setTimeTrackingResults(data)))
      }
    }
  }, [group, username, groupState, dispatch, user])

  const handleChangeRowsPerPage = (event: React.ChangeEvent<HTMLInputElement>) => {
    setRowsPerPage(parseInt(event.target.value, 10))
    setPage(0)
  }

  const handleSorting = (ACTION: SORT_TYPE, sortType: string = 'title') => {
    let newArr: ITimeTrackingResults[] = resultsToShow ? [...resultsToShow] : []
    switch (ACTION) {
      case SORT_TYPE.TYPE:
        newArr = []
        resultsToShow?.map((el) => {
          const type = !!resultsToShow && resultsToShow[0].mergeRequest ? 'mergeRequest' : 'issue'
          return el[type] ? newArr.push(el) : newArr.unshift(el)
        })
        break
      case SORT_TYPE.DATE:
        newArr.sort((a, b) => {
          if (resultsToShow && resultsToShow[0].spentAt > resultsToShow[resultsToShow.length - 1].spentAt) {
            return a.spentAt.localeCompare(b.spentAt)
          } else {
            return b.spentAt.localeCompare(a.spentAt)
          }
        })
        break
      case SORT_TYPE.TIME:
        newArr.sort((a, b) => {
          if (resultsToShow && resultsToShow[0].timeSpent > resultsToShow[resultsToShow.length - 1].timeSpent) {
            return a.timeSpent > b.timeSpent ? 1 : -1
          } else {
            return a.timeSpent < b.timeSpent ? 1 : -1
          }
        })
        break
      case SORT_TYPE.NAME:
        newArr.sort((a, b) => {
          const firstEl = getElementTitle(resultsToShow![0], sortType)
          const lastEl = getElementTitle(resultsToShow![resultsToShow!.length - 1], sortType)
          const aTitle = getElementTitle(a, sortType)
          const bTitle = getElementTitle(b, sortType)
          if (firstEl.localeCompare(lastEl) === 1) {
            return aTitle.localeCompare(bTitle)
          } else {
            return bTitle.localeCompare(aTitle)
          }
        })
        break
      case SORT_TYPE.FILTERS_CHECKBOX:
        newArr = results ? [...results] : []
        if (!projectFilter.length && !typeFilter.length) {
          setResultsToShow(newArr)
        } else {
          newArr = []
          results?.map((el) => {
            if (
              el.mergeRequest &&
              (!projectFilter.length || projectFilter.includes(el.mergeRequest.webUrl.split('/')[4])) &&
              (!typeFilter.length || typeFilter.includes('merge'))
            ) {
              newArr.push(el)
            } else if (
              el.issue &&
              (!projectFilter.length || projectFilter.includes(el.issue.webUrl.split('/')[4])) &&
              (!typeFilter.length || typeFilter.includes('issue'))
            ) {
              newArr.push(el)
            }
            return newArr
          })
        }
        break
    }
    setResultsToShow(newArr)
  }

  const handleConnectSimilar = () => {
    const newObj: any = {}
    resultsToShow?.map((el) => {
      const link = getElementLink(el)
      if (newObj[link]) {
        newObj[link].push(el)
      } else {
        newObj[link] = [el]
      }
      return newObj
    })
    createConnectedResults(newObj)
  }

  const createConnectedResults = (objToEdit: any) => {
    const newArr = []
    if (!resultsToShow) return
    for (const el of Object.keys(objToEdit)) {
      if (objToEdit[el].length === 1) {
        newArr.push(...objToEdit[el])
      } else {
        let time = 0
        for (const i of objToEdit[el]) {
          time += i.timeSpent
        }
        const newEl = {
          type: 'CONNECTED',
          link: el,
          projectName: getElementTitle(objToEdit[el][0], 'project-name'),
          title: getElementTitle(objToEdit[el][0], 'title'),
          timeSpent: time,
          spentAt: objToEdit[el][0].spentAt,
          history: [...objToEdit[el]],
          user: objToEdit[el][0].user
        }
        newArr.push(newEl)
      }
    }
    setResultsToShow(newArr)
  }

  React.useEffect(() => {
    setConnectSimilar(false)
    handleSorting(SORT_TYPE.FILTERS_CHECKBOX)
  }, [typeFilter, projectFilter])

  React.useEffect(() => {
    if (connectSimilar === true) {
      handleConnectSimilar()
    } else {
      handleSorting(SORT_TYPE.FILTERS_CHECKBOX)
    }
  }, [connectSimilar])

  React.useEffect(() => {
    const newArr: string[] = []
    results?.map((el) => {
      const projectName = getElementTitle(el, 'project-names')
      if (!newArr.includes(projectName)) {
        newArr.push(projectName)
      }
      return newArr
    })
    setProjectsList(newArr)
  }, [resultsToShow])

  const getElementTitle = (element: any, sortType: string) => {
    let elementTitle = ''
    if (element.mergeRequest) {
      elementTitle = sortType === 'title' ? element.mergeRequest.title : element.mergeRequest.webUrl.split('/')[4]
    } else if (element.issue) {
      elementTitle = sortType === 'title' ? element.issue.title : element.issue.webUrl.split('/')[4]
    }
    return elementTitle
  }

  const getElementLink = (element: any) => {
    let elementTitle = ''
    if (element.mergeRequest) {
      elementTitle = element.mergeRequest.webUrl
    } else if (element.issue) {
      elementTitle = element.issue.webUrl
    }
    return elementTitle
  }

  return (
    <TimeForUserPerGroupContextProvider>
      <Container>
        <Header
          setTypeFilter={setTypeFilter}
          typeFilter={typeFilter}
          projectsList={projectsList}
          setProjectFilter={setProjectFilter}
          projectFilter={projectFilter}
          connectSimilar={connectSimilar}
          setConnectSimilar={setConnectSimilar}
          resultsToShow={resultsToShow}
        />
        <TableWrapper>
          {locationStateUser && groupState && (
            <GroupUser groupUser={locationStateUser} group={group} requestPath={groupState.fullPath} />
          )}
          {!results || results?.length === 0
            ? (
            <NoResultsMessage>
              <img className='no-results-img' src={noResults} alt='no results' />
              <p className='no-results'>{intl.formatMessage({ id: 'no-results-for-date' })}</p>
            </NoResultsMessage>
              )
            : (
            <>
              <TableContainer component={Paper}>
                <Table aria-label='collapsible table'>
                  <TableHead className='issuesHead'>
                    <TableRow>
                      <TableCell className='type-cell'>
                        <TableSortLabel
                          active={true}
                          direction={filterArrow === 'types' ? 'asc' : 'desc'}
                          IconComponent={ArrowDropDown}
                          onClick={() => {
                            handleSorting(SORT_TYPE.TYPE)
                            setFilterArrow('types')
                          }}>
                          {intl.formatMessage({ id: 'type' })}
                        </TableSortLabel>
                      </TableCell>
                      <TableCell className='name-cell' align='left'>
                        <TableSortLabel
                          active={true}
                          direction={filterArrow === 'names' ? 'asc' : 'desc'}
                          IconComponent={ArrowDropDown}
                          onClick={() => {
                            handleSorting(SORT_TYPE.NAME, 'title')
                            setFilterArrow('names')
                          }}>
                          {intl.formatMessage({ id: 'name' })}
                        </TableSortLabel>
                      </TableCell>
                      <TableCell align='left' className='project-cell'>
                        <TableSortLabel
                          active={true}
                          direction={filterArrow === 'project-names' ? 'asc' : 'desc'}
                          IconComponent={ArrowDropDown}
                          onClick={() => {
                            handleSorting(SORT_TYPE.NAME, 'project-name')
                            setFilterArrow('project-names')
                          }}>
                          {intl.formatMessage({ id: 'project-name' })}
                        </TableSortLabel>
                      </TableCell>
                      <TableCell align='left' className='time-cell'>
                        <TableSortLabel
                          active={true}
                          direction={filterArrow === 'time' ? 'asc' : 'desc'}
                          IconComponent={ArrowDropDown}
                          onClick={() => {
                            handleSorting(SORT_TYPE.TIME)
                            setFilterArrow('time')
                          }}>
                          {intl.formatMessage({ id: 'time' })} [h]
                        </TableSortLabel>
                      </TableCell>
                      <TableCell align='left' className='dateCell date-cell'>
                        <TableSortLabel
                          active={true}
                          direction={filterArrow === 'date' ? 'asc' : 'desc'}
                          IconComponent={ArrowDropDown}
                          onClick={() => {
                            handleSorting(SORT_TYPE.DATE)
                            setFilterArrow('date')
                          }}>
                          {intl.formatMessage({ id: 'date' })}
                        </TableSortLabel>
                      </TableCell>
                      <TableCell align='left' className='link-cell'>
                        {intl.formatMessage({ id: 'link' })}
                      </TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {resultsToShow?.slice(page * rowsPerPage, page * rowsPerPage + rowsPerPage).map((result, index) => (
                      <MergeRequestsTable key={index} result={result} />
                    ))}
                  </TableBody>
                </Table>
              </TableContainer>
              {resultsToShow && (
                <TablePagination
                  rowsPerPageOptions={[5, 10, 25]}
                  component='div'
                  count={resultsToShow.length}
                  rowsPerPage={rowsPerPage}
                  page={page}
                  onPageChange={(event: unknown, newPage: number) => setPage(newPage)}
                  onRowsPerPageChange={handleChangeRowsPerPage}
                />
              )}
            </>
              )}
        </TableWrapper>
      </Container>
    </TimeForUserPerGroupContextProvider>
  )
}

export default Results
