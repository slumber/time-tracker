import * as React from 'react'
import { useIntl } from 'react-intl'
import KeyboardArrowDownIcon from '@material-ui/icons/KeyboardArrowDown'
import KeyboardArrowUpIcon from '@material-ui/icons/KeyboardArrowUp'
import { Box, Collapse, TableCell, TableHead, Table, IconButton, TableBody, TableRow } from '@material-ui/core'
import { IHistory, IIssue, ITimeTrackingResults } from '../../../types'
import { timeConverter } from '../../commonFunctions/timeConverter'
import { dateFormatter } from '../../commonFunctions/dateFormatter'

export interface IIssueResult {
  totalTime: number
  spentAt: string
  issue: IIssue
}

export interface IssuesTableTableProps {
  result: IIssueResult
  historyResults: ITimeTrackingResults[]
}

const IssuesTableTable: React.FC<IssuesTableTableProps> = ({ result, historyResults }) => {
  const intl = useIntl()
  const [open, setOpen] = React.useState(false)
  const [time, setTime] = React.useState('')
  const [history, setHistory] = React.useState<IHistory[]>([])
  const resultId = result.issue!.id

  React.useEffect(() => {
    timeConverter(result.totalTime, setTime)
  }, [result])

  React.useEffect(() => {
    const historyArray: IHistory[] = []
    historyResults.forEach(historyResult => {
      historyResult.issue &&
        historyArray.push({
          date: dateFormatter(historyResult.spentAt),
          originalDateFormat: historyResult.spentAt,
          time: timeConverter(historyResult.timeSpent)!,
          id: historyResult.issue.id
        })
    })
    const filteredHistory = historyArray.filter(history => history.id === resultId)
    filteredHistory.sort((a, b) => {
      const dateA = new Date(a.originalDateFormat!)
      const dateB = new Date(b.originalDateFormat!)
      return dateA.getDate() - dateB.getDate()
    })
    setHistory(historyArray)
  }, [historyResults, resultId])

  return (
    <>
      <TableRow className={open ? 'thickBorder' : ''} onClick={() => setOpen(!open)}>
        <TableCell component='th' scope='row'>
          <a
            href={result.issue.webUrl}
            target='_blank'
            rel='noopener noreferrer'
            className='link'
            onClick={event => event.stopPropagation()}
          >
            {result.issue!.title}
          </a>
        </TableCell>
        <TableCell align='left' className='timeCell'>
          {time}
        </TableCell>
        <TableCell>
          <IconButton aria-label='expand row' size='small' onClick={() => setOpen(!open)} className='historyExpandBtn'>
            {intl.formatMessage({ id: 'history' })} {open ? <KeyboardArrowUpIcon /> : <KeyboardArrowDownIcon />}
          </IconButton>
        </TableCell>
      </TableRow>
      <TableRow>
        <TableCell style={{ paddingBottom: 0, paddingTop: 0 }} colSpan={6}>
          <Collapse in={open} timeout='auto' unmountOnExit>
            <Box>
              <Table size='small' aria-label='purchases'>
                <TableHead className='historyHead'>
                  <TableRow>
                    <TableCell>{intl.formatMessage({ id: 'day' })}</TableCell>
                    <TableCell>{intl.formatMessage({ id: 'time' })}</TableCell>
                  </TableRow>
                </TableHead>
                <TableBody>
                  {history.map((history, index) => (
                    <TableRow key={index} className='historyRow'>
                      <TableCell component='th' scope='row' className='historyCell'>
                        {history.date}
                      </TableCell>
                      <TableCell className='historyCell'>{history.time}</TableCell>
                    </TableRow>
                  ))}
                </TableBody>
              </Table>
            </Box>
          </Collapse>
        </TableCell>
      </TableRow>
    </>
  )
}

export default IssuesTableTable
