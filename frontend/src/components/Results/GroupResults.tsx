import { Table, TableCell, TableContainer, TableHead, TableRow, Paper, TableBody } from '@material-ui/core'
import * as React from 'react'
import { useIntl } from 'react-intl'
import { useLocation, useParams } from 'react-router-dom'

import { useAppDispatch, useAppSelector } from '../../redux/hooks'
import { setTimeTrackingResults } from '../../redux/slices/timeTrackingResults'
import { IGroup, IOwnedGroupsUsers, ITimeTrackingResults } from '../../types'
import { getTotalGroupTime } from '../../api/timeTrackingResults.api'

import Header from './Header/Header'
import ResultsTable from './ResultsTable/ResultsTable'
import Message from '../Message/Message'
import { Container, TableWrapper } from '../common/Table/Table.style'
import GroupUser from './GroupUser/GroupUser'
import TimeForUserPerGroupContextProvider from './context/timeForUserPerGroup'

const GroupResults = () => {
  const intl = useIntl()
  const { username, group } = useParams<{ username: string; group: string }>()
  const dispatch = useAppDispatch()
  const location = useLocation<{ user: IOwnedGroupsUsers; group: IGroup }>()
  const user = location?.state?.user
  const groupState = location?.state?.group
  const { results } = useAppSelector((state) => state.timeTrackingResults)
  const [resultsToShow, setResultsToShow] = React.useState<ITimeTrackingResults[] | null>(null)
  const basicSetup = () => {
    results && setResultsToShow(results)
  }

  React.useEffect(basicSetup, [results])

  React.useEffect(() => {
    dispatch(setTimeTrackingResults([]))
    if (group) {
      getTotalGroupTime(group, (data) => dispatch(setTimeTrackingResults(data)))
    }
  }, [group, username, groupState, dispatch])

  return (
    <TimeForUserPerGroupContextProvider>
      <Container>
        <Header />
        <TableWrapper>
          {user && groupState && (
            <GroupUser groupUser={user} group={group} requestPath={group} />
          )}
          {!results || results?.length === 0
            ? (
            <Message text={intl.formatMessage({ id: 'no-results' })} />
              )
            : (
            <>
              <TableContainer component={Paper}>
                <Table aria-label='collapsible table'>
                  <TableHead className='issuesHead'>
                    <TableRow>
                      <TableCell>Issue</TableCell>
                      <TableCell align='left'>{intl.formatMessage({ id: 'time' })}</TableCell>
                      <TableCell align='left' className='dateCell'></TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {resultsToShow?.map((result, index) => (
                      <ResultsTable key={index} result={result} />
                    ))}
                  </TableBody>
                </Table>
              </TableContainer>
              <TableContainer component={Paper}>
                <Table aria-label='collapsible table'>
                  <TableHead>
                    <TableRow>
                      <TableCell>Merge Request</TableCell>
                      <TableCell align='left'>{intl.formatMessage({ id: 'time' })}</TableCell>
                      <TableCell align='left' className='dateCell'></TableCell>
                    </TableRow>
                  </TableHead>
                  <TableBody>
                    {resultsToShow?.map((result, index) => (
                      <ResultsTable key={index} result={result} />
                    ))}
                  </TableBody>
                </Table>
              </TableContainer>
            </>
              )}
        </TableWrapper>
      </Container>
    </TimeForUserPerGroupContextProvider>
  )
}

export default GroupResults
