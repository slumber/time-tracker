import * as React from 'react'
import { TableCell, TableRow, Collapse, Table, TableHead } from '@material-ui/core'
import { useIntl } from 'react-intl'
import { timeConverter } from '../../commonFunctions/timeConverter'
import { dateFormatter } from '../../commonFunctions/dateFormatter'

interface IDataResults {
  title: string;
  id: string;
  webUrl: string;
}

export interface IHistory {
  timeSpent: number
  spentAt: string
  mergeRequest: IDataResults | null
  issue?: IDataResults | null
}

export interface IMergeRequestResult {
  timeSpent: number
  spentAt: string
  projectName?: string
  link?: string
  type?: string
  title?: string
  history?: IHistory[]
  mergeRequest: IDataResults | null
  issue?: IDataResults | null
}

export interface MergeRequestsTableProps {
  result: IMergeRequestResult
}

const MergeRequestsTable: React.FC<MergeRequestsTableProps> = ({ result }) => {
  const [open, setOpen] = React.useState(false)
  const [time, setTime] = React.useState('')
  const intl = useIntl()

  React.useEffect(() => {
    timeConverter(result.timeSpent, setTime)
  }, [result])
  return (
    <>
      <TableRow className={result.type ? 'click' : ''} onClick={() => setOpen(!open)}>
        <TableCell component='th' scope='row'>
          {!result.mergeRequest && !result.issue ? result.type : (result.mergeRequest ? 'Merge Request' : 'Issue')}
        </TableCell>
        <TableCell component='th' scope='row'>
          {result.mergeRequest?.title || result.issue?.title || result.title}
        </TableCell>
        <TableCell align='left' className='timeCell'>
          {result.mergeRequest?.webUrl.split('/')[4] || result.issue?.webUrl.split('/')[4] || result.projectName}
        </TableCell>
        <TableCell align='left' className='timeCell'>
          {time}
        </TableCell>
        <TableCell>
          {dateFormatter(result.spentAt)}
        </TableCell>
        <TableCell align='left'>
          <a
            href={result.mergeRequest?.webUrl || result.issue?.webUrl || result.link}
            target='_blank'
            rel='noopener noreferrer'
            className='link'
            onClick={event => event.stopPropagation()}
          >
            {intl.formatMessage({ id: 'go-to-task' })}
          </a>
        </TableCell>
      </TableRow>
      {result.history &&
        <TableCell style={{ padding: 0, paddingTop: 0 }} colSpan={6}>
          <Collapse in={open} timeout="auto" unmountOnExit className='collapse'>
          <Table>
            <TableHead style={{ display: 'none' }}>
              <TableRow>
                <TableCell className='type-cell'></TableCell>
                <TableCell className='name-cell'></TableCell>
                <TableCell className='project-cell'></TableCell>
                <TableCell className='time-cell'></TableCell>
                <TableCell className='date-cell'></TableCell>
                <TableCell className='link-cell'></TableCell>
              </TableRow>
            </TableHead>
            {result.history.map((el) => (
              <TableRow key={el.spentAt} style={{ width: '100%' }}>
                <TableCell component='th' scope='row' className='type-cell'>
                  {el.mergeRequest ? 'Merge Request' : 'Issue'}
                </TableCell>
                <TableCell component='th' scope='row' className='name-cell'>
                  {el.mergeRequest?.title || el.issue?.title || 'Connected'}
                </TableCell>
                <TableCell align='left' className='project-cell'>
                  {el.mergeRequest?.webUrl.split('/')[4] || el.issue?.webUrl.split('/')[4]}
                </TableCell>
                <TableCell align='left' className='time-cell'>
                  {timeConverter(el.timeSpent)}
                </TableCell>
                <TableCell className='date-cell'>
                  {dateFormatter(el.spentAt)}
                </TableCell>
                <TableCell align='left' className='link-cell'>
                  <a
                    href={el.mergeRequest?.webUrl || el.issue?.webUrl}
                    target='_blank'
                    rel='noopener noreferrer'
                    className='link'
                    onClick={event => event.stopPropagation()}
                  >
                    {intl.formatMessage({ id: 'go-to-task' })}
                  </a>
                </TableCell>
              </TableRow>
            ))}
            </Table>
          </Collapse>
        </TableCell>
      }
    </>
  )
}

export default MergeRequestsTable
