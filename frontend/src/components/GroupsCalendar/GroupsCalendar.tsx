import * as React from 'react'
import 'react-big-calendar/lib/css/react-big-calendar.css'
import format from 'date-fns/format'
import FullCalendar from '@fullcalendar/react'
import dayGridPlugin from '@fullcalendar/daygrid'
import '@fullcalendar/daygrid/main.css'

import { useAppDispatch, useAppSelector } from '../../redux/hooks'
import { timeConverterWithSeconds } from '../commonFunctions/timeConverter'
import { getUserTimeTrackingResults } from '../../api/timeTrackingResults.api'
import { setTimeTrackingResults } from '../../redux/slices/timeTrackingResults'
import { CalendarWrapper } from './GroupsCalendar.style'

const GroupsCalendar = () => {
  interface IGroupedResults {
    title: string
    allDay: true
    start: Date
    end: Date
  }

  interface CalendarFix extends React.Component {}

  const Calendar = (FullCalendar as any) as {
    new(): CalendarFix;
  }
  const dispatch = useAppDispatch()
  const { results } = useAppSelector(state => state.timeTrackingResults)
  const { user } = useAppSelector(state => state.user)
  const [groupedResultsList, setGroupedResultsList] = React.useState<IGroupedResults[]>([])

  React.useEffect(() => {
    const resultsPerDay: { date: Date; time: number; group: string }[] = []
    results?.forEach(result => {
      const date: number | Date = new Date(result.spentAt)
      const formattedDate = format(date, 'dd-MM-yyyy')
      const index = resultsPerDay.map(e => format(e.date, 'dd-MM-yyyy')).indexOf(formattedDate)
      if (index !== -1) {
        resultsPerDay[index].time += result.timeSpent
      } else {
        resultsPerDay.push({ date, time: result.timeSpent, group: user!.defaultGroup })
      }
    })
    const calendarData: IGroupedResults[] = []
    resultsPerDay.forEach(result => {
      calendarData.push({
        title: `${result.group} - ${timeConverterWithSeconds(result.time)}`,
        allDay: true,
        start: result.date,
        end: result.date
      })
    })
    setGroupedResultsList(calendarData)
  }, [results])

  React.useEffect(() => {
    getUserTimeTrackingResults(user!.nickname, data => dispatch(setTimeTrackingResults(data)))
  }, [user])

  const props: any = {
    plugins: [dayGridPlugin],
    initialView: 'dayGridMonth',
    headerToolbar: { start: '', center: 'prev,title,today,next', end: 'dayGridMonth,dayGridWeek' },
    events: groupedResultsList,
    height: 700,
    fixedWeekCount: false,
    locale: 'en-gb',
    eventMouseEnter: (info: any) => {
      const div = document.createElement('div')
      div.classList.add('popup')
      div.append(info.el.innerText)
      info.el.appendChild(div)
      info.el.classList.add('active')
      return info.el
    },
    eventMouseLeave: (info: any) => {
      const popup = document.querySelector('.popup')
      info.el.removeChild(popup)
      info.el.classList.remove('active')
      return info.el
    }
  }

  return (
    <>
      <CalendarWrapper>
        <Calendar {...props}
        />
      </CalendarWrapper>
    </>
  )
}
export default GroupsCalendar
