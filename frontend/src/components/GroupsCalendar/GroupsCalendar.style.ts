import styled from 'styled-components'

export const CalendarWrapper = styled.div`
  width: 100vw;
  padding: 24px 24px 24px 104px;
  .fc-media-screen {
    background-color: #fff;
    padding: 27px 36px;
    box-shadow: 4px 4px 28px -10px rgba(0, 0, 0, 0.1);
    border-radius: 12px;
  }
  .fc-toolbar-chunk div {
    display: flex;
    align-items: center;
  }
  .fc-daygrid-day-frame,
  .fc-scrollgrid-sync-inner {
    height: 20px;
  }
  .fc-next-button,
  .fc-prev-button {
    background-color: #fff;
    color: #666E7D;
    border: 1px solid #DBE5F0;
    height: 40px;
    width: 40px;
    display: flex;
    align-items: center;
    justify-content: center;
    :hover {
      background-color: #fff;
      color: #666E7D;
    }
  }
  .fc-today-button {
    background-color: #fff;
    color: #000;
    border-radius: 24px;
    font-size: 16px;
    margin-right: 37px; 
    padding: 8px 16px;
    text-transform: capitalize;
    border: 1px solid #DBE5F0;
  }
  .fc-toolbar-title {
    margin: 0 16px 0 26px;
    font-size: 21px;
  }
  .fc-button:disabled {
    background-color: #fff;
    color: #000;
  }
  .fc-scrollgrid-sync-inner {
    text-align: right;
    margin-bottom: 12px;
    border: none;
  }
  .fc-scrollgrid {
    border-right: none;
    border-top: none;
    position: relative;
    ::before {
      position: absolute;
      content: '';
      width: 3px;
      height: 32px;
      top: 0;
      left: -1px;
      background-color: #fff;
    }
  }
  td .fc-daygrid-day:first-child {
    border-left: 5px solid black;
    box-sizing: content-box;
  }
  thead {
    border-bottom: 1px solid #E9E9E9;
  }
  .fc-dayGridMonth-button,
  .fc-dayGridWeek-button {
    margin-right: 16px;
    border-radius: 24px;
    padding: 8px 25px;
    background-color: #fff;
    color: #000;
    text-transform: capitalize;
    font-size: 16px;
  }
  .fc .fc-button-primary:not(:disabled):active, 
  .fc .fc-button-primary:not(:disabled).fc-button-active,
  .fc .fc-button-primary:hover {
    background-color: #fff;
    color: #000;
    border: 1px solid #5932EA;
  }
  .fc-direction-ltr .fc-button-group > .fc-button:not(:last-child) {
    border-top-right-radius: 24px;
    border-bottom-right-radius: 24px;
  }
  .fc-direction-ltr .fc-button-group > .fc-button:not(:first-child) {
    border-top-left-radius: 24px;
    border-bottom-left-radius: 24px;
  }
  .fc .fc-button-primary:focus {
    box-shadow: none;
  }
  .fc .fc-button-primary:not(:disabled):active:focus, .fc .fc-button-primary:not(:disabled).fc-button-active:focus {
    box-shadow: none;
  }
  .fc-daygrid-day-events {
    padding: 5px;
    display: flex;
    flex-direction: column;
    justify-content: flex-end;
  }
  .fc-event-title-container {
    text-align: left;
    padding: 3px;
    background-color: #1871F8;
    cursor: pointer;
  }
  .fc-event-title {
    font-size: 12px;
    font-weight: 500;
    text-overflow: ellipsis;
  }
  .fc-daygrid-day-number {
    font-size: 16px;
  }
  .fc .fc-daygrid-day.fc-day-today {
    background-color: #fff;
  }
  .fc .fc-button-primary,
  .fc .fc-button-primary:disabled {
    border-color: #DBE5F0;
  }
  th {
    border: none;
  }
  .active {
    position: relative;
  }
  .popup {
    bottom: 150%;
    left: 50%;
    transform: translateX(-50%);
    position: absolute;
    width: fit-content;
    background-color: #EFF2FB;
    padding: 5px 10px;
    border-radius: 4px;
    color: #000;
    font-size: 12px;
    font-weight: 500;
  }
`
