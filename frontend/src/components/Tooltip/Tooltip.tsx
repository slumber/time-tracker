import React from 'react'
import { Tooltip } from '@material-ui/core'
import HelpOutlineIcon from '@material-ui/icons/HelpOutline'

import { StyledButton, TooltipWrapper } from './Tooltip.style'

export interface TooltipComponentProps {
  title: string
  placement: 'right' | 'bottom' | 'left'
}

const TooltipComponent: React.FC<TooltipComponentProps> = ({ title, placement }) => {
  return (
    <TooltipWrapper className='tooltip'>
      <div>
        <Tooltip arrow title={title} placement={placement}>
          <StyledButton>
            <HelpOutlineIcon fontSize='small' />
          </StyledButton>
        </Tooltip>
      </div>
    </TooltipWrapper>
  )
}

export default TooltipComponent
