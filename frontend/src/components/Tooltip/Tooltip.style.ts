import styled from 'styled-components'

export const StyledButton = styled.div`
  overflow-x: hidden;
  * {
    overflow-x: hidden;
  }
  padding: 0;
  min-width: 0;
`
export const TooltipWrapper = styled.div`
  .MuiTooltip-tooltipPlacementBottom {
    margin: 0;
  }
  .MuiTooltip-tooltip {
    background-color: #025e84;
    color: white;
    opacity: 1 !important;
  }
  .MuiSvgIcon-root {
    vertical-align: middle;
    fill: #0082b7;
    margin-left: 4px;
  }
`
