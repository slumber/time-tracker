import * as React from 'react'
import { useIntl } from 'react-intl'
import { Container } from './SessionExpired.style'
import ErrorImg from './images/Error.svg'

const ErrorSessionExpired = () => {
  const intl = useIntl()
  const refreshButton = () => {
    window.location.href = '/'
  }

  return (
    <Container>
      <div>
        <img className='errorImg' src={ErrorImg}/>
        <span>{intl.formatMessage({ id: 'session-expired' })}</span>
        <button
          onClick={refreshButton}
        >
          {intl.formatMessage({ id: 'refresh-page' })}
        </button>
      </div>
    </Container>
  )
}

export default ErrorSessionExpired
