import styled from 'styled-components'

export const Container = styled.div`
  width: 100vw;
  height: 100vh;
  background-color: #fff;
  div {
    display: flex;
    justify-content: center;
    align-items: center;
    flex-direction: column;
  }
  span {
    font-family: 'Poppins';
    font-style: normal;
    font-weight: 600;
    font-size: 36px;
    line-height: 54px;
    margin-bottom: 20px;
  }
  button {
    background: #5932EA;
    align-items: center;
    padding: 8px 24px;
    gap: 16px;
    border-radius: 70px;
    border: none;
    color: white;
    cursor: pointer;
  }
  .errorImg {
    width: auto;
    height: auto;
  }
`
