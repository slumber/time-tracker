import styled from 'styled-components'
import Avatar from '@material-ui/core/Avatar'

interface Props {
  size?: string
  margin?: string
}
export const StyledAvatar = styled(Avatar)<Props>`
  width: ${({ size }) => size};
  height: ${({ size }) => size};
  margin: ${({ margin }) => margin || '2px 10px'};
  box-shadow: 0px 4px 4px rgba(0, 0, 0, 0.15);
`
