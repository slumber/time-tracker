import * as React from 'react'
import { StyledAvatar } from './Avatar.style'

export interface AvatarProps {
  src: string
  size?: string
  margin?: string
}

const Avatar: React.FC<AvatarProps> = ({ src, size }) => {
  return <StyledAvatar size={size} src={src} className='avatar' />
}

export default Avatar
