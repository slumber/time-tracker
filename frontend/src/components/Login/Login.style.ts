import styled from 'styled-components'
import { Button } from '@material-ui/core'

export const MainContainer = styled.div`
  width: 100vw;
  height: 100vh;
  background-color: #fff;
`
export const LeftContainer = styled.div`
  display: flex;
  float:left;
  align-items: center;
  justify-content: center;
  width: 50%;
  height: 100%;
  background-color: #fff;
  font-style: normal;
  .section {
    display: flex;
    flex-direction: column;
    align-items: center;
    width: max-content;
    height: auto;
  }
  .logoTT {
    width: 64px;
    height: 64px;
  }
  .heading {
    font-weight: 700;
    font-size: 40px;
    line-height: 48px;
    padding-bottom: 12px;
    padding-top: 24px;
  }
  .description {
    max-width: 290px;
    font-weight: 400;
    font-size: 16px;
    line-height: 24px;
    text-align: center;
    padding-bottom: 24px;
  }
`
export const RightContainer = styled.div`
  display:flex;
  float: right;
  align-items: center;
  justify-content: center;
  height: 100%;
  width: 50%;
  background-color: #5932EA;
  img {
    width: 400px;
    height: 400px;
  }
  .description2 {
    font-weight: 500;
    font-size: 16px;
    line-height: 24px;
    text-align: center;
    width: 542px;
    color: #fff;
  }
`
export const LoginSection = styled.section`
  background-color: #fff;
  padding: 32px 48px;
  border-radius: 10px 0 0 10px;
  justify-content: space-between;
  .intro {
    display: flex;
    flex-direction: column;
    align-items: center;
    justify-content: center;
  }
  .description {
    text-align: center;
  }
`
export const GitlabButton = styled(Button)`
  display: flex;
  flex-direction: row;
  align-items: center;
  height: 40px;
  min-width: 302px;
  padding: 8px 24px;
  border-radius: 70px;
  background-color: #5932EA;
  &:hover {
    background-color: #5932EA;
  }
  .MuiButton-label {
    display: flex;
  }
  img {
    width: 35px;
    margin-right: 8px;
  }
  span {
    font-style: normal;
    font-weight: 700;
    font-size: 16px;
    text-transform: none;
  }
`
