import * as React from 'react'
import { useIntl } from 'react-intl'

import { login } from '../../api/login.api'
import gitlabLogo from '../../assets/images/gitlab-icon.png'

import logoTT from './icons/LogoTT.svg'
import welcomeImg from './icons/Welcome.svg'

import { MainContainer, LeftContainer, RightContainer, LoginSection, GitlabButton } from './Login.style'

const SearchForm = () => {
  const intl = useIntl()

  return (
    <MainContainer>
      <LeftContainer>
        <LoginSection className='section'>
          <div className='intro'>
            <img className='logoTT' src={logoTT}/>
            <span className='heading'>TimeTracker</span>
            <div className='description'>{intl.formatMessage({ id: 'check-working-time' })}</div>
          </div>
          <GitlabButton type='submit' variant='contained' onClick={() => login()}>
            <img src={gitlabLogo} alt='logo' />
            <span className='login'>{intl.formatMessage({ id: 'login' })}</span>
          </GitlabButton>
        </LoginSection>
      </LeftContainer>
      <RightContainer>
        <div>
          <img src={welcomeImg}/>
          <div className='description2'>{intl.formatMessage({ id: 'app-info' })}</div>
        </div>
      </RightContainer>
    </MainContainer>
  )
}
export default SearchForm
