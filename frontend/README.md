# Time Tracker

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.\
You will also see any lint errors in the console.

### `npm test`

Launches the test runner in the interactive watch mode.\
See the section about [running tests](https://facebook.github.io/create-react-app/docs/running-tests) for more information.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

See the section about [deployment](https://facebook.github.io/create-react-app/docs/deployment) for more information.

## Libraries

1. [Typescript](https://www.typescriptlang.org/docs/) - it gives us opportunity of undentify errors earlier, before script runs and error occurs in the browser, and it helps to avoid some unwanted behavior
2. [styled-components](https://styled-components.com/docs) - help keep the concerns of styling and element architecture separated and make components more readable.
3. [Material-UI](https://material-ui.com/getting-started/usage/) - React components for faster and easier web development.
4. [Redux Toolkit](https://redux-toolkit.js.org/tutorials/quick-start) - The official, opinionated, batteries-included toolset for efficient Redux development.
5. [react-intl](https://formatjs.io/docs/react-intl/) - Used for internalization. The language is set by default according to the locale of the user's browser. Any text should be written in proper file in a `language` directory and used in file with useIntl hook. For more info see [the docs.](https://formatjs.io/docs/react-intl/api/)
6. [Axios](https://github.com/axios/axios) - A promis-based HTTP client for the browser and Node.js, intended to improve native fetch().
7. [date-fns](https://date-fns.org/v2.9.0/docs/Getting-Started) - provides the most comprehensive, yet simple and consistent toolset for manipulating JavaScript dates.
8. [react-date-range](https://github.com/hypeserver/react-date-range#readme) - A date library agnostic React component for choosing dates and date ranges. Uses [date-fns](https://date-fns.org/v2.9.0/docs/Getting-Started) for date operations.
9. [react-big-calendar](https://jquense.github.io/react-big-calendar/examples/index.html) - An events calendar component built for React.
10. [recharts](https://recharts.org/en-US/api) - A composable charting library.
11. [react-csv](https://github.com/react-csv/react-csv#readme) - Generates a CSV file from given data.
