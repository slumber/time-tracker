# Time-tracker

Small utility app to retrieve time logs from GitLab and present them on the dashboard.

## Preview

Live demo available [here](https://tt-dev.internal.hakkie.io)

![](images/login.png)
![](images/dashboard.png)

- Displaying data on hours worked in the current and previous month.
- Calculation wage based on the given hourly rate.

![](images/results.png)

- Displaying information about reported tasks with the possibility of filtering by date, task type, project name and connecting similar tasks.
- Generating a report based on displayed tasks.

![](images/calendar.png)

- Displaying the time statement in the calendar for the selected group.

![](images/groups.png)

- Generating a report with time statement for a selected group, subgroup or user with the option of selecting a specific time range.

![](images/group-permissions.png)

- Adding and removing permissions for a specific user that can generate a time statement for any user.

## Deployment
You have few options to deploy this application and test it:
1. Classic way - set up database, build backend, frontend and start them on your host/vm/vps.

2. Local docker images - build docker images for frontend and backend locally and use `docker-compose.yml` file to
   start all needed components - frontend, backend and postgres.

3. Kubernetes - use manifests (deployment-frontend.yaml, deployment-backend.yaml) as examples to deploy application on the Kubernetes cluster.

### Gitlab environment variables setup

Before any kind of deployment some additional steps are needed, because App uses Gitlab as Auth Identity Provider. So:

- Go to your gitlab profile `https://gitlab.com/-/profile/applications`.
  - create new application.
  - "Name" field is up to you (just can be TimeTracker).
  - set "Redirect URI" to `http://localhost:8080/api/auth`.
  - check `confidential` and scopes: `openid` `profile` `email` `read_api` and click "Save Application".
  - copy and store somewhere temporary `Application ID` and `Secret` (it will show only once). You will need it in the App.

## Classic way local deployment on MacOS / Windows

### MacOS (tested on Monterey)
<a name="database_classic_mac_anchor"></a>
#### 1. Database (only in first run):
- make `01-init.sh` file executable by `chmod u+x 01-init.sh` command.
- run postres container `docker-compose up -d postgres` and verify in logs that `01-init.sh` was executed properly (should be by default).

<a name="backend_classic_mac_anchor"></a>
#### 2. Backend:
- create in project root folder .env file with following content:
 
  `GITLAB_OIDC_AUTH_SERVER=https://gitlab.com`  
  `GITLAB_OIDC_CLIENT_ID=(here provide your Gitlab Application ID)`  
  `GITLAB_OIDC_CLIENT_SECRET=(here provide your Gitlab Secret)`

- run mvn or mvn wrapper command: `compile quarkus:dev` .
- verify that app is up and there is no problem with backend <-> database connection

#### 3. Frontend
- navigate to `/frontend` directory
- make `env.sh` file executable by `chmod u+x env.sh` command.
- change API_PATH in .env file from: `https://timetracker.itersive.com/api` to: `http://localhost:8080/api`
- run commands:
```shell script
npm install
npm start
```

### Windows (tested on Windows 10)
<a name="database_classic_windows_anchor"></a>
#### 1. Database (only in first run):
- VALIDATE, that file `db/01-init.sh` has correct line endings! If your editor already changed it to CRLF, change it
  back to LF. If you skip this, bash shell scripts will not work under Windows.
- run postres container `docker-compose up -d postgres` and verify in logs that `01-init.sh` was executed properly (should be by default).
#### 2. Backend:
- looks exactly the same as on MacOS, take a look [here](#backend_classic_mac_anchor).
#### 3. Frontend:
- navigate to `/frontend` directory
- change API_PATH in .env file from: `https://timetracker.itersive.com/api` to: `http://localhost:8080/api`
- VALIDATE, that file `.env` has correct (LF) line endings.
- VALIDATE, that file `env.sh` has correct (LF) line endings.
- go to package.json and change start script from: `chmod +x ./env.sh && bash env.sh && cp env-config.js ./public/ && react-scripts start` to:
  `bash env.sh && copy env-config.js .\\public\\ && react-scripts start`
- run commands:
```shell script
npm install
npm start
```

## Local docker images on MacOS / Windows
### MacOS (tested on Monterey)
#### 1. Database:
- looks exactly the same as in classic way deployment MacOS version [here](#database_classic_mac_anchor).
#### 2. Backend:
- create in project root folder .env file with the same content, as in Classic way deployment.
- prepare backend artifact by running command: `docker run -it --rm --name my-maven-project -v
  "$(pwd)":/usr/src/mymaven -w /usr/src/mymaven maven:3.8-jdk-11 mvn clean install` or using maven: `mvn package` or
  maven wrapper: `mvnw package`
- build backend docker image: `docker build -f src/main/docker/Dockerfile.jvm --no-cache -t registry.gitlab.com/hakkie-it/time-tracker/time-tracker-api .`
- run docker backend container: `docker-compose up -d backend`
#### 3. Frontend:
- navigate to `/frontend` directory
- make `env.sh` file executable by `chmod u+x env.sh` command.
- check `.env` file has path to local backend `API_PATH=http://localhost:8080/api` if You're running it locally,
  otherwise it should be `API_PATH=https://timetracker.itersive.com/api`
- prepare frontend build:
```shell script
 npm ci
 npm run build
```
- build frontend image: `docker build --no-cache -t registry.gitlab.com/hakkie-it/time-tracker/time-tracker .`
- run frontend container: `docker-compose up -d frontend`
- after starting container, `env.sh` script will be executed to set up environment variables from `.env` file.

### Windows (tested on Windows 10)
#### 1. Database:
- looks exactly the same as in Classic way deployment Windows version [here](#database_classic_windows_anchor).
#### 2. Backend:
- create in project root folder .env file with the same content, as in Classic way deployment.
- prepare backend artifact by running command: ` docker run -it --rm --name my-maven-project -v "$(pwd):/usr/src/mymaven"
  -w /usr/src/mymaven maven:3.8-jdk-11 mvn clean install` or using maven: `mvn package` or
  maven wrapper: `mvnw package`
- build backend docker image: `docker build -f src/main/docker/Dockerfile.jvm --no-cache -t registry.gitlab.com/hakkie-it/time-tracker/time-tracker-api .`
- run docker backend container: `docker-compose up -d backend`
#### 3. Frontend:
- navigate to `/frontend` directory
- VALIDATE, that file `env.sh` has correct (LF) line endings.
- check `.env` file has path to local backend `API_PATH=http://localhost:8080/api` if You're running it locally,
  otherwise it should be `API_PATH=https://timetracker.itersive.com/api`
- prepare frontend build:
```shell script
 npm ci
 npm run build
```
- build frontend image: `docker build --no-cache -t registry.gitlab.com/hakkie-it/time-tracker/time-tracker .`
- run frontend container: `docker-compose up -d frontend`
- after starting container, `env.sh` script will be executed to set up environment variables from `.env` file.

### Frontend notes

You can run your application in dev mode that enables live coding using (all commands in `frontend` directory:

```shell script
npm run start
```

### Backend notes

### Running the application in dev mode

You can run your application in dev mode that enables live coding using:

```shell script
./mvnw compile quarkus:dev
```

> **_NOTE:_**  Quarkus now ships with a Dev UI, which is available in dev mode only at <http://localhost:8080/q/dev/>.

### Packaging and running the application

The application can be packaged using:

```shell script
./mvnw package
```

It produces the `quarkus-run.jar` file in the `target/quarkus-app/` directory. Be aware that it’s not an _über-jar_ as
the dependencies are copied into the `target/quarkus-app/lib/` directory.

If you want to build an _über-jar_, execute the following command:

```shell script
./mvnw package -Dquarkus.package.type=uber-jar
```

The application is now runnable using `java -jar target/quarkus-app/quarkus-run.jar`.

### Creating a native executable

You can create a native executable using:

```shell script
./mvnw package -Pnative
```

Or, if you don't have GraalVM installed, you can run the native executable build in a container using:

```shell script
./mvnw package -Pnative -Dquarkus.native.container-build=true
```

You can then execute your native executable with: `./target/time-tracker-1.0-SNAPSHOT-runner`

If you want to learn more about building native executables, please consult <https://quarkus.io/guides/maven-tooling.html>
.

### Deployment

To local test you can use `docker-compose.yml`.

Master - always tested and stable environment - <https://timetracker.internal.hakkie.io>

Develop - environment in which we test new features - <https://ttdesigns.internal.hakkie.io>

### Useful commands

Show all docker containers running `docker ps`

Stop docker / remove containers `docker-compose down`

Remove all volumes `docker volume prune`

Remove image `docker rmi image_name`

Remove unused images `docker image prune`

Retrieve logs present at the time of execution of container `docker logs container_name/_id`

## Roadmap 2022

- Performance backend fixes
- Add times directly from app
- Fix logout option
- Clarify session expiration behavior

## Contributors

- Natalia Dziedzic - @ndziedzic - frontend
- Paulina Karnabal - @pkarnabal - management
- Wojciech Pater - @wpater - backend
- Kamil Szestowicki - @kszestowicki - backend
- Rafał Wędzony - @rwedzony - backend
- Przemysław Korościk - @pkoroscik - team lead
- Maciej Smolarczyk - @Maciej0 - frontend
